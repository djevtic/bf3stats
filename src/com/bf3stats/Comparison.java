package com.bf3stats;

import java.text.DecimalFormat;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Comparison extends ListActivity{
	
	private Player player;
	private Context context;
	private ListView list;
	private LinearLayout listHeaderView;
	private GlobalStatsArrayAdaptor teamadapter;
	private TextView textPlayer1Name;
	private TextView textPlayer2Name;
	private String tag = "djevtic";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comparison);
        context = getApplicationContext();
        player = Player.getInstance();
        LayoutInflater inflater = this.getLayoutInflater();
        textPlayer1Name = (TextView) findViewById(R.id.comparisonplayer1);
        textPlayer2Name = (TextView) findViewById(R.id.comparisonplayer2);
        fillPage();
	}

	private void fillPage() {
		textPlayer1Name.setText(player.getPlayerName());
		textPlayer2Name.setText(player.getPlayer2name());
		list = 	getListView();
		String[] playerInfostats = getPlayerCompare();
    	teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, "compare");
    	setListAdapter(teamadapter);
		
	}

	private String[] getPlayerCompare() {
		String[] menulist = new String[] {"Rank","W/L","K/D","Score","Headshoot","Longest H.","K.Streak","Accuracy","Assault","Engineer","Recon","Support","Revive","Resupplies","Heals","Repairs"};
		return menulist;
	}

	public String GetAccuracy(long first, long secound){
		 
        double d;
        d = ((double)first/(double)secound)*100;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	private long getScorePerMinute(long playerScore, long playerGameTime) {
		long gameTimeInMinutes = playerGameTime/60;
		long scorePerMinute = playerScore/gameTimeInMinutes;
		return scorePerMinute;
	}
	
	public String GetTwoDecimal(long first, long secound){
		 
        double d;
        d = (double)first/(double)secound;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	@Override
    public void onBackPressed(){
		Intent i = new Intent(context, PlayerSelector.class);
		startActivity(i);
	}
}
