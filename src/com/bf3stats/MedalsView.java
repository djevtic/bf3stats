package com.bf3stats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public class MedalsView extends Activity {
 
	GridView gridView;

	private Player player;

	private String tag = "djevtic";

	private JSONObject medals;
	String MedalType="";

	private ImageTagFactory imageTagFactory;

	private Loader imageLoader;

	private static ImageManager imageManager;
 
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
 
		super.onCreate(savedInstanceState);
		//Odavde poceto sa image JAR implementacijom
		LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(this);
	    imageManager = new ImageManager(this, settings);
	    imageTagFactory = new ImageTagFactory(this, R.drawable.oldguysscaledmenu);
	    imageTagFactory.setErrorImageId(R.drawable.oldguysscaledmenu);
	    imageLoader = imageManager.getLoader();
	    //Ovde je kraj
		setContentView(R.layout.medalgridview);
		AdView adView = (AdView)this.findViewById(R.id.adView);
	    adView.loadAd(new AdRequest());
		player = Player.getInstance();
		if(player == null){
        	Intent gotoPlayerSelector = new Intent(MedalsView.this, PlayerSelector.class);
            MedalsView.this.startActivity(gotoPlayerSelector);
        }
		medals = player.getMedals();
		fillPage(); 
	}

	//Implementacija image JAR
	public static final ImageManager getImageManager() {
	    return imageManager;
	}
	
	private void fillPage() {
		TextView playerName = (TextView) findViewById(R.id.PlayerName);
		ImageView playerRankImg = (ImageView) findViewById(R.id.PlayerRankImg);
		ImageView playerDogtagBasic = (ImageView) findViewById(R.id.playerdogtagbasic);
		ImageView playerDogtagAdvance = (ImageView) findViewById(R.id.playerdogtagadvance);
		playerName.setText(player.getPlayerName());
		setImage(playerRankImg, player.getPlayerRankImmage());
		setImage(playerDogtagBasic, player.getPlayerBasicDogtagImage());
		setImage(playerDogtagAdvance, player.getPlayerAddvanceDogtagImage());
		//imageManager.load(playerRankImg);
		//new DownloadImageTask(playerRankImg).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerRankImmage());
		//new DownloadImageTask(playerDogtagBasic).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerBasicDogtagImage());
		//new DownloadImageTask(playerDogtagAdvance).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerAddvanceDogtagImage());
		gridView = (GridView) findViewById(R.id.medalGridView);
		gridView.setAdapter(new MedalAdapter(this, MEDALS));
		gridView.setOnItemClickListener(new OnItemClickListener() {
			private long medalCurent;
			private long medalNedded;

			public void onItemClick(AdapterView<?> parent, View v,int position, long id) {
				   	LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
				   	View popupView = layoutInflater.inflate(R.layout.medalpopuplayout, null);
				   	final PopupWindow popupWindow = new PopupWindow(popupView,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				   	String medalID = (String) ((TextView) v.findViewById(R.id.medalID)).getText();
				   	ProgressBar medalProgres = (ProgressBar) popupView.findViewById(R.id.medalProgressBar);
				   	TextView medalLeftToNext = (TextView) popupView.findViewById(R.id.medalLeftForNext);
				   	ImageView medalImage = (ImageView) popupView.findViewById(R.id.medalImagepopup);
				   	TextView medalName = (TextView) popupView.findViewById(R.id.medalNamePopup);
				   	TextView medalDescript = (TextView) popupView.findViewById(R.id.medalDescriptPopup);
				   	try {
				   		setImage(medalImage, medals.getJSONObject(medalID).getString("img_medium"));
				   		//new DownloadImageTask(medalImage).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/medals.getJSONObject(medalID).getString("img_medium"));
				   	} catch (JSONException e) {
				   		// TODO Auto-generated catch block
				   		e.printStackTrace();
				   	}
				   	try {
				   		medalName.setText(medals.getJSONObject(medalID).getString("name"));
				   	} catch (JSONException e) {
				   		// TODO Auto-generated catch block
				   		e.printStackTrace();
				   	}
				   	try {
				   		medalDescript.setText(medals.getJSONObject(medalID).getString("desc"));
				   	} catch (JSONException e) {
				   		// TODO Auto-generated catch block
				   		e.printStackTrace();
				   	}
				   	try {
						MedalType = medals.getJSONObject(medalID).getString("type");
						medalCurent = medals.getJSONObject(medalID).getLong("curr");
						medalNedded = medals.getJSONObject(medalID).getLong("needed");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(MedalType.equals("multi")){
						if((int)medalNedded%50==0){
							medalProgres.setMax(50);
							medalProgres.setProgress((int)medalCurent%50);
							medalLeftToNext.setText("Left to next medal:"+(50-((int)medalCurent)%50));
						}else if((int)medalNedded%30==0){
							medalProgres.setMax(30);
							medalProgres.setProgress((int)medalCurent%30);
							medalLeftToNext.setText("Left to next medal:"+(30-((int)medalCurent)%30));
						}else{
							medalProgres.setMax((int)medalNedded);
							medalProgres.setProgress((int)medalCurent);
							medalLeftToNext.setText("Left to next medal:"+((int)medalNedded-(int)medalCurent));
						}
						
					}else{
						medalProgres.setVisibility(View.INVISIBLE);
						medalLeftToNext.setVisibility(View.INVISIBLE);
					}
				   	Button btnDismiss = (Button)popupView.findViewById(R.id.medalButtonDismissPopup);
				   	btnDismiss.setOnClickListener(new Button.OnClickListener(){
				   		public void onClick(View v) {
				   			// TODO Auto-generated method stub
				   			popupWindow.dismiss();
				   		}});
				   		popupWindow.showAtLocation(gridView, Gravity.CENTER, 0, 0);
	               	}
		});
		
	}

	static final String[] MEDALS = new String[] {"m01",
		"m02",
		"m03",
		"m04",
		"m05",
		"m06",
		"m07",
		"m08",
		"m09",
		"m10",
		"m11",
		"m12",
		"m13",
		"m14",
		"m15",
		"m16",
		"m17",
		"m18",
		"m19",
		"m20",
		"m21",
		"m22",
		"m23",
		"m24",
		"m25",
		"m26",
		"m27",
		"m28",
		"m29",
		"m30",
		"m31",
		"m32",
		"m33",
		"m34",
		"m35",
		"m36",
		"m37",
		"m38",
		"m39",
		"m40",
		"m41",
		"m42",
		"m43",
		"m44",
		"m45",
		"m46",
		"m47",
		"m48",
		"m49",
		"m50",
		"xp2mdom",
		"xp2mgm",
		"xp3mts",
		"xp4mscv"
		};

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;
		private String extStorageDirectory;

	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    protected Bitmap doInBackground(String... urls) {
	    	String filename = null;
	        String urldisplay = urls[0];
			extStorageDirectory = Environment.getExternalStorageDirectory().toString();
	        String[] result = urldisplay.split("/");
	        String urltoDropBox = "http://dl.dropbox.com/u/1480498/bf3/";
	        Bitmap mIcon11 = null;
	        boolean cardStatus = getCardState();
	        if(cardStatus){
	        	File dir = new File(extStorageDirectory+"/bf3statistic");
				try{
				  if(dir.mkdirs()) {
				  } else {
				  }
				}catch(Exception e){
				  e.printStackTrace();
				}
				File newdir = new File(extStorageDirectory+"/bf3statistic/"+urldisplay.substring(0, urldisplay.indexOf('/')));
				try{
					  if(newdir.mkdirs()) {
					  } else {
					  }
					}catch(Exception e){
					  e.printStackTrace();
					}
				filename = urldisplay.substring(urldisplay.indexOf('/')+1);
				File newfile = new File(newdir+"/"+filename);
				if(!newfile.exists()){
					downloadFile(urltoDropBox+urldisplay, newfile);
					try {
			            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
			            mIcon11 = BitmapFactory.decodeStream(in);
			        } catch (Exception e) {
			            Log.e("Error", e.getMessage());
			            e.printStackTrace();
			        }
				}else{
					mIcon11 = BitmapFactory.decodeFile(newfile.getAbsolutePath());
				}
	        }else{
	        	try {
		            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
		            mIcon11 = BitmapFactory.decodeStream(in);
		        } catch (Exception e) {
		            Log.e("Error", e.getMessage());
		            e.printStackTrace();
		        }
	        }
	        /*try {
	            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }*/
	        return mIcon11;
	    }

	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}
	
	public boolean getCardState(){
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		return mExternalStorageAvailable;
	}
	
	private static void downloadFile(String url, File outputFile) {
		  try {
		      URL u = new URL(url);
		      URLConnection conn = u.openConnection();
		      int contentLength = conn.getContentLength();
		      if(contentLength != -1){
		      DataInputStream stream = new DataInputStream(u.openStream());
		      
		        byte[] buffer = new byte[contentLength];
		        stream.readFully(buffer);
		        stream.close();

		        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
		        fos.write(buffer);
		        fos.flush();
		        fos.close();
		      }
		  } catch(FileNotFoundException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  } catch (IOException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  }
		}
	
	private void setImage(ImageView imageView,String url){
		String link = "http://dl.dropbox.com/u/1480498/bf3/"+url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}
	
}
