package com.bf3stats;

import org.json.JSONException;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class PageListMedals extends ListFragment{
	
	public static PageListMedals newInstance(int page) {
		  
		PageListMedals pageListFragment = new PageListMedals();
        Bundle bundle = new Bundle();
        bundle.putInt("key", page);
        pageListFragment.setArguments(bundle);
        return pageListFragment;
    }

	private Player player;
	private CharSequence[] medalValues;
	private Object imageLink;
	private Object medals;
	private Object medalCount;
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;
	private GridView gridView;
	
	@Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
    }  
      
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        int page = getArguments().getInt("key");
        LoadImageLibs();
        player = Player.getInstance();
        View view = null;
    	view = inflater.inflate(R.layout.medalgridview, container, false);
		if(player == null){
        	Intent gotoPlayerSelector = new Intent(getActivity(), PlayerSelector.class);
            getActivity().startActivity(gotoPlayerSelector);
        }
		medals = player.getMedals();
        fillPage(page,view);
        	//loadAdds(view);
        
        return view;  
    }
    
    
	
	private void fillPage(int page, View view) {
		TextView playerName = (TextView) view.findViewById(R.id.PlayerName);
		ImageView playerRankImg = (ImageView) view.findViewById(R.id.PlayerRankImg);
		ImageView playerDogtagBasic = (ImageView) view.findViewById(R.id.playerdogtagbasic);
		ImageView playerDogtagAdvance = (ImageView) view.findViewById(R.id.playerdogtagadvance);
		playerName.setText(player.getPlayerName());
		setImage(playerRankImg, player.getPlayerRankImmage());
		setImage(playerDogtagBasic, player.getPlayerBasicDogtagImage());
		setImage(playerDogtagAdvance, player.getPlayerAddvanceDogtagImage());
		gridView = (GridView) view.findViewById(R.id.medalGridView);
		gridView.setAdapter(new MedalAdapter(getActivity(), MEDALS));		
	}
	
	static final String[] MEDALS = new String[] {"m01",
		"m02",
		"m03",
		"m04",
		"m05",
		"m06",
		"m07",
		"m08",
		"m09",
		"m10",
		"m11",
		"m12",
		"m13",
		"m14",
		"m15",
		"m16",
		"m17",
		"m18",
		"m19",
		"m20",
		"m21",
		"m22",
		"m23",
		"m24",
		"m25",
		"m26",
		"m27",
		"m28",
		"m29",
		"m30",
		"m31",
		"m32",
		"m33",
		"m34",
		"m35",
		"m36",
		"m37",
		"m38",
		"m39",
		"m40",
		"m41",
		"m42",
		"m43",
		"m44",
		"m45",
		"m46",
		"m47",
		"m48",
		"m49",
		"m50",
		"xp2mdom",
		"xp2mgm",
		"xp3mts",
		"xp4mscv"
		};

	private void setImage(ImageView imageView,String url){
		String link = "http://dl.dropbox.com/u/1480498/bf3/"+url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}
	
	private void LoadImageLibs() {
		LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(getActivity());
		imageManager = new ImageManager(getActivity(), settings);
		imageTagFactory = new ImageTagFactory(getActivity(),R.drawable.oldguysgamingscaled);
		imageTagFactory.setErrorImageId(R.drawable.oldguysgamingscaled);
		imageLoader = imageManager.getLoader();
		
	}

}
