package com.bf3stats;

import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;


public class RibonView extends Activity {
 
	GridView gridView;

	private Player player;

	private String tag = "djevtic";

	private JSONObject ribons;

	private ImageManager imageManager;

	private ImageTagFactory imageTagFactory;

	private Loader imageLoader;
  
	@Override
	public void onCreate(Bundle savedInstanceState) {
 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ribongridview);
		AdView adView = (AdView)this.findViewById(R.id.adView);
	    adView.loadAd(new AdRequest());
		player = Player.getInstance();
		if(player == null){
        	Intent gotoPlayerSelector = new Intent(RibonView.this, PlayerSelector.class);
            RibonView.this.startActivity(gotoPlayerSelector);
        }
		ribons = player.getRibons();
		//Odavde poceto sa image JAR implementacijom
				LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(this);
			    imageManager = new ImageManager(this, settings);
			    imageTagFactory = new ImageTagFactory(this, R.drawable.oldguysscaledmenu);
			    imageTagFactory.setErrorImageId(R.drawable.oldguysscaledmenu);
			    imageLoader = imageManager.getLoader();
			    //Ovde je kraj
		fillPage(); 
	}
	
	private void fillPage() {
		TextView playerName = (TextView) findViewById(R.id.PlayerName);
		ImageView playerRankImg = (ImageView) findViewById(R.id.PlayerRankImg);
		ImageView playerDogtagBasic = (ImageView) findViewById(R.id.playerdogtagbasic);
		ImageView playerDogtagAdvance = (ImageView) findViewById(R.id.playerdogtagadvance);
		playerName.setText(player.getPlayerName());
		setImage(playerRankImg, player.getPlayerRankImmage());
		setImage(playerDogtagBasic, player.getPlayerBasicDogtagImage());
		setImage(playerDogtagAdvance, player.getPlayerAddvanceDogtagImage());
		//new DownloadImageTask(playerRankImg).execute("http://dl.dropbox.com/u/1480498/bf3/"+player.getPlayerRankImmage());
		//new DownloadImageTask(playerDogtagBasic).execute("http://dl.dropbox.com/u/1480498/bf3/"+player.getPlayerBasicDogtagImage());
		//new DownloadImageTask(playerDogtagAdvance).execute("http://dl.dropbox.com/u/1480498/bf3/"+player.getPlayerAddvanceDogtagImage());
		gridView = (GridView) findViewById(R.id.ribonGridView);
		gridView.setAdapter(new RibonGridAdapter(this, RIBONS));
		gridView.setOnItemClickListener(new OnItemClickListener() {
			private JSONObject ribon;

			public void onItemClick(AdapterView<?> parent, View v,int position, long id) {
				   	LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
				   	View popupView = layoutInflater.inflate(R.layout.ribonpopuplayout, null);
				   	final PopupWindow popupWindow = new PopupWindow(popupView,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				   	String ribonID = (String) ((TextView) v.findViewById(R.id.ribonID)).getText();
				   	ImageView ribonImage = (ImageView) popupView.findViewById(R.id.ribonImagepopup);
				   	TextView ribonName = (TextView) popupView.findViewById(R.id.ribonNamePopup);
				   	TextView ribonDescript = (TextView) popupView.findViewById(R.id.ribonDescriptPopup);
				   	try {
						ribon = ribons.getJSONObject(ribonID);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				   	try {
				   		setImage(ribonImage, ribon.getString("img_medium"));
				   		//new DownloadImageTask(ribonImage).execute("http://dl.dropbox.com/u/1480498/bf3/"+ribon.getString("img_medium"));
				   	} catch (JSONException e) {
				   		// TODO Auto-generated catch block
				   		e.printStackTrace();
				   	}
				   	try {
				   		ribonName.setText(ribons.getJSONObject(ribonID).getString("name"));
				   	} catch (JSONException e) {
				   		// TODO Auto-generated catch block
				   		e.printStackTrace();
				   	}
				   	try {
				   		ribonDescript.setText(ribons.getJSONObject(ribonID).getString("desc"));
				   	} catch (JSONException e) {
				   		// TODO Auto-generated catch block
				   		e.printStackTrace();
				   	}
				   	Button btnDismiss = (Button)popupView.findViewById(R.id.ribonButtonDismissPopup);
				   	btnDismiss.setOnClickListener(new Button.OnClickListener(){
				   		public void onClick(View v) {
				   			// TODO Auto-generated method stub
				   			popupWindow.dismiss();
				   		}});
				   		popupWindow.showAtLocation(gridView, Gravity.CENTER, 0, 0);
	               	}
		});
		
	}

	static final String[] RIBONS = new String[] {"r01",
		"r02",
		"r03",
		"r04",
		"r05",
		"r06",
		"r07",
		"r08",
		"r09",
		"r10",
		"r11",
		"r12",
		"r13",
		"r14",
		"r15",
		"r16",
		"r17",
		"r18",
		"r19",
		"r20",
		"r21",
		"r22",
		"r23",
		"r24",
		"r25",
		"r26",
		"r27",
		"r28",
		"r29",
		"r30",
		"r31",
		"r32",
		"r33",
		"r34",
		"r35",
		"r36",
		"r37",
		"r38",
		"r39",
		"r40",
		"r41",
		"r42",
		"r43",
		"r44",
		"r45",
		"xp2rgm",
		"xp3rdom",
		"xp3rts",
		"xp4rndom",
		"xp4rnscv",
		"xp4rscav"};

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;

	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    protected Bitmap doInBackground(String... urls) {
	        String urldisplay = urls[0];
	        Bitmap mIcon11 = null;
	        try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }
	        return mIcon11;
	    }

	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}
	
	private void setImage(ImageView imageView,String url){
		String link = "http://dl.dropbox.com/u/1480498/bf3/"+url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}

}
