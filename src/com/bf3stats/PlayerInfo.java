package com.bf3stats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class PlayerInfo extends Activity{
	private Player player;
	private Context context;
	private Database db;
	private String tag = "djevtic";
	private Cursor c;
	private String playerID;
	private boolean idweupdated;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playerstatlayout);
        context = getApplicationContext();
        player = Player.getInstance();
        if(player.getPlayerName()!=null){
        	databaseCheck();
            fillPage();
        }else{
        	Intent i = new Intent(context, PlayerSelector.class);
    		startActivity(i);
        }
	}


	private void databaseCheck() {
		boolean playerExist = false;
    	db = new Database(this);
    	db.open();
   		long id = 0;
   		c = db.getAllPlayers();
		if(c!=null){
			if  (c.moveToFirst()) {
		        do {
		        	if(player.getPlayerName().equals(c.getString(c.getColumnIndex("name")))){
		        		playerExist = true;
		        		playerID = c.getString(c.getColumnIndex("id"));
		        	}
		        }while (c.moveToNext());
		    }
		}
		if(!playerExist){
			id = db.insertPlayer(player.getPlayerName(), player.getPlayerPlatform(), player.getPlayerRankImmage(), player.getPlayerRank(),  ""+(player.getPlayerScore()-player.getPlayerPreviouseRankScore()),
					""+((player.getPlayerNextRankScore()-player.getPlayerPreviouseRankScore())/2),"0");
		}else{
			idweupdated = db.updatePlayer(Long.parseLong(playerID),player.getPlayerName(), player.getPlayerPlatform(), player.getPlayerRankImmage(), player.getPlayerRank(),    ""+(player.getPlayerScore()-player.getPlayerPreviouseRankScore()),
					""+((player.getPlayerNextRankScore()-player.getPlayerPreviouseRankScore())/2),"0");
		}
   		db.close();
		
	}


	private void fillPage() {
		TextView playerName = (TextView) findViewById(R.id.PlayerName);
		ImageView playerRankImg = (ImageView) findViewById(R.id.PlayerRankImg);
		ImageView playerDogtagBasic = (ImageView) findViewById(R.id.playerdogtagbasic);
		ImageView playerDogtagAdvance = (ImageView) findViewById(R.id.playerdogtagadvance);
		playerName.setText(player.getPlayerName());
		new DownloadImageTask(playerRankImg).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerRankImmage());
		new DownloadImageTask(playerDogtagBasic).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerBasicDogtagImage());
		new DownloadImageTask(playerDogtagAdvance).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerAddvanceDogtagImage());
		TextView playerRounds = (TextView) findViewById(R.id.textRounds);
		TextView playerFinishedRounds = (TextView) findViewById(R.id.textFinishedRounds);
		TextView playerSkils = (TextView) findViewById(R.id.textSkils);
		TextView playerWins = (TextView) findViewById(R.id.textWins);
		TextView playerLoses = (TextView) findViewById(R.id.textLoses);
		TextView playerKils = (TextView) findViewById(R.id.textKills);
		TextView playerDeaths = (TextView) findViewById(R.id.textDeths);
		TextView playerHeadshots = (TextView) findViewById(R.id.textHeadshots);
		TextView playerLongestHeadshot = (TextView) findViewById(R.id.textLongestHeadshots);
		TextView playerKillStreak = (TextView) findViewById(R.id.textKillStreak);
		TextView playerShots = (TextView) findViewById(R.id.textShots);
		TextView playerHits = (TextView) findViewById(R.id.textHits);
		TextView playerAcuracy = (TextView) findViewById(R.id.textAccuracy);
		TextView playerAssault = (TextView) findViewById(R.id.textAssault);
		TextView playerEnginer = (TextView) findViewById(R.id.textEnginer);
		TextView playerReacon = (TextView) findViewById(R.id.textReacon);
		TextView playerSuport = (TextView) findViewById(R.id.textSuport);
		TextView playerVehicle = (TextView) findViewById(R.id.textVehicle);
		TextView playerAwards = (TextView) findViewById(R.id.textAwards);
		TextView playerUnlocks = (TextView) findViewById(R.id.textUnlocks);
		TextView playerTeam = (TextView) findViewById(R.id.textTeam);
		TextView playerSquad = (TextView) findViewById(R.id.textSquad);
		TextView playerBonus = (TextView) findViewById(R.id.textBonus);
		TextView playerObjective = (TextView) findViewById(R.id.textObjective);
		TextView playerGeneral = (TextView) findViewById(R.id.textGeneral);
		TextView playerScore = (TextView) findViewById(R.id.textScore);
		TextView playerTime = (TextView) findViewById(R.id.textTime);
		TextView playerSMP = (TextView) findViewById(R.id.textSPM);
		TextView playerKD = (TextView) findViewById(R.id.textKD);
		TextView playerWL = (TextView) findViewById(R.id.textWL);
		TextView playerRank = (TextView) findViewById(R.id.textRank);
		TextView playerKillAssist = (TextView) findViewById(R.id.textKillAssist);
		TextView playerDestroyAssist = (TextView) findViewById(R.id.textDestroyAssist);
		TextView playerDamageAssist = (TextView) findViewById(R.id.textDamageAssist);
		TextView playerSupresionAssist = (TextView) findViewById(R.id.textSupressionAssist);
		TextView playerRevive = (TextView) findViewById(R.id.textRevive);
		TextView playerResuply = (TextView) findViewById(R.id.textResuply);
		TextView playerHeals = (TextView) findViewById(R.id.textHeals);
		TextView playerRepaire = (TextView) findViewById(R.id.textRepaire);
		playerRounds.setText(""+player.getPlayerRoundsPlayed());
		playerFinishedRounds.setText(""+player.getPlayerRoundsFinished());
		playerSkils.setText(""+player.getPlayerSkill());
		playerWins.setText(""+player.getPlayerGameWin());
		playerLoses.setText(""+player.getPlayerGameLose());
		playerKils.setText(""+player.getPlayerGlobalKills());
		playerDeaths.setText(""+player.getPlayerGlobalDeaths());
		playerHeadshots.setText(""+player.getPlayerHeadShoots());
		playerLongestHeadshot.setText(""+player.getPlayerLongestHeadshoot());
		playerKillStreak.setText(""+player.getPlayerKillStreek());
		playerShots.setText(""+player.getPlayerShootsFired());
		playerHits.setText(""+player.getPlayerHits());
		playerAcuracy.setText(""+GetAccuracy(player.getPlayerHits(),player.getPlayerShootsFired())+"%");
		playerAssault.setText(""+player.getPlayerAssaultScore());
		playerEnginer.setText(""+player.getPlayerEnginerScore());
		playerReacon.setText(""+player.getPlayerReaconScore());
		playerSuport.setText(""+player.getPlayerSuportScore());
		playerVehicle.setText(""+player.getPlayerVehicleScore());
		playerAwards.setText(""+player.getPlayerAwardPoints());
		playerUnlocks.setText(""+player.getPlayerUnlocksScore());
		playerTeam.setText(""+player.getPlayerTeamScore());
		playerSquad.setText(""+player.getPlayerSquadScore());
		playerBonus.setText(""+player.getPlayerBonusPoints());
		playerObjective.setText(""+player.getPlayerObjectiveScore());
		playerGeneral.setText(""+player.getPlayerGeneralScore());
		playerScore.setText(""+player.getPlayerScore());
		playerTime.setText(""+getTime(player.getPlayerGameTime()));
		playerSMP.setText(""+getScorePerMinute(player.getPlayerScore(), player.getPlayerGameTime()));
		playerKD.setText(""+GetTwoDecimal(player.getPlayerGlobalKills(), player.getPlayerGlobalDeaths()));
		playerWL.setText(""+GetTwoDecimal(player.getPlayerGameWin(),player.getPlayerGameLose()));
		playerRank.setText(""+player.getPlayerRank());
		playerKillAssist.setText(""+player.getPlayerKillAssist());
		playerDestroyAssist.setText(""+player.getPlayerVehicleDestroyedAssistance());
		playerDamageAssist.setText(""+player.getPlayerDamageAssist());
		playerSupresionAssist.setText(""+player.getPlayerSupression());
		playerRevive.setText(""+player.getPlayerRevives());
		playerResuply.setText(""+player.getPlayerResuply());
		playerHeals.setText(""+player.getPlayerHeals());
		playerRepaire.setText(""+player.getPlayerRepaires());
		
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;
		private String extStorageDirectory;

	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    protected Bitmap doInBackground(String... urls) {
	    	String filename = null;
	        String urldisplay = urls[0];
			extStorageDirectory = Environment.getExternalStorageDirectory().toString();
	        String[] result = urldisplay.split("/");
	        String urltoDropBox = "http://dl.dropbox.com/u/1480498/bf3/";
	        Bitmap mIcon11 = null;
	        boolean cardStatus = getCardState();
	        if(cardStatus){
	        	File dir = new File(extStorageDirectory+"/bf3statistic");
				try{
				  if(dir.mkdirs()) {
				  } else {
				  }
				}catch(Exception e){
				  e.printStackTrace();
				}
				File newdir = new File(extStorageDirectory+"/bf3statistic/"+urldisplay.substring(0, urldisplay.indexOf('/')));
				try{
					  if(newdir.mkdirs()) {
					  } else {
					  }
					}catch(Exception e){
					  e.printStackTrace();
					}
				filename = urldisplay.substring(urldisplay.indexOf('/')+1);
				File newfile = new File(newdir+"/"+filename);
				if(!newfile.exists()){
					downloadFile(urltoDropBox+urldisplay, newfile);
					try {
			            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
			            mIcon11 = BitmapFactory.decodeStream(in);
			        } catch (Exception e) {
			            Log.e("Error", e.getMessage());
			            e.printStackTrace();
			        }
				}else{
					mIcon11 = BitmapFactory.decodeFile(newfile.getAbsolutePath());
				}
	        }else{
	        	try {
		            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
		            mIcon11 = BitmapFactory.decodeStream(in);
		        } catch (Exception e) {
		            Log.e("Error", e.getMessage());
		            e.printStackTrace();
		        }
	        }
	        /*try {
	            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }*/
	        return mIcon11;
	    }

	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}
	
	public boolean getCardState(){
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		return mExternalStorageAvailable;
	}
	
	private static void downloadFile(String url, File outputFile) {
		  try {
		      URL u = new URL(url);
		      URLConnection conn = u.openConnection();
		      int contentLength = conn.getContentLength();
		      if(contentLength != -1){
		      DataInputStream stream = new DataInputStream(u.openStream());
		      
		        byte[] buffer = new byte[contentLength];
		        stream.readFully(buffer);
		        stream.close();

		        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
		        fos.write(buffer);
		        fos.flush();
		        fos.close();
		      }
		  } catch(FileNotFoundException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  } catch (IOException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  }
		}
	
	public String getTime(long timeLong){
		String timeString = "";
		int seconds = (int) (timeLong) % 60 ;
		int minutes = (int) ((timeLong/60) % 60);
		int hours   = (int) ((timeLong / (60*60)) % 24);
		int days   = (int) ((timeLong / (60*60*24)) % 365);
		if(days>0){
			timeString = timeString+days+"d ";
		}
		if(hours>0){
			timeString = timeString+hours+"h ";
		}
		timeString = timeString+minutes+"m "+seconds+"s ";
		return timeString;
	}
	
	public String GetAccuracy(long first, long secound){
		 
        double d;
        d = ((double)first/(double)secound)*100;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	private long getScorePerMinute(long playerScore, long playerGameTime) {
		long gameTimeInMinutes = playerGameTime/60;
		long scorePerMinute = playerScore/gameTimeInMinutes;
		return scorePerMinute;
	}
	
	public String GetTwoDecimal(long first, long secound){
		 
        double d;
        d = (double)first/(double)secound;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	@Override
    public void onBackPressed(){
		Intent i = new Intent(context, PlayerSelector.class);
		startActivity(i);
	}
}
