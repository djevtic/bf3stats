package com.bf3stats;

import org.json.JSONObject;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentRibonList extends ListFragment{
		
		public static FragmentRibonList newInstance(int page) {
			  
			FragmentRibonList pageListFragment = new FragmentRibonList();
	        Bundle bundle = new Bundle();
	        bundle.putInt("key", page);
	        pageListFragment.setArguments(bundle);
	        return pageListFragment;
	    }

		private ImageManager imageManager;
		private ImageTagFactory imageTagFactory;
		private Loader imageLoader;
		private Player player;
		private JSONObject ribons;
		private GridView gridView;
		
		@Override  
	    public void onCreate(Bundle savedInstanceState) {  
	        super.onCreate(savedInstanceState);  
	    }  
	      
	    @Override  
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
	        int page = getArguments().getInt("key");
	        LoadImageLibs();
	        player = Player.getInstance();
	        View view = null;
	    	view = inflater.inflate(R.layout.medalgridview, container, false);
			if(player == null){
	        	Intent gotoPlayerSelector = new Intent(getActivity(), PlayerSelector.class);
	            getActivity().startActivity(gotoPlayerSelector);
	        }
			ribons = player.getRibons();
	        fillPage(page,view);
	        	//loadAdds(view);
	        
	        return view;  
	    }
	    
	    private void fillPage(int page, View view) {
	    	TextView playerName = (TextView) view.findViewById(R.id.PlayerName);
			ImageView playerRankImg = (ImageView) view.findViewById(R.id.PlayerRankImg);
			ImageView playerDogtagBasic = (ImageView) view.findViewById(R.id.playerdogtagbasic);
			ImageView playerDogtagAdvance = (ImageView) view.findViewById(R.id.playerdogtagadvance);
			playerName.setText(player.getPlayerName());
			setImage(playerRankImg, player.getPlayerRankImmage());
			setImage(playerDogtagBasic, player.getPlayerBasicDogtagImage());
			setImage(playerDogtagAdvance, player.getPlayerAddvanceDogtagImage());
			gridView = (GridView) view.findViewById(R.id.ribonGridView);
			gridView.setAdapter(new RibonGridAdapter(getActivity(), RIBONS));
			
		}

		private void setImage(ImageView imageView,String url){
			String link = "http://dl.dropbox.com/u/1480498/bf3/"+url;
			ImageTag tag = imageTagFactory.build(link);
			imageView.setTag(tag);
			imageLoader.load(imageView);
		}
		
		private void LoadImageLibs() {
			LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(getActivity());
			imageManager = new ImageManager(getActivity(), settings);
			imageTagFactory = new ImageTagFactory(getActivity(),R.drawable.oldguysgamingscaled);
			imageTagFactory.setErrorImageId(R.drawable.oldguysgamingscaled);
			imageLoader = imageManager.getLoader();
			
		}
		
		static final String[] RIBONS = new String[] {"r01",
			"r02",
			"r03",
			"r04",
			"r05",
			"r06",
			"r07",
			"r08",
			"r09",
			"r10",
			"r11",
			"r12",
			"r13",
			"r14",
			"r15",
			"r16",
			"r17",
			"r18",
			"r19",
			"r20",
			"r21",
			"r22",
			"r23",
			"r24",
			"r25",
			"r26",
			"r27",
			"r28",
			"r29",
			"r30",
			"r31",
			"r32",
			"r33",
			"r34",
			"r35",
			"r36",
			"r37",
			"r38",
			"r39",
			"r40",
			"r41",
			"r42",
			"r43",
			"r44",
			"r45",
			"xp2rgm",
			"xp3rdom",
			"xp3rts",
			"xp4rndom",
			"xp4rnscv",
			"xp4rscav"};

	

}
