package com.bf3stats;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ComparePlayer extends Activity{
	private Context context;
	private Database db;
	private Cursor c;
	private Button button;
	private CheckBox[] cbs;
	private String tag = "djevtic";
	int numberOfPlayers = 0;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.compareplayerlayout);
        initializeComponents();
        addListenerOnButton();
    }

	private void addListenerOnButton() {
			 
			button = (Button) findViewById(R.id.comparePlayerButton);
	 
			button.setOnClickListener(new OnClickListener() {
	 
				private int playerselectedID;
				private int numberOfChecked;
				private String selectedPlayer1name = "";
				private String selectedPlatform1 = "";
				private String selectedPlayer2name = "";
				private String selectedPlatform2 = "";
				private Intent main;

				@Override
				public void onClick(View arg0) {
					if(numberOfPlayers>0){
						db.open();
					}
					for(int i = 0; numberOfPlayers>i;i++){
						if(cbs[i].isChecked()){
							numberOfChecked ++;
							//playerselectedID = cbs[i].getId();
							
							//if(db.getPlayer((long)playerselectedID)){
							//}
						}
					}
					if(numberOfChecked>2){
						int duration = Toast.LENGTH_LONG;
						Toast toast = Toast.makeText(context, "Please select only two players", duration);
			        	toast.show();
			        	for(int i = 0; numberOfPlayers>i;i++){
			        		cbs[i].setChecked(false);
			        	}
					}else if(numberOfChecked<2){
						int duration = Toast.LENGTH_LONG;
						Toast toast = Toast.makeText(context, "Please select two players", duration);
			        	toast.show();
			        	for(int i = 0; numberOfPlayers>i;i++){
			        		cbs[i].setChecked(false);
			        	}
					}else{
						main = new Intent(context, LoadingComparisomData.class);
						for(int i = 0; numberOfPlayers>i;i++){
							if(cbs[i].isChecked()){
								playerselectedID = cbs[i].getId();
								c = db.getPlayer((long)playerselectedID);
								if(c!=null){
									if  (c.moveToFirst()) {
										if(selectedPlayer1name==""){
											selectedPlayer1name = c.getString(c.getColumnIndex("name"));
											selectedPlatform1 = c.getString(c.getColumnIndex("platform"));
											main.putExtra("player1name", selectedPlayer1name);
											main.putExtra("player1platform", selectedPlatform1);
										}else{
											selectedPlayer2name = c.getString(c.getColumnIndex("name"));
											selectedPlatform2 = c.getString(c.getColumnIndex("platform"));
											main.putExtra("player2name", selectedPlayer2name);
											main.putExtra("player2platform", selectedPlatform2);
										}
									}
								}
							}
			        	}
						db.close();	
						ComparePlayer.this.startActivity(main);
					}
					if(numberOfPlayers>0){
						db.close();
					}
				}	 
			});	 				
	}

	private void initializeComponents() {
		LinearLayout ll = (LinearLayout)findViewById(R.id.compareplayerlayoutinternal);
		db = new Database(this);
		db.open();
		c = db.getAllPlayers();
		if (c != null ) {
			numberOfPlayers = 0;
			if  (c.moveToFirst()) {
		        do {
		            numberOfPlayers++;
		        }while (c.moveToNext());
		    }cbs = new CheckBox[numberOfPlayers];
			if  (c.moveToFirst()) {
				int i = 0;
		        do {
		        	cbs[i]  = new CheckBox(this);
		        	cbs[i].setText(c.getString(c.getColumnIndex("name")));
		        	cbs[i].setId(Integer.parseInt(c.getString(c.getColumnIndex("id"))));
		        	ll.addView(cbs[i]);
				    i++;
		        }while (c.moveToNext());
		    }
			String[] playerIds = new String[numberOfPlayers];			
			c.close();
			db.close();
		}
		
	}
	@Override
    public void onBackPressed(){
		Intent main = new Intent(context, PlayerSelector.class);
		ComparePlayer.this.startActivity(main);
	}
}
