package com.bf3stats;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SlidingDrawer;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class LoadingComparisomData extends Activity{

	private String tag = "djevtic";
	private String platform = "";
	private String name = "";
	Runnable runnable = null;
	Context context = null;
	Player player = null;
	LinearLayout loadingLayout;
	ViewFlipper page;
	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;
	private ProgressBar mProgress;
		/** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.loadingdata);
	        context = getApplicationContext();
	        Bundle extras = getIntent().getExtras();
	        if (extras == null) {
	        		return;
	        		}
	        // Get data via the key
	        String value1 = extras.getString("player1name");
	        String value2 = extras.getString("player1platform");
	        String value3 = extras.getString("player2name");
	        String value4 = extras.getString("player2platform");
	        player = Player.getInstance();
	        if(player.GetPlayers(value1, value2,value3, value4)==1){
	        	player.setConnectionProblems(true);
	        }
	        //Toast creation
	        Context context = getApplicationContext();
	        CharSequence connectionErrorMessage = "Problem with connection to statistic database, please try again later, looks like BF3Stats are down :(";
	        int duration = Toast.LENGTH_LONG;
	        if(player.getConnectionProblems()){
	        	Toast toast = Toast.makeText(context, connectionErrorMessage, duration);
	        	toast.show();
	        }else{
	        	GetData();
	        }
	    }
	    
	    private void GetData() {
	    	new Thread(new Runnable() {
	             public void run() {
	            	 while(!player.weAreGood){	                     
	                 }
	            	 boolean dataGood = player.getData();
	            	 if(dataGood){
	            		 while(player.getJsonString2()==null){
	            			 try {
								Thread.sleep(500);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	            		 }
	            		 if(dataGood!=player.getData2()){
	            			 dataGood= false;
	            		 }
	            	 }
	            	 if(dataGood){	            		 
	            		 Intent i = new Intent(context, Comparison.class);
	            		 startActivity(i);
	            	 }else{
	            		 Intent i = new Intent(context, PlayerSelect.class);
	            		 i.putExtra("error", dataGood);
	            		 startActivity(i);
	            	 }
	             }
	         }).start();
		}
}

