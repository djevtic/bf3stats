package com.bf3stats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.ads.AdRequest;
import com.google.ads.AdView;


import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class ShowStatistic extends ListActivity{
	private static String tag = "djevtic";
	private String platform = "";
	private String statname = "";
	Runnable runnable = null;
	Context context = null;
	Player player = null;
	HashMap immageMap;
	LinearLayout loadingLayout;
	GlobalStatsArrayAdaptor adapter;
	String[] globalStats, MenuStats;
	ViewFlipper page;
	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;
	private ProgressBar mProgress;
	private GlobalStatsArrayAdaptor infoadapter;
	private String[] playerInfostats;
	private GlobalStatsArrayAdaptor globaladapter;
	private String[] playerGlobalstats;
	private GlobalStatsArrayAdaptor combatadapter;
	private GlobalStatsArrayAdaptor scoreadapter;
	private GlobalStatsArrayAdaptor teamadapter;
	private HashMap<String, String> meMap;
	private JSONObject weaponJSON;
	private HashMap<String, String> weaponHash;
	private JSONObject separateWeaponString;
	private JSONArray weaponUnlocks;
	private JSONObject row;
	private ListView list;
	private String weponCode;
	private HashMap<String, String> vehicleHash;
	private String vehicleCode;
	private JSONObject vehicleJSON;
	private JSONObject separateVehicleString;
	private JSONObject separateVehicle;
	private JSONArray unlocksArray;
	private String[] weaponString;
	private JSONArray weaponUnlocksArray;
	private JSONObject weaponUnlocksSeparated;
	private String[] playerInfoUnlock;
	private String stringKit;
	private JSONObject kitJSON;
	private JSONArray unlockedWeaponsArray;
	private JSONObject unlockedWeaponFromKit;
	private JSONObject vehiclekategoriesJSON;
	private JSONArray vehicleunlocksArray;
	private JSONObject vehicleUnlockSingle;
	private JSONObject vehicleunlocksObject;
	private JSONArray vehicleKategoryUnlocksArray;
	private JSONObject medalsJSON;
	private JSONObject medalObject;
	private String extStorageDirectory;
	private String imagestring;
	private Database db;
	private Cursor c;
	private String playerID;
	private boolean idweupdated;
		/** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        Bundle extras = getIntent().getExtras();
	        player = Player.getInstance();
	        if(player == null){
	        	Intent gotoPlayerSelector = new Intent(ShowStatistic.this, PlayerSelector.class);
                ShowStatistic.this.startActivity(gotoPlayerSelector);
	        }
	        if (extras != null) {
	        	String value1 = extras.getString("stats");
		        if (value1 != null) {
		        	statname=value1;
		        }
	        }
	        if(checkForWeaponStat(statname)){
	        	setContentView(R.layout.singleweaponlayout);
	        	AdView adView = (AdView)this.findViewById(R.id.adView);
	    	    adView.loadAd(new AdRequest());
	        }else if(statname.equals("Weapon Stats")){
	        	setContentView(R.layout.weaponscorelayout);
	        	AdView adView = (AdView)this.findViewById(R.id.adView);
	    	    adView.loadAd(new AdRequest());
	        }else if(checkForVehicleStat(statname)){
	        	setContentView(R.layout.singlevehiclelayout);
	        	AdView adView = (AdView)this.findViewById(R.id.adView);
	    	    adView.loadAd(new AdRequest());
	        }else if(checkForUnlocks(statname)){
	        	setContentView(R.layout.statistics);
	        	AdView adView = (AdView)this.findViewById(R.id.adView);
	    	    adView.loadAd(new AdRequest());
	        }else{
	        	setContentView(R.layout.statistics);
	        	AdView adView = (AdView)this.findViewById(R.id.adView);
	    	    adView.loadAd(new AdRequest());
	        	boolean playerExist = false;
	        	db = new Database(this);
	        	db.open();
	       		long id;
	       		c = db.getAllPlayers();
	    		if(c!=null){
	    			if  (c.moveToFirst()) {
	    		        do {
	    		        	if(player.getPlayerName().equals(c.getString(c.getColumnIndex("name")))){
	    		        		playerExist = true;
	    		        		playerID = c.getString(c.getColumnIndex("id"));
	    		        	}
	    		        }while (c.moveToNext());
	    		    }
	    		}
	    		if(!playerExist){
	    			id = db.insertPlayer(player.getPlayerName(), player.getPlayerPlatform(), player.getPlayerRankImmage(), player.getPlayerRank(),  ""+(player.getPlayerScore()-player.getPlayerPreviouseRankScore()),
	    					""+((player.getPlayerNextRankScore()-player.getPlayerPreviouseRankScore())/2),"0");
	    		}else{
	    			idweupdated = db.updatePlayer(Long.parseLong(playerID),player.getPlayerName(), player.getPlayerPlatform(), player.getPlayerRankImmage(), player.getPlayerRank(),    ""+(player.getPlayerScore()-player.getPlayerPreviouseRankScore()),
	    					""+((player.getPlayerNextRankScore()-player.getPlayerPreviouseRankScore())/2),"0");
	    		}
	       		db.close();
	        }
	        context = getApplicationContext();

	        InitializeComponents();


	    }
	    


		private boolean checkForVehicleStat(String statname2) {
			String[] comparestring= getPlayerVehiclesString();
			for(int i=0; comparestring.length>i;i++){
				if(statname2.equals(comparestring[i]))
				{
					return true;
				}
			}
			return false;
		}


		private void InitializeComponents() {
			try{
			immageMap = player.getPlayerhashMap();
			if(!statname.equals("Weapon Stats")){
				if(!checkForWeaponStat(statname)){
					if(!checkForVehicleStat(statname)){
						list = 	getListView();
						TextView playerName = (TextView) findViewById(R.id.PlayerName);
						ImageView playerRankImg = (ImageView) findViewById(R.id.PlayerRankImg);
						ImageView playerDogtagBasic = (ImageView) findViewById(R.id.playerdogtagbasic);
						ImageView playerDogtagAdvance = (ImageView) findViewById(R.id.playerdogtagadvance);
						playerName.setText(player.getPlayerName());
						//player.getImage(player.getPlayerRankImmage(), playerRankImg);
						//player.getImage(player.getPlayerBasicDogtagImage(), playerDogtagBasic);
						//player.getImage(player.getPlayerAddvanceDogtagImage(), playerDogtagAdvance);
						new DownloadImageTask(playerRankImg).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerRankImmage());
						new DownloadImageTask(playerDogtagBasic).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerBasicDogtagImage());
						new DownloadImageTask(playerDogtagAdvance).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/player.getPlayerAddvanceDogtagImage());
					}else {
						list = 	getListView();
						TextView vehiclename = (TextView) findViewById(R.id.namevehicletext);
						TextView vehicleKills = (TextView) findViewById(R.id.singlevehiclekills);
						TextView vehicleTime = (TextView) findViewById(R.id.singlevehicletime);
						TextView vehicleDestroys = (TextView) findViewById(R.id.singlevehicledestroys);
						TextView vehicleDesccription = (TextView) findViewById(R.id.descriptionvehicle);
						vehicleHash = vehicleHashMap();
						String vehicleCode = vehicleHash.get(statname);
						vehicleJSON = player.getVehicles();
						try {
							separateVehicle = vehicleJSON.getJSONObject(vehicleCode);
							vehiclename.setText(separateVehicle.getString("name"));
							vehicleKills.setText(""+separateVehicle.getLong("kills"));
							vehicleTime.setText(getTime(separateVehicle.getLong("time")));
							vehicleDestroys.setText(""+separateVehicle.getLong("destroys"));
							vehicleDesccription.setText(separateVehicle.getString("desc"));
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}else{
					list = 	getListView();
					TextView weaponName = (TextView) findViewById(R.id.singleweaponname);
					TextView weaponTime = (TextView) findViewById(R.id.singleweapontime);
					TextView weaponKills = (TextView) findViewById(R.id.singleweaponkills);
					TextView weaponHits = (TextView) findViewById(R.id.singleweaponhits);
					TextView weaponHeadshots = (TextView) findViewById(R.id.singleweaponheadshots);
					TextView weaponShots = (TextView) findViewById(R.id.singleweaponhots);
					TextView weaponRange = (TextView) findViewById(R.id.singleweaporange);
					TextView weaponSingle = (TextView) findViewById(R.id.singleweaposingle);
					TextView weaponBurst = (TextView) findViewById(R.id.singleweapoburst);
					TextView weaponFull = (TextView) findViewById(R.id.singleweapofull);
					TextView weaponAmmo = (TextView) findViewById(R.id.singleweapoammo);
					TextView weaponDescription = (TextView) findViewById(R.id.singleweapodescription);
					weaponHash = weaponHashMap();
					String weponCode = weaponHash.get(statname);
					weaponJSON = player.getWeapons();
					try {
						separateWeaponString = weaponJSON.getJSONObject(weponCode);
						weaponTime.setText(""+getTime(separateWeaponString.getLong("time")));
						weaponName.setText(separateWeaponString.getString("name"));
						weaponKills.setText(separateWeaponString.getString("kills"));
						weaponHits.setText(""+separateWeaponString.getLong("hits"));
						weaponHeadshots.setText(""+separateWeaponString.getLong("headshots"));
						weaponShots.setText(""+separateWeaponString.getLong("shots"));
						weaponRange.setText(""+separateWeaponString.getString("range"));
						weaponAmmo.setText(separateWeaponString.getString("ammo"));
						weaponDescription.setText(separateWeaponString.getString("desc"));
						if(separateWeaponString.getBoolean("fireModeSingle")){
							weaponSingle.setText("Yes");
						}else{
							weaponSingle.setText("No");
						}
						if(separateWeaponString.getBoolean("fireModeBurst")){
							weaponBurst.setText("Yes");
						}else{
							weaponBurst.setText("No");
						}
						if(separateWeaponString.getBoolean("fireModeAuto")){
							weaponFull.setText("Yes");
						}else{
							weaponFull.setText("No");
						}
						weaponRange.setText(separateWeaponString.getString("range"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			//playerRankImg.setImageResource((Integer) immageMap.get(player.getPlayerRankImmage()));
			if(statname.equals("")){
				MenuStats = getMenuString();
				adapter = new GlobalStatsArrayAdaptor(this, MenuStats, statname);
				setListAdapter(adapter);
	        }else if(statname.equals("Weapon Stats")){
	        	playerInfostats = sortByKill();
	    		teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	    		setListAdapter(teamadapter);
	        }else if(statname.equals("Unlocks")){
	        	playerInfostats = getUnlockMenuString();
	    		teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	    		setListAdapter(teamadapter);
	        }else if(checkForWeaponStat(statname)){
	        	playerInfostats = getWeaponAttachmentString(statname);
	    		teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	    		setListAdapter(teamadapter);
	        }else if(statname.equals("Vehicle Stats")){
	        	playerInfostats = getVehicleByKills();
	        	teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	        	setListAdapter(teamadapter);
	        }else if(statname.equals("Assignments")){
	        	playerInfostats = getAsighmentsString();
	        	teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	        	setListAdapter(teamadapter);
	        }else if(checkForUnlocks(statname)){
	        	playerInfostats = getUnlockString(statname);
	        	teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	        	setListAdapter(teamadapter);
	        }else if(checkForVehicleStat(statname)){
	        	playerInfostats = getVehicleUnlocksString(statname);
	        	teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	        	setListAdapter(teamadapter);
	        }else if(statname.equals("Equipement")){
	        	playerInfostats = getEquipementString();
	        	teamadapter = new GlobalStatsArrayAdaptor(this, playerInfostats, statname);
	        	setListAdapter(teamadapter);
	        }
			}catch(NullPointerException e){
				Intent gotoPlayerSelector = new Intent(ShowStatistic.this, PlayerSelector.class);
				gotoPlayerSelector.putExtra("error", true);
                ShowStatistic.this.startActivity(gotoPlayerSelector);
			}
		}

		private String[] getAsighmentsString() {
			String[] menuList = new String[] {
					"xpma01","xpma02","xpma03","xpma04","xpma05","xpma06","xpma07","xpma08","xpma09","xpma10", 
					"xp2prema01","xp2prema02","xp2prema03","xp2prema04","xp2prema05","xp2prema06","xp2prema07","xp2prema08","xp2prema09","xp2prema10",
					"xp2ma01","xp2ma02","xp2ma03","xp2ma04","xp2ma05","xp2ma06","xp2ma07","xp2ma08","xp2ma09","xp2ma10","xp3prema01","xp3prema02","xp3prema03"
					,"xp3prema04","xp3prema05","xp3prema06","xp3prema07","xp3prema08","xp3prema09","xp3prema10","xp3ma01","xp3ma02","xp3ma03","xp3ma04","xp3ma05"
					,"xp4ma01","xp4ma02","xp4ma03","xp4ma04","xp4ma05","xp4ma06","xp4ma07","xp4ma08","xp4ma09", "xp4ma10", "xp4prema01","xp4prema02","xp4prema03"
					,"xp4prema04","xp4prema05"
					};
			return menuList;
		}



		private String[] getUnlockString(String statname2) {
			// "Weapon Unlocks","Weapon","Vehicle Unlocks","Medals To Unlock"
			if(statname2.equals("Weapon Unlocks")){
				return getWeaponUnlockString();
			//}else if(statname2.equals("Weapon")){
				//return getWeaponUnlockedString();
			}else if(statname2.equals("Vehicle Unlocks")){
				return getVehicleUnlockString();
			}else if(statname2.equals("Medals To Unlock")){
				return getMedalUnlockString();
			}
			return null;
		}



		private String[] getMedalUnlockString() {
			int numberOfElements = 0;
			String[] medalString = MEDALS;
			medalsJSON = player.getMedals();
			for(int i = 0;medalString.length>i;i++){
				try {
					medalObject = medalsJSON.getJSONObject(medalString[i]);
					if(medalObject.getString("type").equals("multi")){
						numberOfElements++;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			int tempint = 0;
			String[][] temp = new String[numberOfElements][2];
			for(int i = 0;medalString.length>i;i++){
				try {
					medalObject = medalsJSON.getJSONObject(medalString[i]);
					if(medalObject.getString("type").equals("multi")){
						temp[tempint][0]=medalString[i];
						temp[tempint][1]=""+getPointsLeft(medalObject.getLong("needed"),medalObject.getLong("curr"));
						tempint++;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			Arrays.sort(temp, new Comparator<String[]>() {
	            public int compare(final String[] entry1, final String[] entry2) {
	                final Long time1 = Long.valueOf(entry1[1]);
	                final Long time2 = Long.valueOf(entry2[1]);
	                return time1.compareTo(time2);
	            }
	        });
			String[] returnStringArray = new String[temp.length];
			for(int element = 0;temp.length>element;element++){
				returnStringArray[element]=temp[element][0]+"/"+temp[element][1];
			}
			return returnStringArray;
			//return null;
		}



		private String[] getVehicleUnlockString() {
			int numberOfElements = 0;
			String[] vehicleString = getPlayerVehiclekategory();
			String[][] vehicleKillsArray = new String [vehicleString.length][2];
			vehiclekategoriesJSON = player.getVehiclekategories();
			for(int i = 0; vehicleString.length>i;i++){
				try {
					vehicleunlocksObject = vehiclekategoriesJSON.getJSONObject(vehicleString[i]);
					vehicleKategoryUnlocksArray = vehicleunlocksObject.getJSONArray("unlocks");
					for(int y = 0; vehicleKategoryUnlocksArray.length()>y;y++){
						vehicleUnlockSingle = vehicleKategoryUnlocksArray.getJSONObject(y);
						if(vehicleUnlockSingle.getLong("needed")>vehicleUnlockSingle.getLong("curr")){
							numberOfElements++;
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			String[][] temp = new String[numberOfElements][3];
			int tempint = 0;
			for(int i = 0; vehicleString.length>i;i++){
				try {
					vehicleunlocksObject = vehiclekategoriesJSON.getJSONObject(vehicleString[i]);
					vehicleKategoryUnlocksArray = vehicleunlocksObject.getJSONArray("unlocks");
					for(int y = 0; vehicleKategoryUnlocksArray.length()>y;y++){
						vehicleUnlockSingle = vehicleKategoryUnlocksArray.getJSONObject(y);
						if(vehicleUnlockSingle.getLong("needed")>vehicleUnlockSingle.getLong("curr")){
							temp[tempint][0]=vehicleString[i];
							temp[tempint][1]=vehicleUnlockSingle.getString("name");
							temp[tempint][2]=""+getPointsLeft(vehicleUnlockSingle.getLong("needed"),vehicleUnlockSingle.getLong("curr"));
							tempint++;
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Arrays.sort(temp, new Comparator<String[]>() {
	            public int compare(final String[] entry1, final String[] entry2) {
	                final Long time1 = Long.valueOf(entry1[2]);
	                final Long time2 = Long.valueOf(entry2[2]);
	                return time1.compareTo(time2);
	            }
	        });
			String[] returnStringArray = new String[temp.length];
			for(int element = 0;temp.length>element;element++){
				returnStringArray[element]=temp[element][0]+"/"+temp[element][1]+"/"+temp[element][2];
			}
			return returnStringArray;
			//return null;
		}



		private String[] getWeaponUnlockString() {
			int numberOfElements = 0;
			weaponString = getPlayerWeaponString();
			//Check weapons and their unlocks
			String[][] weaponKillsArray = new String [weaponString.length][2];
			for(int i = 0; weaponString.length>i;i++){
				weaponHash = weaponHashMap();
				weponCode = weaponHash.get(weaponString[i]);
				try {
					weaponJSON = player.getWeapons();
					separateWeaponString = weaponJSON.getJSONObject(weponCode);
					weaponUnlocksArray = separateWeaponString.getJSONArray("unlocks");
					for(int c = 0;weaponUnlocksArray.length()>c;c++){
						weaponUnlocksSeparated = weaponUnlocksArray.getJSONObject(c);
						if(weaponUnlocksSeparated.getLong("needed")>weaponUnlocksSeparated.getLong("curr")){
							numberOfElements++;
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			String[][] temp = new String[numberOfElements][3];
			int tempint = 0;
			for(int i = 0; weaponString.length>i;i++){
				weaponHash = weaponHashMap();
				weponCode = weaponHash.get(weaponString[i]);
				try {
					weaponJSON = player.getWeapons();
					separateWeaponString = weaponJSON.getJSONObject(weponCode);
					weaponUnlocksArray = separateWeaponString.getJSONArray("unlocks");
					for(int c = 0;weaponUnlocksArray.length()>c;c++){
						weaponUnlocksSeparated = weaponUnlocksArray.getJSONObject(c);
						if(weaponUnlocksSeparated.getLong("needed")>weaponUnlocksSeparated.getLong("curr")){
							temp[tempint][0]=weponCode;
							temp[tempint][1]=weaponUnlocksSeparated.getString("name");
							temp[tempint][2]=""+getKillsleft(weaponUnlocksSeparated.getLong("needed"),weaponUnlocksSeparated.getLong("curr"));
							tempint++;
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			Arrays.sort(temp, new Comparator<String[]>() {
	            public int compare(final String[] entry1, final String[] entry2) {
	                final Long time1 = Long.valueOf(entry1[2]);
	                final Long time2 = Long.valueOf(entry2[2]);
	                return time1.compareTo(time2);
	            }
	        });
			String[] returnStringArray = new String[temp.length];
			for(int element = 0;temp.length>element;element++){
				returnStringArray[element]=temp[element][0]+"/"+temp[element][1]+"/"+temp[element][2];
			}
			return returnStringArray;
		}

		private String getPointsLeft(long long1,long long2){
			long d;
	        d = (long1-long2);  // this will helps you to always keeps in two decimal places
	        if(d<1){
	        	return ""+50;
	        }else{
	        	return ""+d;
	        }
		}


		private String getKillsleft(long long1, long long2) {
			long d;
	        d = (long1-long2);  // this will helps you to always keeps in two decimal places
	        if(d<1){
	        	return ""+50;
	        }else{
	        	return ""+d;
	        }
		}



		private String[] getVehicleUnlocksString(String statname2) {
			String[] menuList = null;
			String vehicleType = null;
			vehicleHash = vehicleHashMap();
			String vehicleCode = vehicleHash.get(statname2);
			vehicleJSON = player.getVehicles();
			try {
				separateVehicle = vehicleJSON.getJSONObject(vehicleCode);
				vehicleType = separateVehicle.getString("category");
					int numberOgUnlocks = 0;
					unlocksArray = player.getingVehicleUnlocks().getJSONObject(vehicleType).getJSONArray("unlocks");
					if(unlocksArray.length()>0){
						for(int i = 0 ; i < unlocksArray.length(); i++){
							row = unlocksArray.getJSONObject(i);
							numberOgUnlocks++;
						}
					}
					int y = 0;
					menuList = new String[numberOgUnlocks];
					for(int i = 0 ; i < unlocksArray.length(); i++){
						row = unlocksArray.getJSONObject(i);
							menuList[y]=row.getString("name");
							y++;
					}
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			if(menuList==null){
				menuList=new String[0];
			}
			return menuList;
		}



		private String[] sortByKill() {
			String[] weaponString = getPlayerWeaponString();
			String[][] weaponKillsArray = new String [weaponString.length][2];
			for(int i = 0; weaponString.length>i;i++){
				weaponHash = weaponHashMap();
				weponCode = weaponHash.get(weaponString[i]);
				try {
					weaponJSON = player.getWeapons();
					separateWeaponString = weaponJSON.getJSONObject(weponCode);
					weaponKillsArray[i][1] = separateWeaponString.getString("kills");
					weaponKillsArray[i][0] = weaponString[i];
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			String[][] temp = new String[1][2];
			for(int counter=0; weaponString.length>counter; counter++) { //Loop once for each element in the array.
		            for (int i = 0; i < weaponKillsArray.length - 1; i++){
		        		int min = i;

		        		for (int  j = i + 1; j < weaponKillsArray.length; j++){
		        			if (Long.parseLong(weaponKillsArray[j][1]) > Long.parseLong(weaponKillsArray[min][1])){

		        				min = j;

		        			}

		        		}
		        		if (i != min){ 
		        			temp[0][0] = weaponKillsArray[i][0];
		        			temp[0][1] = weaponKillsArray[i][1];
		        			//int tmp = data[i];
		        			weaponKillsArray[i][0]=weaponKillsArray[min][0];
		        			weaponKillsArray[i][1]=weaponKillsArray[min][1];
		        			//data[i] = data[min];
		        			weaponKillsArray[min][0]=temp[0][0];
		        			weaponKillsArray[min][1]=temp[0][1];
		        			//data[min] = tmp;

		    	      }

		        	}
		        }
			String[] arrayToReturn= new String[weaponKillsArray.length];
			for(int i = 0; weaponKillsArray.length>i;i++){
				arrayToReturn[i]=weaponKillsArray[i][0];
			}
			return arrayToReturn;
		}



		private boolean checkForWeaponStat(String statname2) {
			String[] comparestring= getPlayerWeaponString();
			for(int i=0; comparestring.length>i;i++){
				if(statname2.equals(comparestring[i]))
				{
					return true;
				}
			}
			return false;
		}



		public static String[] getPlayerWeaponString() {
			String[] menuList = new String[] {"MP7", "G17C", "M98B", "SG553","870MCS","MP443 SUPP.","RPG-7V2", "MTAR-21", "KH2002", "AKS-74u", "M1911 SUPP.", 
					"M27 IAR", "G36C", ".44 MAGNUM", "AS VAL", "DAO-12", "M320", "UMP-45", "SA-18 IGLA", "SCAR-H", "MP412 REX", "M416", "AK-74M", "PKP PECHENEG", 
					"M60E4", "M240B", "M16A4", "M1911 TACT.", "PP-2000", "G3A3", "M4A1", "AEK-971", "AUG A3", "MK11 MOD 0", "AN-94", "SPAS-12", ".44 SCOPED", 
					"USAS-12", "MP443", "RPK-74M", "ACB-90", "L85A2", "SV98", "M40A5", "SCAR-L", "ACW-R", "MG36", "SAIGA 12K", "FAMAS", "M1911 S-TAC", "93R", 
					"M9 SUPP.", "M1911", "SVD", "MK3A1", "G18 SUPP.", "M249", "G17C SUPP.", "QBU-88", "M9", "M26 MASS", "JNG-90", "L86A2", "LSAT", "SMAW", "P90", 
					"M417", "A-91", "SKS", "L96", "PP-19", "TYPE 88 LMG", "QBZ-95B", "M9 TACT.", "G18", "F2000", "FGM-148 JAVELIN", "M5K", "MP443 TACT.", "G53", 
					"FIM-92 STINGER", "M39 EMR", "M1014", "PDW-R", "QBB-95", "XBOW SCOPED", "XBOW"};
			Arrays.sort(menuList);
			return menuList;
		}
		
		public static String[] getEquipementString(){
			String[] menuList = new String[]{"RADIO BEACON","M67 GRENADE","REPAIR TOOL","C4 EXPLOSIVES","DEFIBRILLATOR","T-UGS","M15 AT MINE","M18 CLAYMORE","EOD BOT","SOFLAM","M224 MORTAR","MAV"};
			Arrays.sort(menuList);
			return menuList;
		}
		
		public static String[] getPlayerVehiclekategory(){
			String[] menuList = new String[] {
					"vehicleaa"
					, "vehicleah"
					,"vehicleifv"
					,"vehiclejf"
					,"vehiclembt"
					,"vehiclesh"
					//,"vehicleja"
					};
			return menuList;
		}
		public static String[] getPlayerVehiclesString(){
			String[] menuList = new String[] {                   "UH-1Y VENOM",
	                   "AAV-7A1 AMTRAC",
	                   "M220 TOW LAUNCHER",
	                   "F-35",
	                   "T-90A",
	                   "SKID LOADER",
	                   "LAV-AD",
	                   "MI-28 HAVOC",
	                   "GAZ-3937 VODNIK",
	                   "DPV",
	                   "9K22 TUNGUSKA-M",
	                   "AH-1Z VIPER",
	                   "BMP-2M",
	                   "LAV-25",
	                   "M1 ABRAMS",
	                   "RHIB BOAT",
	                   "A-10 THUNDERBOLT",
	                   "KA-60 KASATKA",
	                   "PANTSIR-S1",
	                   "F/A-18E SUPER HORNET",
	                   "SU-25TM FROGFOOT",
	                   "M1114 HMMWV",
	                   "CENTURION C-RAM",
	                   "VDV BUGGY",
	                   "Z-11W",
	                   "9M133 KORNET LAUNCHER",
	                   "GROWLER ITV",
	                   "SU-35BM FLANKER-E",
	                   "BTR-90",
	                   "AH-6J LITTLE BIRD",
	                   "M1128",
	                   "BM23",
	                   "QUAD BIKE",
	                   "SPRUT-SD",
	                   "GUNSHIP",
	                   "M142",
	                   "RHINO",
	                   "PHOENIX",
	                   "BARSUK"};
			Arrays.sort(menuList);
			return menuList;
		}
		
		public String[] getVehicleByKills(){
			String[] vehicleString = getPlayerVehiclesString();
			String[][] vehicleKillsArray = new String [vehicleString.length][2];
			for(int i = 0; vehicleString.length>i;i++){
				vehicleHash = vehicleHashMap();
				vehicleCode = vehicleHash.get(vehicleString[i]);
				try {
					vehicleJSON = player.getVehicles();
					separateVehicleString = vehicleJSON.getJSONObject(vehicleCode);
					vehicleKillsArray[i][1] = separateVehicleString.getString("kills");
					vehicleKillsArray[i][0] = vehicleString[i];
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			String[][] temp = new String[1][2];
			for(int counter=0; vehicleString.length>counter; counter++) { //Loop once for each element in the array.
		            for (int i = 0; i < vehicleKillsArray.length - 1; i++){
		        		int min = i;

		        		for (int  j = i + 1; j < vehicleKillsArray.length; j++){
		        			if (Long.parseLong(vehicleKillsArray[j][1]) > Long.parseLong(vehicleKillsArray[min][1])){

		        				min = j;

		        			}

		        		}
		        		if (i != min){ 
		        			temp[0][0] = vehicleKillsArray[i][0];
		        			temp[0][1] = vehicleKillsArray[i][1];
		        			//int tmp = data[i];
		        			vehicleKillsArray[i][0]=vehicleKillsArray[min][0];
		        			vehicleKillsArray[i][1]=vehicleKillsArray[min][1];
		        			//data[i] = data[min];
		        			vehicleKillsArray[min][0]=temp[0][0];
		        			vehicleKillsArray[min][1]=temp[0][1];
		        			//data[min] = tmp;

		    	      }

		        	}
		        }
			String[] arrayToReturn= new String[vehicleKillsArray.length];
			for(int i = 0; vehicleKillsArray.length>i;i++){
				arrayToReturn[i]=vehicleKillsArray[i][0];
			}
			return arrayToReturn;
		}

		
		private HashMap<String, String> vehicleHashMap() {
				meMap = new HashMap<String, String>();
                 meMap.put("UH-1Y VENOM","trUH1");
                 meMap.put("AAV-7A1 AMTRAC","trAAV");
                 meMap.put("M220 TOW LAUNCHER","atTOW");
                 meMap.put("F-35","jfx1F35JSF");
                 meMap.put("T-90A","mbtT90");
                 meMap.put("SKID LOADER","trSkid");
                 meMap.put("LAV-AD","aaLAV");
                 meMap.put("MI-28 HAVOC","ahMi28");
                 meMap.put("GAZ-3937 VODNIK","trVod");
                 meMap.put("DPV","trDpv");
                 meMap.put("9K22 TUNGUSKA-M","aa9k22");
                 meMap.put("AH-1Z VIPER","ahAH1Z");
                 meMap.put("BMP-2M","ifvBMP");
                 meMap.put("LAV-25","ifvLAV");
                 meMap.put("M1 ABRAMS","mbtM1A");
                 meMap.put("RHIB BOAT","trRIB");
                 meMap.put("A-10 THUNDERBOLT","jaA10");
                 meMap.put("KA-60 KASATKA","trKA60");
                 meMap.put("PANTSIR-S1","saaPant");
                 meMap.put("F/A-18E SUPER HORNET","jfF18");
                 meMap.put("SU-25TM FROGFOOT","jaSU25");
                 meMap.put("M1114 HMMWV","trHum");
                 meMap.put("CENTURION C-RAM","saaCRAM");
                 meMap.put("VDV BUGGY","trVDV");
                 meMap.put("Z-11W","shz11");
                 meMap.put("9M133 KORNET LAUNCHER","sKorn");
                 meMap.put("GROWLER ITV","trITV");
                 meMap.put("SU-35BM FLANKER-E","jfSu35");
                 meMap.put("BTR-90","ifvBTR90");
                 meMap.put("AH-6J LITTLE BIRD","shAH6");
                 meMap.put("M1128","mtdM1128");
                 meMap.put("BM23","martBM23");
                 meMap.put("QUAD BIKE","trQB");
                 meMap.put("SPRUT-SD","mtdSPRUTSD");
                 meMap.put("GUNSHIP","vmaG");
                 meMap.put("M142","martHIM");
                 meMap.put("BARSUK","trVodnM");
                 meMap.put("PHOENIX","trHmvM");
                 meMap.put("RHINO","trVanM");
			return meMap;
		}



		private String[] getWeaponAttachmentString(String weaponName){
			String weponCode = weaponHash.get(weaponName);
			String[] menuList = null;
			try {
				separateWeaponString = weaponJSON.getJSONObject(weponCode);
				weaponUnlocks = separateWeaponString.getJSONArray("unlocks");
				int numberOgUnlocks = 0;
				if(weaponUnlocks.length()>0){
					for(int i = 0 ; i < weaponUnlocks.length(); i++){
						row = weaponUnlocks.getJSONObject(i);
						//if(row.getLong("needed")==row.getLong("curr")){
							numberOgUnlocks++;
						//}
					}
				}
				int y = 0;
				menuList = new String[numberOgUnlocks];
				if(weaponUnlocks.length()>0){
					for(int i = 0 ; i < weaponUnlocks.length(); i++){
						row = weaponUnlocks.getJSONObject(i);
						//if(row.getLong("needed")==row.getLong("curr")){
							numberOgUnlocks++;
							menuList[y]=row.getString("name");
							y++;
						//}
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return menuList;
		}

		
		@Override
		protected void onListItemClick(ListView l, View v, int position, long id) {
			super.onListItemClick(l, v, position, id);
			// Get the item that was clicked
			Object o = this.getListAdapter().getItem(position);
			String keyword = o.toString();
			if(keyword.equals("Player Info")){
				Intent playerInfoIntent = new Intent(ShowStatistic.this, PlayerInfo.class);
                ShowStatistic.this.startActivity(playerInfoIntent);
			}else if(keyword.equals("Medals")){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, MedalsView.class);
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if(checkForWeaponStat(keyword)){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, ShowStatistic.class);
				playerGeneralStats.putExtra("stats", keyword);
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (keyword.equals("Vehicle Stats")){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, ShowStatistic.class);
				playerGeneralStats.putExtra("stats", "Vehicle Stats");
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (keyword.equals("Weapon Stats")){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, ShowStatistic.class);
				playerGeneralStats.putExtra("stats", "Weapon Stats");
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (checkForVehicleStat(keyword)){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, ShowStatistic.class);
				playerGeneralStats.putExtra("stats", keyword);
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (keyword.equals("Ribons")){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, RibonView.class);
				playerGeneralStats.putExtra("stats", keyword);
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (keyword.equals("Unlocks")){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, ShowStatistic.class);
				playerGeneralStats.putExtra("stats", "Unlocks");
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (keyword.equals("Assignments")){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, ShowStatistic.class);
				playerGeneralStats.putExtra("stats", "Assignments");
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (checkForUnlocks(keyword)){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, ShowStatistic.class);
				playerGeneralStats.putExtra("stats", keyword);
                ShowStatistic.this.startActivity(playerGeneralStats);
			}else if (keyword.equals("About")){
				Intent playerInfoIntent = new Intent(ShowStatistic.this, About.class);
                ShowStatistic.this.startActivity(playerInfoIntent);
			}else if (keyword.equals("TabTest")){
				Intent playerInfoIntent = new Intent(ShowStatistic.this, Tabs.class);
                ShowStatistic.this.startActivity(playerInfoIntent);
			}

		}
		
		private boolean checkForUnlocks(String keyword) {
			String[] comparestring= getUnlockMenuString();
			for(int i=0; comparestring.length>i;i++){
				if(keyword.equals(comparestring[i]))
				{
					return true;
				}
			}
			return false;
		}



		private String[] getMenuString() {
			String[] menuList = new String[] {"Player Info", "TabTest",/*"General Stats", "Combat Stats", "Scores","Team Stats",*/"Weapon Stats","Vehicle Stats","Medals","Ribons","Unlocks","Assignments","About"};
			return menuList;
		}
		
		private String[] getUnlockMenuString(){
			String[] menuList = new String[]{"Weapon Unlocks",/*"Weapon",*/"Vehicle Unlocks","Medals To Unlock"};
			return menuList;
		}

		@Override
	       public void onBackPressed(){
			if(statname.equals("")){
				Intent i = new Intent(context, PlayerSelector.class);
				startActivity(i);
	        }else if (checkForWeaponStat(statname)){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, Tabs.class);
				playerGeneralStats.putExtra("stats", 1);
                ShowStatistic.this.startActivity(playerGeneralStats);
	        }else if (checkForVehicleStat(statname)){
				Intent playerGeneralStats = new Intent(ShowStatistic.this, Tabs.class);
				playerGeneralStats.putExtra("stats", 2);
                ShowStatistic.this.startActivity(playerGeneralStats);
	        }else{ // if(statname.equals("Player Info")){
	        	Intent i = new Intent(context, PlayerSelector.class);
				startActivity(i);
	        }/*else if(statname.equals("General Stats")){
	        	playerInfostats = getPlayerGlobalString();
	    		globaladapter = new PlayerGlobalArrayAdaptor(this, playerInfostats);
	    		setListAdapter(globaladapter);
	        }else if(statname.equals("Combat Stats")){
	        	playerInfostats = getPlayerCombatString();
	    		combatadapter = new PlayerCombatArrayAdaptor(this, playerInfostats);
	    		setListAdapter(combatadapter);
	        }else if(statname.equals("Scores")){
	        	playerInfostats = getPlayerScoreString();
	    		scoreadapter = new PlayerScoreArrayAdaptor(this, playerInfostats);
	    		setListAdapter(scoreadapter);
	        }else if(statname.equals("Team Stats")){
	        	playerInfostats = getPlayerTeamString();
	    		teamadapter = new PlayerTeamScoreArrayAdaptor(this, playerInfostats);
	    		setListAdapter(teamadapter);
	        }*/
	       }

		private String[] getGlobalStatString() {
			String[] globalStatList = new String[] {"Kills", "Deths", "K/D","Wins", "Losses","W/L","Shots","Hits","Acuracy","Headshots","Longest Headshot"};
			// TODO Auto-generated method stub
			return globalStatList;
		}

		private String[] getPlayerInfoString() {
			String[] infoList = new String[] {"Country", "Score", "Time","SPM","K/D", "W/L", "Rank"};
			return infoList;
		}
		
		private String[] getPlayerGlobalString() {
			String[] infoList = new String[] {"Round Player", "Finished Rounds", "Skills","Wins","Losses"};
			return infoList;
		}
		
		private String[] getPlayerCombatString() {
			String[] infoList = new String[] {"Kills", "Deaths", "Headshots","Longest Headshot","Kill Streak", "Shots", "Hits", "Accuracy"};
			return infoList;
		}
		
		private String[] getPlayerScoreString() {
			String[] infoList = new String[] {"Assault", "Enginer", "Reacon","Suport","Vehicle", "Awards", "Unlocks","Team","Squad","Bonus","Objective","General"};
			return infoList;
		}
		
		private String[] getPlayerTeamString() {
			String[] infoList = new String[] {"Kill Assist", "Destroy Assist", "Damage Assist","Suppression Assist","Revive", "Resuply", "Heals","Repair"};
			return infoList;
		}
		
		
		public void downloadImageFiles() {
			String filename = null;
			String fileURL = "http://dl.dropbox.com/u/1480498/bf3/";
			//String[] immagesarray = imageDownloadArray();
			extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			File dir = new File(extStorageDirectory+"/bf3statistic");
			try{
			  if(dir.mkdirs()) {
			  } else {
			  }
			}catch(Exception e){
			  e.printStackTrace();
			}
			//for(int i=0; immagesarray.length>i;i++){
				//int y = immagesarray[i].indexOf('/');
				File newdir = new File(extStorageDirectory+"/bf3statistic/"+imagestring.substring(0, imagestring.indexOf('/')));
				try{
					  if(newdir.mkdirs()) {
					  } else {
					  }
					}catch(Exception e){
					  e.printStackTrace();
					}
				filename = imagestring.substring(imagestring.indexOf('/')+1);
				File newfile = new File(newdir+filename);
				if(!newfile.exists()){
					downloadFile(fileURL+imagestring, newfile);
				}else{
					//File exist
				}
				
			//}
			
		}
		
		private static void downloadFile(String url, File outputFile) {
			  try {
			      URL u = new URL(url);
			      URLConnection conn = u.openConnection();
			      int contentLength = conn.getContentLength();
			      if(contentLength != -1){
			      DataInputStream stream = new DataInputStream(u.openStream());
			      
			        byte[] buffer = new byte[contentLength];
			        stream.readFully(buffer);
			        stream.close();

			        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
			        fos.write(buffer);
			        fos.flush();
			        fos.close();
			      }
			  } catch(FileNotFoundException e) {
				  e.printStackTrace();
			      return; // swallow a 404
			  } catch (IOException e) {
				  e.printStackTrace();
			      return; // swallow a 404
			  }
			}
		
		
		
		public boolean getCardState(){
			boolean mExternalStorageAvailable = false;
			boolean mExternalStorageWriteable = false;
			String state = Environment.getExternalStorageState();

			if (Environment.MEDIA_MOUNTED.equals(state)) {
			    // We can read and write the media
			    mExternalStorageAvailable = mExternalStorageWriteable = true;
			} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			    // We can only read the media
				mExternalStorageAvailable = mExternalStorageWriteable = false;
			} else {
			    // Something else is wrong. It may be one of many other states, but all we need
			    //  to know is we can neither read nor write
			    mExternalStorageAvailable = mExternalStorageWriteable = false;
			}
			return mExternalStorageAvailable;
		}
		
		public void getImage(String link, ImageView imageview){
			//String imagePath = Environment.getExternalStorageDirectory().toString() + PATH_TO_IMAGE;
			/*BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeResource(getResources(), R.drawable.m30, options);
			int imageHeight = options.outHeight;
			int imageWidth = options.outWidth;
			String imageType = options.outMimeType;
			*/
			//Bitmap bmImg = BitmapFactory.decodeFile(extStorageDirectory+"/bf3statistic/"+link.substring(link.indexOf('/')+1));
			//bmImg = decodeFile(extStorageDirectory+"/bf3statistic/"+link.substring(link.indexOf('/')+1));
			//Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
			Bitmap bmImg = BitmapFactory.decodeFile(extStorageDirectory+"/bf3statistic/"+link/*.substring(link.indexOf('/')+1))*/);
			if(bmImg==null){
				imageview.setImageResource(R.drawable.oldguysscaled);
			}else{
				imageview.setImageBitmap(bmImg);
			}
		}
		
		private Bitmap decodeFile(File f){
		    try {
		        //Decode image size
		        BitmapFactory.Options o = new BitmapFactory.Options();
		        o.inJustDecodeBounds = true;
		        BitmapFactory.decodeStream(new FileInputStream(f),null,o);

		        //The new size we want to scale to
		        final int REQUIRED_SIZE=50;

		        //Find the correct scale value. It should be the power of 2.
		        int scale=1;
		        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
		            scale*=2;

		        //Decode with inSampleSize
		        BitmapFactory.Options o2 = new BitmapFactory.Options();
		        o2.inSampleSize=scale;
		        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		    } catch (FileNotFoundException e) {}
		    return null;
		}
		
		private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		    ImageView bmImage;

		    public DownloadImageTask(ImageView bmImage) {
		        this.bmImage = bmImage;
		    }

		    protected Bitmap doInBackground(String... urls) {
		    	String filename = null;
		        String urldisplay = urls[0];
				extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		        String[] result = urldisplay.split("/");
		        String urltoDropBox = "http://dl.dropbox.com/u/1480498/bf3/";
		        Bitmap mIcon11 = null;
		        boolean cardStatus = getCardState();
		        if(cardStatus){
		        	File dir = new File(extStorageDirectory+"/bf3statistic");
					try{
					  if(dir.mkdirs()) {
					  } else {
					  }
					}catch(Exception e){
					  e.printStackTrace();
					}
					File newdir = new File(extStorageDirectory+"/bf3statistic/"+urldisplay.substring(0, urldisplay.indexOf('/')));
					try{
						  if(newdir.mkdirs()) {
						  } else {
						  }
						}catch(Exception e){
						  e.printStackTrace();
						}
					filename = urldisplay.substring(urldisplay.indexOf('/')+1);
					File newfile = new File(newdir+"/"+filename);
					if(!newfile.exists()){
						downloadFile(urltoDropBox+urldisplay, newfile);
						try {
				            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
				            mIcon11 = BitmapFactory.decodeStream(in);
				        } catch (Exception e) {
				            Log.e("Error", e.getMessage());
				            e.printStackTrace();
				        }
					}else{
						mIcon11 = BitmapFactory.decodeFile(newfile.getAbsolutePath());
					}
		        }else{
		        	try {
			            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
			            mIcon11 = BitmapFactory.decodeStream(in);
			        } catch (Exception e) {
			            Log.e("Error", e.getMessage());
			            e.printStackTrace();
			        }
		        }
		        /*try {
		            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
		            mIcon11 = BitmapFactory.decodeStream(in);
		        } catch (Exception e) {
		            Log.e("Error", e.getMessage());
		            e.printStackTrace();
		        }*/
		        return mIcon11;
		    }

		    protected void onPostExecute(Bitmap result) {
		        bmImage.setImageBitmap(result);
		    }
		}
		
		public HashMap<String, String> weaponHashMap(){
			meMap = new HashMap<String, String>();
			meMap.put("MP7","smMP7");
			meMap.put("G17C","pG17");
			meMap.put("M98B","srM98");
			meMap.put("SG553","caSG553");
			meMap.put("870MCS","sg870");
			meMap.put("MP443 SUPP.","pMP443S");
			meMap.put("RPG-7V2","wLATRPG");
			meMap.put("MTAR-21","caMTAR21");
			meMap.put("KH2002","arKH");
			meMap.put("AKS-74u","caAKS");
			meMap.put("M1911 SUPP.","pM1911S");
			meMap.put("M27 IAR","mgM27");
			meMap.put("G36C", "caG36");
			meMap.put(".44 MAGNUM", "pTaur");
			meMap.put("AS VAL", "smVAL");
			meMap.put("DAO-12", "sgDAO");
			meMap.put("M320", "wahUGL");
			meMap.put("UMP-45", "smUMP");
			meMap.put("SA-18 IGLA", "wLAAIGL");
			meMap.put("SCAR-H", "caSCAR");
			meMap.put("MP412 REX", "pM412");
			meMap.put("M416", "arM416");
			meMap.put("AK-74M", "arAK74");
			meMap.put("PKP PECHENEG", "mgPech");
			meMap.put("M60E4", "mgM60");
			meMap.put("M240B","mgM240");
			meMap.put("M16A4","arM16");
			meMap.put("M1911 TACT.", "pM1911L");
			meMap.put("PP-2000", "smPP2000");
			meMap.put("G3A3","arG3");
			meMap.put("M4A1", "caM4");
			meMap.put("AEK-971","arAEK");
			meMap.put("AUG A3", "arAUG");
			meMap.put("MK11 MOD 0", "srMK11");
			meMap.put("AN-94","arAN94");
			meMap.put("SPAS-12", "sgSPAS12");
			meMap.put(".44 SCOPED","pTaurS");
			meMap.put("USAS-12","sgUSAS");
			meMap.put("MP443", "pMP443");
			meMap.put("RPK-74M", "mgRPK");
			meMap.put("ACB-90", "wasK");
			meMap.put("L85A2", "arL85A2");
			meMap.put("SV98", "srSV98");
			meMap.put("M40A5", "srM40");
			meMap.put("SCAR-L", "arSCARL");
			meMap.put("ACW-R", "caACR");
			meMap.put("MG36", "mgMG36");
			meMap.put("SAIGA 12K", "sgSaiga");
			meMap.put("FAMAS","arFAMAS");
			meMap.put("M1911 S-TAC", "pM1911T");
			meMap.put("93R", "pM93R");
			meMap.put("M9 SUPP.","pM9S");
			meMap.put("M1911", "pM1911");
			meMap.put("SVD", "srSVD");
			meMap.put("MK3A1", "sgJackH");
			meMap.put("G18 SUPP.", "pg18S");
			meMap.put("M249", "mgM249");
			meMap.put("G17C SUPP.","pG17S");
			meMap.put("QBU-88","srQBU88");
			meMap.put("M9", "pM9");
			meMap.put("M26 MASS", "wahUSG");
			meMap.put("JNG-90", "srJNG90");
			meMap.put("L86A2","mgL86A1");
			meMap.put("LSAT", "mgLSAT");
			meMap.put("SMAW", "wLATSMAW");
			meMap.put("P90", "smP90");
			meMap.put("M417","srHK417");
			meMap.put("A-91","caA91");
			meMap.put("SKS","srSKS");
			meMap.put("L96", "srL96");
			meMap.put("PP-19", "smPP19");
			meMap.put("TYPE 88 LMG", "mgT88");
			meMap.put("QBZ-95B", "caQBZ95B");
			meMap.put("M9 TACT.","pM9F");
			meMap.put("G18", "pg18");
			meMap.put("F2000", "arF2");
			meMap.put("FGM-148 JAVELIN", "wLATJAV");
			meMap.put("M5K","smM5K");
			meMap.put("MP443 TACT.", "pMP443L");
			meMap.put("G53", "caHK53");
			meMap.put("FIM-92 STINGER","wLAAFIM");
			meMap.put("M39 EMR", "srM39");
			meMap.put("M1014","sgM1014");
			meMap.put("PDW-R", "smPDR");
			meMap.put("QBB-95","mgQBB95");
			meMap.put("XBOW","wasXBK");
			meMap.put("XBOW SCOPED","wasXBS");
			return meMap;
		}
		
		public HashMap<String, String> equipementHashMap(){
			meMap = new HashMap<String, String>();
			meMap.put("RADIO BEACON","seqRad");
			meMap.put("M67 GRENADE","waeM67");
			meMap.put("REPAIR TOOL","wasRT");
			meMap.put("C4 EXPLOSIVES","waeC4");
			meMap.put("DEFIBRILLATOR","wasDef");
			meMap.put("T-UGS","seqUGS");
			meMap.put("M15 AT MINE","waeMine");
			meMap.put("M18 CLAYMORE","waeClay");
			meMap.put("EOD BOT","seqEOD");
			meMap.put("SOFLAM","seqSOF");
			meMap.put("M224 MORTAR","waeMort");
			meMap.put("MAV","seqMAV");
			return meMap;
		}
		
		public String getTime(long timeLong){
			String timeString = "";
			int seconds = (int) (timeLong) % 60 ;
			int minutes = (int) ((timeLong/60) % 60);
			int hours   = (int) ((timeLong / (60*60)) % 24);
			int days   = (int) ((timeLong / (60*60*24)) % 365);
			if(days>0){
				timeString = timeString+days+"d ";
			}
			if(hours>0){
				timeString = timeString+hours+"h ";
			}
			timeString = timeString+minutes+"m "+seconds+"s ";
			return timeString;
		}
		
		public String GetAccuracy(long first, long secound){
			 
	        double d;
	        d = ((double)first/(double)secound)*100;
	        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
	        return f.format(d); 
		}
		
		private long getScorePerMinute(long playerScore, long playerGameTime) {
			long gameTimeInMinutes = playerGameTime/60;
			long scorePerMinute = playerScore/gameTimeInMinutes;
			return scorePerMinute;
		}
		
		public String GetTwoDecimal(long first, long secound){
			 
	        double d;
	        d = (double)first/(double)secound;
	        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
	        return f.format(d); 
		}
		
		static final String[] MEDALS = new String[] {"m01",
			"m02",
			"m03",
			"m04",
			"m05",
			"m06",
			"m07",
			"m08",
			"m09",
			"m10",
			"m11",
			"m12",
			"m13",
			"m14",
			"m15",
			"m16",
			"m17",
			"m18",
			"m19",
			"m20",
			"m21",
			"m22",
			"m23",
			"m24",
			"m25",
			"m26",
			"m27",
			"m28",
			"m29",
			"m30",
			"m31",
			"m32",
			"m33",
			"m34",
			"m35",
			"m36",
			"m37",
			"m38",
			"m39",
			"m40",
			"m41",
			"m42",
			"m43",
			"m44",
			"m45",
			"m46",
			"m47",
			"m48",
			"m49",
			"m50",
			"xp2mdom",
			"xp2mgm",
			"xp3mts",
			"xp4mscv"
			};


}
