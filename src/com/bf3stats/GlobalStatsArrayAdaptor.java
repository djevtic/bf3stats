package com.bf3stats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class GlobalStatsArrayAdaptor extends ArrayAdapter<String> {
	private String First = null;
	private final Context context;
	private final String[] values;
	private Player player;
	private String tag = "djevtic";
	private String statsName;
	private TextView textViewGlobalStatisticData;
	private ImageView imageViewGlobalStatistic;
	private ImageView imageWeaponView;
	private TextView textViewGlobalStatisticName;
	private View rowView;
	private TextView textWeaponKills;
	private ImageView imageWeaponStars;
	private TextView textWeaponStar;
	private HashMap<String, String> meMap;
	private JSONObject weaponJSON;
	private HashMap<String, String> weaponHash;
	private HashMap<String, String> vehicleHash;
	private JSONObject separateWeaponString;
	private JSONObject stars;
	private TextView textWeaponName;
	private JSONArray weaponUnlocks;
	private ImageView imageUnlockView;
	private TextView textUnlockName;
	private TextView textUnlockDescription;
	private JSONObject row;
	private ImageView imageVehicleView;
	private TextView textVehicleName;
	private TextView textVehicleKills;
	private JSONObject vehicleJSON;
	private JSONObject separateVehicleString;
	private JSONObject separateVehicle;
	private String vehicleType;
	private JSONArray unlocksArray;
	private ImageView imageVehicleUnlockView;
	private TextView textVehicleUnlockName;
	private TextView textWeaponAcc;
	private ProgressBar progresUnlockWeaponProgres;
	private TextView textUnlockLeft;
	private ProgressBar progresVehicleUnlocks;
	private TextView textVehicleUnlockLeft;
	private ImageView imageUnlockViewAll;
	private TextView textUnlockNameAll;
	private ProgressBar progresUnlocksAll;
	private TextView textUnlockLeftAll;
	private ImageView imageUnlockWhatViewAll;
	private TextView textunlockWhatName;
	private JSONObject vehicleCategoryUnlock;
	private JSONObject vehicleKategory;
	private JSONArray vehicleUnlocks;
	private JSONObject medalsJSON;
	private JSONObject medalObject;
	private int medalNedded;
	private int medalCurent;
	private ImageView playerRankImage;
	private TextView playerName;
	private ProgressBar progresRankAll;
	private TextView rankPointsLeft;
	private TextView rankText;
	private Database db;
	private Cursor c;
	private ImageView playerPlatform;
	private String platform;
	private TextView assighmentName;
	private ImageView assighmentImage;
	private TextView assighmentText1;
	private TextView assighmentText2;
	private TextView assighmentText3;
	private TextView assighmentText4;
	private TextView assighmentText5;
	private TextView assighmentText6;
	private JSONObject assighmentsJSON;
	private TextView assighmentDescription;
	private ProgressBar assighmentProgressBar1;
	private TextView assighmentProgressText1;
	private ProgressBar assighmentProgressBar2;
	private TextView assighmentProgressText2;
	private ProgressBar assighmentProgressBar3;
	private TextView assighmentProgressText3;
	private ProgressBar assighmentProgressBar4;
	private TextView assighmentProgressText4;
	private ProgressBar assighmentProgressBar5;
	private TextView assighmentProgressText5;
	private ProgressBar assighmentProgressBar6;
	private TextView assighmentProgressText6;
	private JSONArray criteriaJSON;
	private LinearLayout linearLayout;
	private TextView textDescription;
	private TextView textProgress;
	private ProgressBar progresAssighment;
	private TextView textAssighment1;
	private TextView textAssighmentProgress1;
	private ProgressBar progresAssighment1;
	private TextView textAssighment2;
	private TextView textAssighmentProgress2;
	private ProgressBar progresAssighment2;
	private TextView textAssighment3;
	private TextView textAssighmentProgress3;
	private ProgressBar progresAssighment3;
	private TextView textAssighment4;
	private TextView textAssighmentProgress4;
	private ProgressBar progresAssighment4;
	private TextView textAssighment5;
	private TextView textAssighmentProgress5;
	private ProgressBar progresAssighment5;
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;
	private TextView textStatName;
	private TextView textPlayer1Stat;
	private TextView textPlayer2Stat;
	private ImageView imageEquipementView;
	private TextView textEquipementName;
	private TextView textEquipementFirstName;
	private TextView textEquipementFirstData;
	private TextView textEquipementSecondName;
	private TextView textEquipementSecondData;
	private HashMap<String, String> equipementHash;
	private JSONObject equipementJSON;
	private JSONObject separateEquipementString;
	private String Second;
	private ImageView imagemedal;
	private TextView textMedalName;
	private TextView textMedalCount;
	private TextView textMedalDescription;
	private String[] medalValues;
	private String imageLink;
	private JSONObject medals;
	private String medalCount;
	private String medalName;
	private String medalDescription;
	private ImageView imgribon;
	private TextView textRibonName;
	private TextView textRibonCount;
	private TextView textRibonDescription;
	private String ribonCount;
	private String ribonName;
	private String ribonDescription;
	private String imageRibonLink;
	private JSONObject ribons;

	public GlobalStatsArrayAdaptor(Context context, String[] values, String stats) {
		super(context, R.layout.global_stat_content, values);
		this.context = context;
		this.values = values;
		this.player = Player.getInstance();
		this.statsName = stats;
		this.weaponHash = weaponHashMap();
		this.vehicleHash = vehicleHashMap();
		this.equipementHash = equipementHashMap();
		this.weaponJSON = player.getWeapons();
		this.vehicleJSON = player.getVehicles();
		this.equipementJSON = player.getEquipements();
		this.assighmentsJSON = player.getAssighments();
		this.db = new Database(context);
		//Odavde poceto sa image JAR implementacijom
		LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(context);
	    imageManager = new ImageManager(context, settings);
	    imageTagFactory = new ImageTagFactory(context, R.drawable.oldguysscaledmenu);
	    imageTagFactory.setErrorImageId(R.drawable.oldguysscaledmenu);
	    imageLoader = imageManager.getLoader();
	    //Ovde je kraj
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		String s = values[position];
		if(checkForWeaponStat(statsName)){
			rowView = inflater.inflate(R.layout.weaponunlckslayout, parent, false);
			imageUnlockView = (ImageView) rowView.findViewById(R.id.imageunlockweapon);
			textUnlockName = (TextView) rowView.findViewById(R.id.textUnlockWeaponName);
			progresUnlockWeaponProgres = (ProgressBar) rowView.findViewById(R.id.progresUnlockWeaponProgresbar);
			textUnlockLeft = (TextView) rowView.findViewById(R.id.textUnlockLeft);
		}else if(statsName.equals("compare")){
			rowView = inflater.inflate(R.layout.singlecomparison, parent, false);
			textStatName = (TextView) rowView.findViewById(R.id.comparisoncolumnstat);
			textPlayer1Stat = (TextView) rowView.findViewById(R.id.comparisonplayer1stat);
			textPlayer2Stat = (TextView) rowView.findViewById(R.id.comparisonplayer2stat);
		}else if(statsName.equals("Weapon Stats")){
			rowView = inflater.inflate(R.layout.weaponstatcontent, parent, false);
			imageWeaponView = (ImageView) rowView.findViewById(R.id.weaponImage);
			textWeaponName = (TextView) rowView.findViewById(R.id.textWeaponsName);
			textWeaponKills = (TextView) rowView.findViewById(R.id.textWeaponsKill);
			imageWeaponStars = (ImageView) rowView.findViewById(R.id.weaponStarImage);
			textWeaponStar = (TextView) rowView.findViewById(R.id.textWeaponsStar);
			textWeaponAcc = (TextView) rowView.findViewById(R.id.textWeaponsAcc);
		}else if(statsName.equals("Vehicle Stats")){
			rowView = inflater.inflate(R.layout.vehiclestatscontent, parent, false);
			imageVehicleView = (ImageView) rowView.findViewById(R.id.vehicleImage);
			textVehicleName = (TextView) rowView.findViewById(R.id.textvehicleName);
			textVehicleKills = (TextView) rowView.findViewById(R.id.textvehicleKill);
		}else if(statsName.equals("Equipement")){
			rowView = inflater.inflate(R.layout.singleequipementlayout, parent, false);
			imageEquipementView = (ImageView) rowView.findViewById(R.id.equipementImage);
			textEquipementName = (TextView) rowView.findViewById(R.id.textequipementName);
			textEquipementFirstName = (TextView) rowView.findViewById(R.id.equipementFirstAbilityName);
			textEquipementFirstData = (TextView) rowView.findViewById(R.id.equipementFirstAbilityData);
			textEquipementSecondName = (TextView) rowView.findViewById(R.id.equipementSecondAbilityName);
			textEquipementSecondData = (TextView) rowView.findViewById(R.id.equipementSecondAbilityData);
		}else if(statsName.equals("Unlocks")){
			rowView = inflater.inflate(R.layout.global_stat_content, parent, false);
			imageViewGlobalStatistic = (ImageView) rowView.findViewById(R.id.playerStatContentImage);
			textViewGlobalStatisticName = (TextView) rowView.findViewById(R.id.textGlobalStatisticName);
			textViewGlobalStatisticData = (TextView) rowView.findViewById(R.id.textGlobalStatisticData);
			textViewGlobalStatisticName.setText(values[position]);
		}else if(statsName.equals("Medals")){
			medals = player.getMedals();
			rowView = inflater.inflate(R.layout.newmedallazout, parent, false);
			imagemedal = (ImageView) rowView.findViewById(R.id.imgMedalImage);
			textMedalName = (TextView) rowView.findViewById(R.id.textmedalName);
			textMedalCount = (TextView) rowView.findViewById(R.id.textmedalCount);
			textMedalDescription = (TextView) rowView.findViewById(R.id.textmedalDescr);
		}else if(statsName.equals("Ribbons")){
			ribons = player.getRibons();
			rowView = inflater.inflate(R.layout.newribonlayout, parent, false);
			imgribon = (ImageView) rowView.findViewById(R.id.imgRibonImage);
			textRibonName = (TextView) rowView.findViewById(R.id.textRibonName);
			textRibonCount = (TextView) rowView.findViewById(R.id.textRibonCount);
			textRibonDescription = (TextView) rowView.findViewById(R.id.textRibonDescr);
		}else if(checkForVehicleStat(statsName)){
			rowView = inflater.inflate(R.layout.vehicleunlockscontentlayout, parent, false);
			imageVehicleUnlockView = (ImageView) rowView.findViewById(R.id.vehicleUnlockImage);
			textVehicleUnlockName = (TextView) rowView.findViewById(R.id.textVehicleUnlokName);
			progresVehicleUnlocks = (ProgressBar) rowView.findViewById(R.id.progresUnlockVehicleProgresbar);
			textVehicleUnlockLeft = (TextView) rowView.findViewById(R.id.textVehicleUnlockLeft);
		}else if(checkForUnlocks(statsName)){
			rowView = inflater.inflate(R.layout.unlocksinglelayout, parent, false);
			textUnlockLeftAll = (TextView) rowView.findViewById(R.id.textUnlockLeft);
			textUnlockNameAll = (TextView) rowView.findViewById(R.id.textUnlockName);
			imageUnlockViewAll = (ImageView) rowView.findViewById(R.id.unlockImage);
			progresUnlocksAll = (ProgressBar) rowView.findViewById(R.id.progresUnlockProgresbar);
			imageUnlockWhatViewAll = (ImageView) rowView.findViewById(R.id.unlockWhatImage);
		}else if(statsName.equals("playerselector")){
			rowView = inflater.inflate(R.layout.playerselectorseparatelayout, parent, false);
			playerRankImage = (ImageView) rowView.findViewById(R.id.playerRankImage);
			playerName = (TextView) rowView.findViewById(R.id.textName);
			progresRankAll = (ProgressBar) rowView.findViewById(R.id.playerRankProgresbar);
			rankPointsLeft = (TextView) rowView.findViewById(R.id.textPlayerPoints);
			//playerPlatform = (ImageView) rowView.findViewById(R.id.playerPlatformImage);
			//rankText = (TextView) rowView.findViewById(R.id.textPlayerRank);
		}else if(statsName.equals("Assignments")){
			//1 thing to do
			if(s.equals("xp3prema02")||s.equals("xp3prema01")||s.equals("xp3ma02")||s.equals("xp3ma03")||s.equals("xp3ma04")||s.equals("xp3ma05")||s.equals("xp4ma01")
					||s.equals("xp4ma02")||s.equals("xp5ma01")||s.equals("xp5ma05")){
				rowView = inflater.inflate(R.layout.assighmentlayout1line, parent, false);
				assighmentName = (TextView) rowView.findViewById(R.id.textNameAssighment);
				assighmentImage = (ImageView) rowView.findViewById(R.id.imageAssighment);
				assighmentDescription = (TextView) rowView.findViewById(R.id.textAsighmentUnlocks);
				linearLayout = (LinearLayout) rowView.findViewById(R.id.assighnedLinearlayout);
				textAssighment1 = (TextView) rowView.findViewById(R.id.textAssighment1);
				textAssighmentProgress1 = (TextView) rowView.findViewById(R.id.textAssighmentProgress1);
				progresAssighment1 = (ProgressBar) rowView.findViewById(R.id.progresAssighment1);
			}
			//All with 2 stuff to unlock
			else if(s.equals("xpma01")||s.equals("xpma03")||s.equals("xpma05")||s.equals("xpma07")||s.equals("xp2prema02")||s.equals("xp2ma01")||s.equals("xp2ma02")
					||s.equals("xp2ma03")||s.equals("xp2ma04")||s.equals("xp2ma05")||s.equals("xp2ma06")||s.equals("xp2ma07")||s.equals("xp2ma08")||s.equals("xp2ma09")||s.equals("xp2ma10")
					||s.equals("xp3prema04")||s.equals("xp3prema05")||s.equals("xp3prema03")||s.equals("xp3ma01")||s.equals("xp4ma09")||s.equals("xp4ma06")||s.equals("xp4ma03")
					||s.equals("xp4prema02")||s.equals("xp4prema05")||s.equals("xp5prema01")||s.equals("xp5prema02")||s.equals("xp5prema03")||s.equals("xp5prema04")||s.equals("xp5ma04")){
				rowView = inflater.inflate(R.layout.assighmentlayout, parent, false);
				assighmentName = (TextView) rowView.findViewById(R.id.textNameAssighment);
				assighmentImage = (ImageView) rowView.findViewById(R.id.imageAssighment);
				assighmentDescription = (TextView) rowView.findViewById(R.id.textAsighmentUnlocks);
				linearLayout = (LinearLayout) rowView.findViewById(R.id.assighnedLinearlayout);
				textAssighment1 = (TextView) rowView.findViewById(R.id.textAssighment1);
				textAssighmentProgress1 = (TextView) rowView.findViewById(R.id.textAssighmentProgress1);
				progresAssighment1 = (ProgressBar) rowView.findViewById(R.id.progresAssighment1);
				textAssighment2 = (TextView) rowView.findViewById(R.id.textAssighment2);
				textAssighmentProgress2 = (TextView) rowView.findViewById(R.id.textAssighmentProgress2);
				progresAssighment2 = (ProgressBar) rowView.findViewById(R.id.progresAssighment2);
				//all with 3 stuff to unlock
			}else if(s.equals("xpma02")||s.equals("xpma04")||s.equals("xpma06")||s.equals("xpma08")||s.equals("xpma09")||s.equals("xp2prema09")||s.equals("xp2prema08")||s.equals("xp2prema04")
					||s.equals("xp2prema07")||s.equals("xp2prema06")||s.equals("xp2prema01")||s.equals("xp2prema03")||s.equals("xp2prema06")||s.equals("xp2prema07")||s.equals("xp2prema08")||s.equals("xp2prema09")
					||s.equals("xp3prema06")||s.equals("xp3prema07")||s.equals("xp3prema08")||s.equals("xp3prema09")||s.equals("xp4ma07")||s.equals("xp4ma04")||s.equals("xp4ma05")||s.equals("xp4ma08")
					||s.equals("xp4prema01")||s.equals("xp4prema04")||s.equals("xp4prema06")||s.equals("xp4prema07")||s.equals("xp4prema08")||s.equals("xp4prema09")||s.equals("xp5ma02")||s.equals("xp5ma03")){
				rowView = inflater.inflate(R.layout.assighmentlayout3lines, parent, false);
				assighmentName = (TextView) rowView.findViewById(R.id.textNameAssighment);
				assighmentImage = (ImageView) rowView.findViewById(R.id.imageAssighment);
				assighmentDescription = (TextView) rowView.findViewById(R.id.textAsighmentUnlocks);
				linearLayout = (LinearLayout) rowView.findViewById(R.id.assighnedLinearlayout);
				textAssighment1 = (TextView) rowView.findViewById(R.id.textAssighment1);
				textAssighmentProgress1 = (TextView) rowView.findViewById(R.id.textAssighmentProgress1);
				progresAssighment1 = (ProgressBar) rowView.findViewById(R.id.progresAssighment1);
				textAssighment2 = (TextView) rowView.findViewById(R.id.textAssighment2);
				textAssighmentProgress2 = (TextView) rowView.findViewById(R.id.textAssighmentProgress2);
				progresAssighment2 = (ProgressBar) rowView.findViewById(R.id.progresAssighment2);
				textAssighment3 = (TextView) rowView.findViewById(R.id.textAssighment3);
				textAssighmentProgress3 = (TextView) rowView.findViewById(R.id.textAssighmentProgress3);
				progresAssighment3 = (ProgressBar) rowView.findViewById(R.id.progresAssighment3);
				//all with 4 stuff to unlock
			}else if(s.equals("xp2prema10")||s.equals("xp2prema10")||s.equals("xp3prema10")||s.equals("xp4prema03")||s.equals("xp4prema10")||s.equals("xp5prema04")){
				rowView = inflater.inflate(R.layout.assighmentlayout4lines, parent, false);
				assighmentName = (TextView) rowView.findViewById(R.id.textNameAssighment);
				assighmentImage = (ImageView) rowView.findViewById(R.id.imageAssighment);
				assighmentDescription = (TextView) rowView.findViewById(R.id.textAsighmentUnlocks);
				linearLayout = (LinearLayout) rowView.findViewById(R.id.assighnedLinearlayout);
				textAssighment1 = (TextView) rowView.findViewById(R.id.textAssighment1);
				textAssighmentProgress1 = (TextView) rowView.findViewById(R.id.textAssighmentProgress1);
				progresAssighment1 = (ProgressBar) rowView.findViewById(R.id.progresAssighment1);
				textAssighment2 = (TextView) rowView.findViewById(R.id.textAssighment2);
				textAssighmentProgress2 = (TextView) rowView.findViewById(R.id.textAssighmentProgress2);
				progresAssighment2 = (ProgressBar) rowView.findViewById(R.id.progresAssighment2);
				textAssighment3 = (TextView) rowView.findViewById(R.id.textAssighment3);
				textAssighmentProgress3 = (TextView) rowView.findViewById(R.id.textAssighmentProgress3);
				progresAssighment3 = (ProgressBar) rowView.findViewById(R.id.progresAssighment3);
				textAssighment4 = (TextView) rowView.findViewById(R.id.textAssighment4);
				textAssighmentProgress4 = (TextView) rowView.findViewById(R.id.textAssighmentProgress4);
				progresAssighment4 = (ProgressBar) rowView.findViewById(R.id.progresAssighment4);
				//all with 5 stuff to unlock
			}else if(s.equals("xpma10")||s.equals("xp2prema05")||s.equals("xp4ma10")){
				rowView = inflater.inflate(R.layout.assighmentlayout5lines, parent, false);
				assighmentName = (TextView) rowView.findViewById(R.id.textNameAssighment);
				assighmentImage = (ImageView) rowView.findViewById(R.id.imageAssighment);
				assighmentDescription = (TextView) rowView.findViewById(R.id.textAsighmentUnlocks);
				linearLayout = (LinearLayout) rowView.findViewById(R.id.assighnedLinearlayout);
				textAssighment1 = (TextView) rowView.findViewById(R.id.textAssighment1);
				textAssighmentProgress1 = (TextView) rowView.findViewById(R.id.textAssighmentProgress1);
				progresAssighment1 = (ProgressBar) rowView.findViewById(R.id.progresAssighment1);
				textAssighment2 = (TextView) rowView.findViewById(R.id.textAssighment2);
				textAssighmentProgress2 = (TextView) rowView.findViewById(R.id.textAssighmentProgress2);
				progresAssighment2 = (ProgressBar) rowView.findViewById(R.id.progresAssighment2);
				textAssighment3 = (TextView) rowView.findViewById(R.id.textAssighment3);
				textAssighmentProgress3 = (TextView) rowView.findViewById(R.id.textAssighmentProgress3);
				progresAssighment3 = (ProgressBar) rowView.findViewById(R.id.progresAssighment3);
				textAssighment4 = (TextView) rowView.findViewById(R.id.textAssighment4);
				textAssighmentProgress4 = (TextView) rowView.findViewById(R.id.textAssighmentProgress4);
				progresAssighment4 = (ProgressBar) rowView.findViewById(R.id.progresAssighment4);
				textAssighment5 = (TextView) rowView.findViewById(R.id.textAssighment5);
				textAssighmentProgress5 = (TextView) rowView.findViewById(R.id.textAssighmentProgress5);
				progresAssighment5 = (ProgressBar) rowView.findViewById(R.id.progresAssighment5);
			}
		}else{
		
			rowView = inflater.inflate(R.layout.global_stat_content, parent, false);
			imageViewGlobalStatistic = (ImageView) rowView.findViewById(R.id.playerStatContentImage);
			textViewGlobalStatisticName = (TextView) rowView.findViewById(R.id.textGlobalStatisticName);
			textViewGlobalStatisticData = (TextView) rowView.findViewById(R.id.textGlobalStatisticData);
			textViewGlobalStatisticName.setText(values[position]);
		}
		
		if(statsName.equals("compare")){
			//"Rank","W/L","K/D","Headshoot","Longest H.","K.Streak","Accuracy","Score","Assault","Enginer","Recon","Support","Revive","Resupplies","Heals","Repairs"
			try{
				if(s.equals("Rank")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerRank());
					textPlayer2Stat.setText(""+player.getRank2());
				}else if(s.equals("W/L")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+GetTwoDecimal(player.getPlayerGameWin(),player.getPlayerGameLose())+"%");
					textPlayer2Stat.setText(""+GetTwoDecimal(player.getGlobals2().getLong("wins"),player.getGlobals2().getLong("losses"))+"%");
				}else if(s.equals("K/D")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+GetTwoDecimal(player.getPlayerGlobalKills(), player.getPlayerGlobalDeaths())+"%");
					textPlayer2Stat.setText(""+GetTwoDecimal(player.getGlobals2().getLong("kills"),player.getGlobals2().getLong("deaths"))+"%");
				}else if(s.equals("Score")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerScore());
					textPlayer2Stat.setText(""+player.getRankAndScore2().getLong("score"));
				}else if(s.equals("Headshoot")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerHeadShoots());
					textPlayer2Stat.setText(""+player.getGlobals2().getLong("headshots"));
				}else if(s.equals("Longest H.")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerLongestHeadshoot()+"m");
					textPlayer2Stat.setText(""+player.getGlobals2().getLong("longesthandhs")+"m");
				}else if(s.equals("K.Streak")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerKillStreek());
					textPlayer2Stat.setText(""+player.getGlobals2().getLong("killstreakbonus"));
				}else if(s.equals("Accuracy")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+GetAccuracy(player.getPlayerHits(),player.getPlayerShootsFired())+"%");
					textPlayer2Stat.setText(""+GetAccuracy(player.getGlobals2().getLong("hits"), player.getGlobals2().getLong("shots"))+"%");
				}else if(s.equals("Assault")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerAssaultScore());
					textPlayer2Stat.setText(""+player.getRankAndScore2().getLong("assault"));
				}else if(s.equals("Engineer")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerEnginerScore());
					textPlayer2Stat.setText(""+player.getRankAndScore2().getLong("engineer"));
				}else if(s.equals("Recon")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerReaconScore());
					textPlayer2Stat.setText(""+player.getRankAndScore2().getLong("recon"));
				}else if(s.equals("Support")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerSuportScore());
					textPlayer2Stat.setText(""+player.getRankAndScore2().getLong("support"));
				}else if(s.equals("Revive")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerRevives());
					textPlayer2Stat.setText(""+player.getGlobals2().getLong("revives"));
				}else if(s.equals("Resupplies")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerResuply());
					textPlayer2Stat.setText(""+player.getGlobals2().getLong("resupplies"));
				}else if(s.equals("Heals")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerHeals());
					textPlayer2Stat.setText(""+player.getGlobals2().getLong("heals"));
				}else if(s.equals("Repairs")){
					textStatName.setText(s);
					textPlayer1Stat.setText(""+player.getPlayerHeals());
					textPlayer2Stat.setText(""+player.getGlobals2().getLong("repairs"));
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else if(statsName.equals("Player Info")){
			try{
				if (s.startsWith("Country")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerCountryName());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Score")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.generalscore);
				}else if(s.startsWith("Time")){
					textViewGlobalStatisticData.setText(": "+getTime(player.getPlayerGameTime()));
					imageViewGlobalStatistic.setImageResource(R.drawable.mainscore);
				} else if(s.startsWith("SPM")){
					textViewGlobalStatisticData.setText(": "+getScorePerMinute(player.getPlayerScore(), player.getPlayerGameTime()));
					imageViewGlobalStatistic.setImageResource(R.drawable.scoreperminute);
				} else if(s.startsWith("K/D")){
					textViewGlobalStatisticData.setText(": "+GetTwoDecimal(player.getPlayerGlobalKills(), player.getPlayerGlobalDeaths()));
					imageViewGlobalStatistic.setImageResource(R.drawable.kdratio);
				}else if(s.startsWith("W/L")){
					textViewGlobalStatisticData.setText(": "+GetTwoDecimal(player.getPlayerGameWin(),player.getPlayerGameLose()));
					imageViewGlobalStatistic.setImageResource(R.drawable.winloseratio);
				}else if(s.startsWith("Rank")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerRank());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else if(statsName.equals("General Stats")){
			try{
				if (s.startsWith("Round Player")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerRoundsPlayed());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Finished Rounds")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerRoundsFinished());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("Skills")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerSkill());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Wins")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerGameWin());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Losses")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerGameLose());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else if(statsName.equals("Combat Stats")){
			try{
				if (s.startsWith("Kills")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerGlobalKills());
					imageViewGlobalStatistic.setImageResource(R.drawable.numberofkills);
				} else if(s.startsWith("Deaths")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerGlobalDeaths());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("Headshots")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerHeadShoots());
					imageViewGlobalStatistic.setImageResource(R.drawable.headshots);
				} else if(s.startsWith("Longest Headshot")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerLongestHeadshoot());
					imageViewGlobalStatistic.setImageResource(R.drawable.headshots);
				} else if(s.startsWith("Kill Streak")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerKillStreek());
					imageViewGlobalStatistic.setImageResource(R.drawable.killstreak);
				}else if(s.startsWith("Shots")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerShootsFired());
					imageViewGlobalStatistic.setImageResource(R.drawable.mainscore);
				} else if(s.startsWith("Hits")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerHits());
					imageViewGlobalStatistic.setImageResource(R.drawable.numberofkills);
				} else if(s.startsWith("Accuracy")){
					textViewGlobalStatisticData.setText(": "+GetAccuracy(player.getPlayerHits(),player.getPlayerShootsFired())+"%");
					imageViewGlobalStatistic.setImageResource(R.drawable.accuracy);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else if(statsName.equals("Scores")){
			try{
				if (s.startsWith("Assault")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerAssaultScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.assaultscore);
				} else if(s.startsWith("Enginer")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerEnginerScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.enginerscore);
				}else if(s.startsWith("Reacon")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerReaconScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.reaconscore);
				} else if(s.startsWith("Suport")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerSuportScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.supporscore);
				} else if(s.startsWith("Vehicle")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerVehicleScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.vehiclembtkills);
				}else if(s.startsWith("Awards")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerAwardPoints());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Unlocks")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerUnlocksScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Team")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerTeamScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("Squad")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerSquadScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("Bonus")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerBonusPoints());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("Objective")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerObjectiveScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("General")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerGeneralScore());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else if(statsName.equals("Team Stats")){
			try{
				if (s.startsWith("Kill Assist")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerKillAssist());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Destroy Assist")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerVehicleDestroyedAssistance());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("Damage Assist")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerDamageAssist());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Suppression Assist")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerSupression());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Revive")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerRevives());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}else if(s.startsWith("Resuply")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerResuply());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Heals")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerHeals());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				} else if(s.startsWith("Repair")){
					textViewGlobalStatisticData.setText(": "+player.getPlayerRepaires());
					imageViewGlobalStatistic.setImageResource(R.drawable.empty);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		//WEAPONS
		}else if(statsName.equals("Weapon Stats")){
			try{
				String weponCode = weaponHash.get(s);
				separateWeaponString = weaponJSON.getJSONObject(weponCode);
				//player.getImage(separateWeaponString.getString("img"), imageWeaponView);
				setImage(imageWeaponView, separateWeaponString.getString("img"));
				//GetImage(imageWeaponView, separateWeaponString.getString("img"));
				textWeaponName.setText(""+s);
				textWeaponKills.setText(""+separateWeaponString.getLong("kills"));
				stars = separateWeaponString.getJSONObject("star");
				textWeaponAcc.setText(""+GetAccuracy(separateWeaponString.getLong("hits"), separateWeaponString.getLong("shots"))+"%");
				if(stars.getLong("count")>0){
					//player.getImage(stars.getString("img"), imageWeaponStars);
					setImage(imageWeaponStars,stars.getString("img"));
					//GetImage(imageWeaponStars,stars.getString("img"));
					//new DownloadImageTask(imageWeaponStars).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/stars.getString("img"));
					textWeaponStar.setText(""+stars.getString("count"));
				}else{
					imageWeaponStars.setVisibility(View.INVISIBLE);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		//WEAPON UNLOCKS
		}else if(checkForWeaponStat(statsName)){
				String weponCode = weaponHash.get(statsName);
				try {
					separateWeaponString = weaponJSON.getJSONObject(weponCode);
					weaponUnlocks = separateWeaponString.getJSONArray("unlocks");
					if(weaponUnlocks.length()>0){
						for(int i = 0 ; i < weaponUnlocks.length(); i++){
							row = weaponUnlocks.getJSONObject(i);
							if(row.getString("name").equals(s)){
								//if(row.getLong("needed")==row.getLong("curr")){
									//player.getImage(row.getString("img"), imageUnlockView);
									setImage(imageUnlockView, row.getString("img"));
									//new DownloadImageTask(imageUnlockView).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/row.getString("img"));
									textUnlockName.setText(row.getString("name"));
									if(row.getLong("needed")==row.getLong("curr")){
										progresUnlockWeaponProgres.setVisibility(View.INVISIBLE);
										textUnlockLeft.setVisibility(View.INVISIBLE);
									}else{
										progresUnlockWeaponProgres.setMax((int)row.getLong("needed"));
										progresUnlockWeaponProgres.setProgress((int)row.getLong("curr"));
										textUnlockLeft.setText((row.getLong("needed")-row.getLong("curr"))+" left to unlock");
									}
								//}
							}
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		//EQUIPEMENT
		}else if(statsName.equals("Equipement")){
					try{
						String equipementCode = equipementHash.get(s);
						separateEquipementString = equipementJSON.getJSONObject(equipementCode);
						setImage(imageEquipementView,separateEquipementString.getString("img"));
						textEquipementName.setText(separateEquipementString.getString("name"));
						setEquipementData(equipementCode,separateEquipementString);
					}catch (Exception e) {
						e.printStackTrace();
					}
				
		//VEHICLE
		}else if(statsName.equals("Vehicle Stats")){
			try{
				String vehicleCode = vehicleHash.get(s);
				separateVehicleString = vehicleJSON.getJSONObject(vehicleCode);
					setImage(imageVehicleView,separateVehicleString.getString("img"));
					//new DownloadImageTask(imageVehicleView).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/separateVehicleString.getString("img"));
					textVehicleName.setText(""+s);
					textVehicleKills.setText(""+separateVehicleString.getLong("kills"));
				
			}catch (Exception e) {
				e.printStackTrace();
			}
		//VEHICLE UNLOCKS
		}else if(checkForVehicleStat(statsName)){
			vehicleHash = vehicleHashMap();
			String vehicleCode = vehicleHash.get(statsName);
			vehicleJSON = player.getVehicles();
			try {
				separateVehicle = vehicleJSON.getJSONObject(vehicleCode);
				vehicleType = separateVehicle.getString("category");
				unlocksArray = player.getingVehicleUnlocks().getJSONObject(vehicleType).getJSONArray("unlocks");
				if(!(vehicleType.equals(null)||vehicleType.equals(""))){
					for(int i = 0 ; i < unlocksArray.length(); i++){
						row = unlocksArray.getJSONObject(i);
						if(row.getString("name").equals(s)){
							//if(row.getLong("needed")==row.getLong("curr")){
								//player.setImage(row.getString("img"), imageUnlockView);
								setImage(imageVehicleUnlockView,row.getString("img"));
								//new DownloadImageTask(imageVehicleUnlockView).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/row.getString("img"));
								textVehicleUnlockName.setText(row.getString("name"));
								if(row.getLong("needed")==row.getLong("curr")){
									progresVehicleUnlocks.setVisibility(View.INVISIBLE);
									textVehicleUnlockLeft.setVisibility(View.INVISIBLE);
								}else{
									progresVehicleUnlocks.setMax((int)row.getLong("needed"));
									progresVehicleUnlocks.setProgress((int)row.getLong("curr"));
									textVehicleUnlockLeft.setText((row.getLong("needed")-row.getLong("curr"))+" left to unlock");
								}
							//}
						}
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(checkForUnlocks(statsName)){
			//{"Weapon Unlocks","Weapon","Vehicle Unlocks","Medals To Unlock"}
			if(statsName.equals("Weapon Unlocks")){
				String[] result = s.split("/");
				try{
				separateWeaponString = weaponJSON.getJSONObject(result[0]);
				weaponUnlocks = separateWeaponString.getJSONArray("unlocks");
				if(weaponUnlocks.length()>0){
					for(int i = 0 ; i < weaponUnlocks.length(); i++){
						row = weaponUnlocks.getJSONObject(i);
						if(row.getString("name").equals(result[1])){
								setImage(imageUnlockViewAll,row.getString("img"));
								//new DownloadImageTask(imageUnlockViewAll).execute("http://dl.dropbox.com/u/1480498/bf3/"+row.getString("img"));
								textUnlockNameAll.setText(row.getString("name")+" for "+separateWeaponString.getString("name"));
								setImage(imageUnlockWhatViewAll,separateWeaponString.getString("img"));
								//new DownloadImageTask(imageUnlockWhatViewAll).execute("http://dl.dropbox.com/u/1480498/bf3/"+separateWeaponString.getString("img"));
								progresUnlocksAll.setMax((int)row.getLong("needed"));
								progresUnlocksAll.setProgress((int)row.getLong("curr"));
								textUnlockLeftAll.setText((row.getLong("needed")-row.getLong("curr"))+" kills to unlock");

								
							//}
						}
					}
				}
				}catch(JSONException e){
					e.printStackTrace();
				}
			}else if(statsName.equals("Vehicle Unlocks")){
				String[] result = s.split("/");
				vehicleCategoryUnlock = player.getVehiclekategories();
				try{
				vehicleKategory = vehicleCategoryUnlock.getJSONObject(result[0]);
				vehicleUnlocks = vehicleKategory.getJSONArray("unlocks");
				if(vehicleUnlocks.length()>0){
					for(int i = 0 ; i < vehicleUnlocks.length(); i++){
						row = vehicleUnlocks.getJSONObject(i);
						if(row.getString("name").equals(result[1])){
								setImage(imageUnlockViewAll,row.getString("img"));
								//new DownloadImageTask(imageUnlockViewAll).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/row.getString("img"));
								textUnlockNameAll.setText(row.getString("name"));
								setImage(imageUnlockWhatViewAll,vehicleKategory.getString("img"));
								//new DownloadImageTask(imageUnlockWhatViewAll).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/vehicleKategory.getString("img"));
								//imageUnlockWhatViewAll.setVisibility(View.INVISIBLE);
								progresUnlocksAll.setMax((int)row.getLong("needed"));
								progresUnlocksAll.setProgress((int)row.getLong("curr"));
								textUnlockLeftAll.setText((row.getLong("needed")-row.getLong("curr"))+" points to unlock");

								
							//}
						}
					}
				}
				}catch(JSONException e){
					e.printStackTrace();
				}
			}else if(statsName.equals("Medals To Unlock")){
				String[] result = s.split("/");
				medalsJSON = player.getMedals();
					try {
						medalObject = medalsJSON.getJSONObject(result[0]);
						if(medalObject.getString("type").equals("multi")){
							setImage(imageUnlockViewAll,medalObject.getString("img_small"));
							//new DownloadImageTask(imageUnlockViewAll).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/medalObject.getString("img_medium"));
							textUnlockNameAll.setText(medalObject.getString("name"));
							imageUnlockWhatViewAll.setVisibility(View.INVISIBLE);
							medalNedded = (int)medalObject.getLong("needed");
							medalCurent = (int)medalObject.getLong("curr");
							if((int)medalNedded%50==0){
								progresUnlocksAll.setMax(50);
								progresUnlocksAll.setProgress((int)medalCurent%50);
								textUnlockLeftAll.setText("Left to next medal:"+(50-((int)medalCurent)%50));
							}else if((int)medalNedded%30==0){
								progresUnlocksAll.setMax(30);
								progresUnlocksAll.setProgress((int)medalCurent%30);
								textUnlockLeftAll.setText("Left to next medal:"+(30-((int)medalCurent)%30));
							}else{
								progresUnlocksAll.setMax((int)medalNedded);
								progresUnlocksAll.setProgress((int)medalCurent);
								textUnlockLeftAll.setText("Left to next medal:"+((int)medalNedded-(int)medalCurent));
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
			}
			
		}else if(statsName.equals("playerselector")){
			db.open();
			c = db.getAllPlayers();
			if(c!=null){
				if  (c.moveToFirst()) {
			        do {
			        	if(s.equals(c.getString(c.getColumnIndex("id")))){
			        		setImage(playerRankImage,c.getString(c.getColumnIndex("rankimage")));
			        		//new DownloadImageTask(playerRankImage).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/c.getString(c.getColumnIndex("rankimage")));
							playerName.setText(c.getString(c.getColumnIndex("name")));
							progresRankAll.setMax(Integer.parseInt(c.getString(c.getColumnIndex("nextscore")))-Integer.parseInt(c.getString(c.getColumnIndex("previousescore"))));
							progresRankAll.setProgress(Integer.parseInt(c.getString(c.getColumnIndex("curentscore")))-Integer.parseInt(c.getString(c.getColumnIndex("previousescore"))));
							rankPointsLeft.setText(c.getString(c.getColumnIndex("curentscore"))+"/"+c.getString(c.getColumnIndex("nextscore")));
							platform = c.getString(c.getColumnIndex("platform"));
			        	}
			        }while (c.moveToNext());
			    }
			}
			c.close();
			db.close();
		}else if(statsName.equals("Assignments")){
			GetAssighmentsData(s);
			//MEDALS
		}else if(statsName.equals("Medals")){
			medalValues = MEDALS;
			String medal = medalValues[position];
			medalName = "";
			medalDescription = "";
			try {
				imageLink = medals.getJSONObject(medal).getString("img_medium");
				medalName = medals.getJSONObject(medal).getString("name");
				medalDescription = medals.getJSONObject(medal).getString("desc");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			textMedalName.setText(medalName);
			textMedalDescription.setText(medalDescription);
			setImage(imagemedal, imageLink);
			//new DownloadImageTask(imageView).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/imageLink);
			try {
				medalCount = "x"+medals.getJSONObject(medal).getInt("count");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!medalCount.equals("x0")){
				textMedalCount.setText(""+medalCount);
			}else{
				textMedalCount.setText(medalCount);
				imagemedal.setAlpha(100);
			}
		}else if(statsName.equals("Ribbons")){
			//RIBBONS
			String[] ribonValues = RIBONS;
			String ribon = ribonValues[position];
			Log.v(tag,"Ribon:"+ribon);
			try {
				Log.v(tag,"Ribon:"+ribon);
				imageRibonLink = ribons.getJSONObject(ribon).getString("img_medium");
				ribonName = ribons.getJSONObject(ribon).getString("name");
				ribonDescription = ribons.getJSONObject(ribon).getString("desc");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			setImage(imgribon,imageRibonLink);
			textRibonName.setText(ribonName);
			textRibonDescription.setText(ribonDescription);
			try {
				ribonCount = "x"+ribons.getJSONObject(ribon).getInt("count");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!ribonCount.equals("x0")){
				textRibonCount.setText(""+ribonCount);
			}else{
				textRibonCount.setText(ribonCount);
				imgribon.setAlpha(100);
			}
		}
		
		return rowView;
	}
	private void setEquipementData(String equipementCode, JSONObject separateEquipementString2) {
		try{
		if(equipementCode.equals("seqRad")){
			textEquipementFirstName.setText("Spawns: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("spawns"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("waeM67")){
			textEquipementFirstName.setText("Kills: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("kills"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("wasRT")){
			textEquipementFirstName.setText("Repairs: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("repairs"));
			textEquipementSecondName.setText("Kills: ");
			textEquipementSecondData.setText(separateEquipementString2.getString("kills"));
		}else if(equipementCode.equals("waeC4")){
			textEquipementFirstName.setText("Kills: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("kills"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("wasDef")){
			textEquipementFirstName.setText("Revives: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("revives"));
			textEquipementSecondName.setText("Kills: ");
			textEquipementSecondData.setText(separateEquipementString2.getString("kills"));
		}else if(equipementCode.equals("seqUGS")){
			textEquipementFirstName.setText("Spots: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("spots"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("waeMine")){
			textEquipementFirstName.setText("Kills: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("kills"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("waeClay")){
			textEquipementFirstName.setText("Kills: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("kills"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("seqEOD")){
			textEquipementFirstName.setText("Kills: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("kills"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("seqSOF")){
			textEquipementFirstName.setVisibility(View.INVISIBLE);
			textEquipementFirstData.setVisibility(View.INVISIBLE);
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("waeMort")){
			textEquipementFirstName.setText("Kills: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("kills"));
			textEquipementSecondData.setVisibility(View.INVISIBLE);
			textEquipementSecondName.setVisibility(View.INVISIBLE);
		}else if(equipementCode.equals("seqMAV")){
			textEquipementFirstName.setText("Spots: ");
			textEquipementFirstData.setText(separateEquipementString2.getString("spots"));
			textEquipementSecondName.setText("Kills: ");
			textEquipementSecondData.setText(separateEquipementString2.getString("kills"));
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void GetAssighmentsData(String s) {
		Log.v(tag,"Assighnment we get:"+s);
		if(s.equals("xp4ma01")||s.equals("xp4ma02")||s.equals("xp4ma03")||s.equals("xp4ma04")||s.equals("xp4ma05")||s.equals("xp4ma06")
				||s.equals("xp4ma07")||s.equals("xp4ma08")||s.equals("xp4ma09")||s.equals("xp4ma10")){
			First = "XPACK4";
		}else if(s.equals("xp3ma01")||s.equals("xp3ma02")||s.equals("xp3ma03")||s.equals("xp3ma04")||s.equals("xp3ma05")){
			First = "XPACK3";
		}else if(s.equals("xp5ma01")||s.equals("xp5ma02")||s.equals("xp5ma03")||s.equals("xp5ma04")||s.equals("xp5ma05")){
			First = "XPACK5";
		}else if(s.equals("xp2prema01")||s.equals("xp2prema02")||s.equals("xp2prema03")||s.equals("xp2prema04")||s.equals("xp2prema05")||s.equals("xp2prema06")
				||s.equals("xp2prema07")||s.equals("xp2prema08")||s.equals("xp2prema09")||s.equals("xp2prema10")||s.equals("xp3prema01")||s.equals("xp3prema02")
				||s.equals("xp3prema03")||s.equals("xp3prema04")||s.equals("xp3prema05")||s.equals("xp3prema06")||s.equals("xp3prema07")||s.equals("xp3prema08")
				||s.equals("xp3prema09")||s.equals("xp3prema10")||s.equals("xp4prema01")||s.equals("xp4prema02")||s.equals("xp4prema03")||s.equals("xp4prema04")
				||s.equals("xp4prema05")||s.equals("xp4prema06")||s.equals("xp4prema07")||s.equals("xp4prema08")||s.equals("xp4prema09")||s.equals("xp4prema10")
				||s.equals("xp5prema01")||s.equals("xp5prema02")||s.equals("xp5prema03")||s.equals("xp5prema04")||s.equals("xp5prema05")){
			First = "PREMIUM";
		}else if(s.equals("xpma01")||s.equals("xpma02")){
			First = "xpma01";
		}else if(s.equals("xpma03")||s.equals("xpma04")){
			First = "xpma03";
		}else if(s.equals("xpma05")||s.equals("xpma06")){
			First = "xpma05";
		}else if(s.equals("xpma07")||s.equals("xpma08")){
			First = "xpma07";
		}else if(s.equals("xpma09")||s.equals("xpma10")){
			First = "xpma09";
		}else if(s.equals("xp2ma01")||s.equals("xp2ma02")){
			First = "xp2ma01";
		}else if(s.equals("xp2ma03")||s.equals("xp2ma04")){
			First = "xp2ma03";
		}else if(s.equals("xp2ma05")||s.equals("xp2ma06")){
			First = "xp2ma05";
		}else if(s.equals("xp2ma07")||s.equals("xp2ma08")){
			First = "xp2ma07";
		}else if(s.equals("xp2ma09")||s.equals("xp2ma10")){
			First = "xp2ma09";
		}
		Log.v(tag,"For first we got:"+First);
		try {
				assighmentName.setText(assighmentsJSON.getJSONObject(First).getJSONObject(s).getString("name"));
				setImage(assighmentImage,assighmentsJSON.getJSONObject(First).getJSONObject(s).getString("img"));
				assighmentDescription.setText(assighmentsJSON.getJSONObject(First).getJSONObject(s).getString("desc"));
				criteriaJSON = assighmentsJSON.getJSONObject(First).getJSONObject(s).getJSONArray("criteria");
				Log.v(tag,"Assighnment name:"+s);
				fillAssighmentRows(criteriaJSON,criteriaJSON.length());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void makeComponentsInvisible(JSONArray criteriaJSON2, int length) {
		if(length==1){
			textAssighment1.setVisibility(View.INVISIBLE);
			progresAssighment1.setVisibility(View.INVISIBLE);
			textAssighmentProgress1.setVisibility(View.INVISIBLE);
		}else if(length==2){
			textAssighment1.setVisibility(View.INVISIBLE);
			progresAssighment1.setVisibility(View.INVISIBLE);
			textAssighmentProgress1.setVisibility(View.INVISIBLE);
			textAssighment2.setVisibility(View.INVISIBLE);
			progresAssighment2.setVisibility(View.INVISIBLE);
			textAssighmentProgress2.setVisibility(View.INVISIBLE);
		}else if(length==3){
			textAssighment1.setVisibility(View.INVISIBLE);
			progresAssighment1.setVisibility(View.INVISIBLE);
			textAssighmentProgress1.setVisibility(View.INVISIBLE);
			textAssighment2.setVisibility(View.INVISIBLE);
			progresAssighment2.setVisibility(View.INVISIBLE);
			textAssighmentProgress2.setVisibility(View.INVISIBLE);
			textAssighment3.setVisibility(View.INVISIBLE);
			progresAssighment3.setVisibility(View.INVISIBLE);
			textAssighmentProgress3.setVisibility(View.INVISIBLE);
		}else if(length == 4){
			textAssighment1.setVisibility(View.INVISIBLE);
			progresAssighment1.setVisibility(View.INVISIBLE);
			textAssighmentProgress1.setVisibility(View.INVISIBLE);
			textAssighment2.setVisibility(View.INVISIBLE);
			progresAssighment2.setVisibility(View.INVISIBLE);
			textAssighmentProgress2.setVisibility(View.INVISIBLE);
			textAssighment3.setVisibility(View.INVISIBLE);
			progresAssighment3.setVisibility(View.INVISIBLE);
			textAssighmentProgress3.setVisibility(View.INVISIBLE);
			textAssighment4.setVisibility(View.INVISIBLE);
			progresAssighment4.setVisibility(View.INVISIBLE);
			textAssighmentProgress4.setVisibility(View.INVISIBLE);
		}else if(length == 5){
			textAssighment1.setVisibility(View.INVISIBLE);
			progresAssighment1.setVisibility(View.INVISIBLE);
			textAssighmentProgress1.setVisibility(View.INVISIBLE);
			textAssighment2.setVisibility(View.INVISIBLE);
			progresAssighment2.setVisibility(View.INVISIBLE);
			textAssighmentProgress2.setVisibility(View.INVISIBLE);
			textAssighment3.setVisibility(View.INVISIBLE);
			progresAssighment3.setVisibility(View.INVISIBLE);
			textAssighmentProgress3.setVisibility(View.INVISIBLE);
			textAssighment4.setVisibility(View.INVISIBLE);
			progresAssighment4.setVisibility(View.INVISIBLE);
			textAssighmentProgress4.setVisibility(View.INVISIBLE);
			textAssighment5.setVisibility(View.INVISIBLE);
			progresAssighment5.setVisibility(View.INVISIBLE);
			textAssighmentProgress5.setVisibility(View.INVISIBLE);
		}
		
	}

	private void fillAssighmentRows(JSONArray criteriaJSON2, int length) {
		if(length==1){
				try{
				row = criteriaJSON2.getJSONObject(0);
				textAssighment1.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment1.setVisibility(View.INVISIBLE);
					textAssighmentProgress1.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment1.setMax(row.getInt("needed"));
					progresAssighment1.setProgress(row.getInt("curr"));
					textAssighmentProgress1.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row=null;
				}catch(JSONException e){
					e.printStackTrace();
				}
		}else if(length==2){
			try{
			row = criteriaJSON2.getJSONObject(0);
			textAssighment1.setText(row.getString("nname"));
			if(row.getString("curr").equals(row.getString("needed"))){
				progresAssighment1.setVisibility(View.INVISIBLE);
				textAssighmentProgress1.setVisibility(View.INVISIBLE);
			}else{
				progresAssighment1.setMax(row.getInt("needed"));
				progresAssighment1.setProgress(row.getInt("curr"));
				textAssighmentProgress1.setText(row.getString("curr")+"/"+(row.getString("needed")));
			}
			row=null;
			row = criteriaJSON2.getJSONObject(1);
			textAssighment2.setText(row.getString("nname"));
			if(row.getString("curr").equals(row.getString("needed"))){
				progresAssighment2.setVisibility(View.INVISIBLE);
				textAssighmentProgress2.setVisibility(View.INVISIBLE);
			}else{
				progresAssighment2.setMax(row.getInt("needed"));
				progresAssighment2.setProgress(row.getInt("curr"));
				textAssighmentProgress2.setText(row.getString("curr")+"/"+(row.getString("needed")));
			}
			
			row = null;
			}catch(JSONException e){
				e.printStackTrace();
			}
		}else if(length==3){
			try{
			row = criteriaJSON2.getJSONObject(0);
			textAssighment1.setText(row.getString("nname"));
			if(row.getString("curr").equals(row.getString("needed"))){
				progresAssighment1.setVisibility(View.INVISIBLE);
				textAssighmentProgress1.setVisibility(View.INVISIBLE);
			}else{
				progresAssighment1.setMax(row.getInt("needed"));
				progresAssighment1.setProgress(row.getInt("curr"));
				textAssighmentProgress1.setText(row.getString("curr")+"/"+(row.getString("needed")));
			}
			row=null;
			row = criteriaJSON2.getJSONObject(1);
			textAssighment2.setText(row.getString("nname"));
			if(row.getString("curr").equals(row.getString("needed"))){
				progresAssighment2.setVisibility(View.INVISIBLE);
				textAssighmentProgress2.setVisibility(View.INVISIBLE);
			}else{
				progresAssighment2.setMax(row.getInt("needed"));
				progresAssighment2.setProgress(row.getInt("curr"));
				textAssighmentProgress2.setText(row.getString("curr")+"/"+(row.getString("needed")));
			}
			row = null;
			row = criteriaJSON2.getJSONObject(2);
			textAssighment3.setText(row.getString("nname"));
			if(row.getString("curr").equals(row.getString("needed"))){
				progresAssighment3.setVisibility(View.INVISIBLE);
				textAssighmentProgress3.setVisibility(View.INVISIBLE);
			}else{
				progresAssighment3.setMax(row.getInt("needed"));
				progresAssighment3.setProgress(row.getInt("curr"));
				textAssighmentProgress3.setText(row.getString("curr")+"/"+(row.getString("needed")));
			}
		
		}catch (JSONException e) {
			e.printStackTrace();
		}
		}else if(length == 4){
			try{
				row = criteriaJSON2.getJSONObject(0);
				textAssighment1.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment1.setVisibility(View.INVISIBLE);
					textAssighmentProgress1.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment1.setMax(row.getInt("needed"));
					progresAssighment1.setProgress(row.getInt("curr"));
					textAssighmentProgress1.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row=null;
				row = criteriaJSON2.getJSONObject(1);
				textAssighment2.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment2.setVisibility(View.INVISIBLE);
					textAssighmentProgress2.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment2.setMax(row.getInt("needed"));
					progresAssighment2.setProgress(row.getInt("curr"));
					textAssighmentProgress2.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row = null;
				row = criteriaJSON2.getJSONObject(2);
				textAssighment3.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment3.setVisibility(View.INVISIBLE);
					textAssighmentProgress3.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment3.setMax(row.getInt("needed"));
					progresAssighment3.setProgress(row.getInt("curr"));
					textAssighmentProgress3.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row = null;
				row = criteriaJSON2.getJSONObject(3);
				textAssighment4.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment4.setVisibility(View.INVISIBLE);
					textAssighmentProgress4.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment4.setMax(row.getInt("needed"));
					progresAssighment4.setProgress(row.getInt("curr"));
					textAssighmentProgress4.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
		}else if(length == 5){
			try{
				row = criteriaJSON2.getJSONObject(0);
				textAssighment1.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment1.setVisibility(View.INVISIBLE);
					textAssighmentProgress1.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment1.setMax(row.getInt("needed"));
					progresAssighment1.setProgress(row.getInt("curr"));
					textAssighmentProgress1.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row=null;
				row = criteriaJSON2.getJSONObject(1);
				textAssighment2.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment2.setVisibility(View.INVISIBLE);
					textAssighmentProgress2.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment2.setMax(row.getInt("needed"));
					progresAssighment2.setProgress(row.getInt("curr"));
					textAssighmentProgress2.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row = null;
				row = criteriaJSON2.getJSONObject(2);
				textAssighment3.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment3.setVisibility(View.INVISIBLE);
					textAssighmentProgress3.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment3.setMax(row.getInt("needed"));
					progresAssighment3.setProgress(row.getInt("curr"));
					textAssighmentProgress3.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row = null;
				row = criteriaJSON2.getJSONObject(3);
				textAssighment4.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment4.setVisibility(View.INVISIBLE);
					textAssighmentProgress4.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment4.setMax(row.getInt("needed"));
					progresAssighment4.setProgress(row.getInt("curr"));
					textAssighmentProgress4.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
				row = null;
				row = criteriaJSON2.getJSONObject(4);
				textAssighment5.setText(row.getString("nname"));
				if(row.getString("curr").equals(row.getString("needed"))){
					progresAssighment5.setVisibility(View.INVISIBLE);
					textAssighmentProgress5.setVisibility(View.INVISIBLE);
				}else{
					progresAssighment5.setMax(row.getInt("needed"));
					progresAssighment5.setProgress(row.getInt("curr"));
					textAssighmentProgress5.setText(row.getString("curr")+"/"+(row.getString("needed")));
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
		}
		
	}

	private boolean checkForWeaponStat(String statname2) {
		String[] comparestring= ShowStatistic.getPlayerWeaponString();
		for(int i=0; comparestring.length>i;i++){
			if(statname2.equals(comparestring[i]))
			{
				return true;
			}
		}
		return false;
	}
	
	private boolean checkForVehicleStat(String statname2) {
		String[] comparestring= getPlayerVehiclesString();
		for(int i=0; comparestring.length>i;i++){
			if(statname2.equals(comparestring[i]))
			{
				return true;
			}
		}
		return false;
	}
	
	static final String[] RIBONS = new String[] {"r01",
		"r02",
		"r03",
		"r04",
		"r05",
		"r06",
		"r07",
		"r08",
		"r09",
		"r10",
		"r11",
		"r12",
		"r13",
		"r14",
		"r15",
		"r16",
		"r17",
		"r18",
		"r19",
		"r20",
		"r21",
		"r22",
		"r23",
		"r24",
		"r25",
		"r26",
		"r27",
		"r28",
		"r29",
		"r30",
		"r31",
		"r32",
		"r33",
		"r34",
		"r35",
		"r36",
		"r37",
		"r38",
		"r39",
		"r40",
		"r41",
		"r42",
		"r43",
		"r44",
		"r45",
		"xp2rgm",
		"xp3rdom",
		"xp3rts",
		"xp4rndom",
		"xp4rnscv",
		"xp4rscav",
		"xp5asw",
		"xp5r501",
		"xp5r502",
		"xp5ras"};

	static final String[] MEDALS = new String[] {"m01",
		"m02",
		"m03",
		"m04",
		"m05",
		"m06",
		"m07",
		"m08",
		"m09",
		"m10",
		"m11",
		"m12",
		"m13",
		"m14",
		"m15",
		"m16",
		"m17",
		"m18",
		"m19",
		"m20",
		"m21",
		"m22",
		"m23",
		"m24",
		"m25",
		"m26",
		"m27",
		"m28",
		"m29",
		"m30",
		"m31",
		"m32",
		"m33",
		"m34",
		"m35",
		"m36",
		"m37",
		"m38",
		"m39",
		"m40",
		"m41",
		"m42",
		"m43",
		"m44",
		"m45",
		"m46",
		"m47",
		"m48",
		"m49",
		"m50",
		"xp2mdom",
		"xp2mgm",
		"xp3mts",
		"xp4mscv",
		"xp5m501",
		"xp5mas"
		};
	
	public static String[] getPlayerVehiclesString(){
		String[] menuList = new String[] {                   "UH-1Y VENOM",
                   "AAV-7A1 AMTRAC",
                   "M220 TOW LAUNCHER",
                   "F-35",
                   "T-90A",
                   "SKID LOADER",
                   "LAV-AD",
                   "MI-28 HAVOC",
                   "GAZ-3937 VODNIK",
                   "DPV",
                   "9K22 TUNGUSKA-M",
                   "AH-1Z VIPER",
                   "BMP-2M",
                   "LAV-25",
                   "M1 ABRAMS",
                   "RHIB BOAT",
                   "A-10 THUNDERBOLT",
                   "KA-60 KASATKA",
                   "PANTSIR-S1",
                   "F/A-18E SUPER HORNET",
                   "SU-25TM FROGFOOT",
                   "M1114 HMMWV",
                   "CENTURION C-RAM",
                   "VDV BUGGY",
                   "Z-11W",
                   "9M133 KORNET LAUNCHER",
                   "GROWLER ITV",
                   "SU-35BM FLANKER-E",
                   "BTR-90",
                   "AH-6J LITTLE BIRD",
                   "M1128",
                   "BM23",
                   "QUAD BIKE",
                   "SPRUT-SD",
                   "GUNSHIP",
                   "M142",
                   "RHINO",
                   "PHOENIX",
                   "BARSUK", 
                   "DROPSHIP",
                   "HMMWV ASRAD",
                   "DIRTBIKE",
                   "VODNIK AA"};
		Arrays.sort(menuList);
		return menuList;
	}
	
	public String getTime(long timeLong){
		String timeString = "";
		int seconds = (int) (timeLong) % 60 ;
		int minutes = (int) ((timeLong/60) % 60);
		int hours   = (int) ((timeLong / (60*60)) % 24);
		int days   = (int) ((timeLong / (60*60*24)) % 365);
		if(days>0){
			timeString = timeString+days+"d ";
		}
		if(hours>0){
			timeString = timeString+hours+"h ";
		}
		timeString = timeString+minutes+"m "+seconds+"s ";
		return timeString;
	}
	
	public String GetTwoDecimal(long first, long secound){
		 
        double d;
        d = (double)first/(double)secound;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	public String GetAccuracy(long first, long secound){
		 
        double d;
        d = ((double)first/(double)secound)*100;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	private long getScorePerMinute(long playerScore, long playerGameTime) {
		long gameTimeInMinutes = playerGameTime/60;
		long scorePerMinute = playerScore/gameTimeInMinutes;
		return scorePerMinute;
	}
	private HashMap<String, String> vehicleHashMap() {
		meMap = new HashMap<String, String>();
         meMap.put("UH-1Y VENOM","trUH1");
         meMap.put("AAV-7A1 AMTRAC","trAAV");
         meMap.put("M220 TOW LAUNCHER","atTOW");
         meMap.put("F-35","jfx1F35JSF");
         meMap.put("T-90A","mbtT90");
         meMap.put("SKID LOADER","trSkid");
         meMap.put("LAV-AD","aaLAV");
         meMap.put("MI-28 HAVOC","ahMi28");
         meMap.put("GAZ-3937 VODNIK","trVod");
         meMap.put("DPV","trDpv");
         meMap.put("9K22 TUNGUSKA-M","aa9k22");
         meMap.put("AH-1Z VIPER","ahAH1Z");
         meMap.put("BMP-2M","ifvBMP");
         meMap.put("LAV-25","ifvLAV");
         meMap.put("M1 ABRAMS","mbtM1A");
         meMap.put("RHIB BOAT","trRIB");
         meMap.put("A-10 THUNDERBOLT","jaA10");
         meMap.put("KA-60 KASATKA","trKA60");
         meMap.put("PANTSIR-S1","saaPant");
         meMap.put("F/A-18E SUPER HORNET","jfF18");
         meMap.put("SU-25TM FROGFOOT","jaSU25");
         meMap.put("M1114 HMMWV","trHum");
         meMap.put("CENTURION C-RAM","saaCRAM");
         meMap.put("VDV BUGGY","trVDV");
         meMap.put("Z-11W","shz11");
         meMap.put("9M133 KORNET LAUNCHER","sKorn");
         meMap.put("GROWLER ITV","trITV");
         meMap.put("SU-35BM FLANKER-E","jfSu35");
         meMap.put("BTR-90","ifvBTR90");
         meMap.put("AH-6J LITTLE BIRD","shAH6");
         meMap.put("M1128","mtdM1128");
         meMap.put("BM23","martBM23");
         meMap.put("QUAD BIKE","trQB");
         meMap.put("SPRUT-SD","mtdSPRUTSD");
         meMap.put("GUNSHIP","vmaG");
         meMap.put("M142","martHIM");
         meMap.put("BARSUK","trVodnM");
         meMap.put("PHOENIX","trHmvM");
         meMap.put("RHINO","trVanM");
         meMap.put("DROPSHIP","vmaD");
         meMap.put("HMMWV ASRAD","trhmvnxp5");
         meMap.put("DIRTBIKE","trKLR");
         meMap.put("VODNIK AA","trVodnxp5");
	return meMap;
    }
	
	public HashMap weaponHashMap(){
		meMap = new HashMap<String, String>();
		meMap.put("MP7","smMP7");
		meMap.put("G17C","pG17");
		meMap.put("M98B","srM98");
		meMap.put("SG553","caSG553");
		meMap.put("870MCS","sg870");
		meMap.put("MP443 SUPP.","pMP443S");
		meMap.put("RPG-7V2","wLATRPG");
		meMap.put("MTAR-21","caMTAR21");
		meMap.put("KH2002","arKH");
		meMap.put("AKS-74u","caAKS");
		meMap.put("M1911 SUPP.","pM1911S");
		meMap.put("M27 IAR","mgM27");
		meMap.put("G36C", "caG36");
		meMap.put(".44 MAGNUM", "pTaur");
		meMap.put("AS VAL", "smVAL");
		meMap.put("DAO-12", "sgDAO");
		meMap.put("M320", "wahUGL");
		meMap.put("UMP-45", "smUMP");
		meMap.put("SA-18 IGLA", "wLAAIGL");
		meMap.put("SCAR-H", "caSCAR");
		meMap.put("MP412 REX", "pM412");
		meMap.put("M416", "arM416");
		meMap.put("AK-74M", "arAK74");
		meMap.put("PKP PECHENEG", "mgPech");
		meMap.put("M60E4", "mgM60");
		meMap.put("M240B","mgM240");
		meMap.put("M16A4","arM16");
		meMap.put("M1911 TACT.", "pM1911L");
		meMap.put("PP-2000", "smPP2000");
		meMap.put("G3A3","arG3");
		meMap.put("M4A1", "caM4");
		meMap.put("AEK-971","arAEK");
		meMap.put("AUG A3", "arAUG");
		meMap.put("MK11 MOD 0", "srMK11");
		meMap.put("AN-94","arAN94");
		meMap.put("SPAS-12", "sgSPAS12");
		meMap.put(".44 SCOPED","pTaurS");
		meMap.put("USAS-12","sgUSAS");
		meMap.put("MP443", "pMP443");
		meMap.put("RPK-74M", "mgRPK");
		meMap.put("ACB-90", "wasK");
		meMap.put("L85A2", "arL85A2");
		meMap.put("SV98", "srSV98");
		meMap.put("M40A5", "srM40");
		meMap.put("SCAR-L", "arSCARL");
		meMap.put("ACW-R", "caACR");
		meMap.put("MG36", "mgMG36");
		meMap.put("SAIGA 12K", "sgSaiga");
		meMap.put("FAMAS","arFAMAS");
		meMap.put("M1911 S-TAC", "pM1911T");
		meMap.put("93R", "pM93R");
		meMap.put("M9 SUPP.","pM9S");
		meMap.put("M1911", "pM1911");
		meMap.put("SVD", "srSVD");
		meMap.put("MK3A1", "sgJackH");
		meMap.put("G18 SUPP.", "pg18S");
		meMap.put("M249", "mgM249");
		meMap.put("G17C SUPP.","pG17S");
		meMap.put("QBU-88","srQBU88");
		meMap.put("M9", "pM9");
		meMap.put("M26 MASS", "wahUSG");
		meMap.put("JNG-90", "srJNG90");
		meMap.put("L86A2","mgL86A1");
		meMap.put("LSAT", "mgLSAT");
		meMap.put("SMAW", "wLATSMAW");
		meMap.put("P90", "smP90");
		meMap.put("M417","srHK417");
		meMap.put("A-91","caA91");
		meMap.put("SKS","srSKS");
		meMap.put("L96", "srL96");
		meMap.put("PP-19", "smPP19");
		meMap.put("TYPE 88 LMG", "mgT88");
		meMap.put("QBZ-95B", "caQBZ95B");
		meMap.put("M9 TACT.","pM9F");
		meMap.put("G18", "pg18");
		meMap.put("F2000", "arF2");
		meMap.put("FGM-148 JAVELIN", "wLATJAV");
		meMap.put("M5K","smM5K");
		meMap.put("MP443 TACT.", "pMP443L");
		meMap.put("G53", "caHK53");
		meMap.put("FIM-92 STINGER","wLAAFIM");
		meMap.put("M39 EMR", "srM39");
		meMap.put("M1014","sgM1014");
		meMap.put("PDW-R", "smPDR");
		meMap.put("QBB-95","mgQBB95");
		meMap.put("XBOW","wasXBK");
		meMap.put("XBOW SCOPED","wasXBS");
		return meMap;
	}
	
	public HashMap<String, String> equipementHashMap(){
		meMap = new HashMap<String, String>();
		meMap.put("RADIO BEACON","seqRad");
		meMap.put("M67 GRENADE","waeM67");
		meMap.put("REPAIR TOOL","wasRT");
		meMap.put("C4 EXPLOSIVES","waeC4");
		meMap.put("DEFIBRILLATOR","wasDef");
		meMap.put("T-UGS","seqUGS");
		meMap.put("M15 AT MINE","waeMine");
		meMap.put("M18 CLAYMORE","waeClay");
		meMap.put("EOD BOT","seqEOD");
		meMap.put("SOFLAM","seqSOF");
		meMap.put("M224 MORTAR","waeMort");
		meMap.put("MAV","seqMAV");
		return meMap;
	}
	
	private boolean checkForUnlocks(String keyword) {
		String[] comparestring= getUnlockMenuString();
		for(int i=0; comparestring.length>i;i++){
			if(keyword.equals(comparestring[i]))
			{
				return true;
			}
		}
		return false;
	}
	private String[] getUnlockMenuString(){
		String[] menuList = new String[]{"Weapon Unlocks","Weapon","Vehicle Unlocks","Medals To Unlock"};
		return menuList;
	}
	
	public void GetImage(ImageView bmImage, String url){
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		String urltoDropBox = "http://dl.dropbox.com/u/1480498/bf3/";
		boolean cardStatus = getCardState();
		Bitmap mIcon11 = null;
		if(cardStatus){
			File dir = new File(extStorageDirectory+"/bf3statistic");
			try{
			  if(dir.mkdirs()) {
			  } else {
			  }
			}catch(Exception e){
			  e.printStackTrace();
			}
			File newdir = new File(extStorageDirectory+"/bf3statistic/"+url.substring(0, url.indexOf('/')));
			try{
				  if(newdir.mkdirs()) {
				  } else {
				  }
				}catch(Exception e){
				  e.printStackTrace();
				}
			String filename = url.substring(url.indexOf('/')+1);
			File newfile = new File(newdir+"/"+filename);
			if(!newfile.exists()){
				new DownloadImageTask(bmImage).execute(url);
			}else{
				mIcon11 = BitmapFactory.decodeFile(newfile.getAbsolutePath());
				bmImage.setImageBitmap(mIcon11);
			}
		}else{
			try {
	            InputStream in = new java.net.URL(urltoDropBox+url).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }
        }
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;
		private String extStorageDirectory;

	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    protected Bitmap doInBackground(String... urls) {
	    	String filename = null;
	        String urldisplay = urls[0];
			extStorageDirectory = Environment.getExternalStorageDirectory().toString();
	        String[] result = urldisplay.split("/");
	        String urltoDropBox = "http://dl.dropbox.com/u/1480498/bf3/";
	        Bitmap mIcon11 = null;
	        boolean cardStatus = getCardState();
	        if(cardStatus){
	        	File dir = new File(extStorageDirectory+"/bf3statistic");
				try{
				  if(dir.mkdirs()) {
				  } else {
				  }
				}catch(Exception e){
				  e.printStackTrace();
				}
				File newdir = new File(extStorageDirectory+"/bf3statistic/"+urldisplay.substring(0, urldisplay.indexOf('/')));
				try{
					  if(newdir.mkdirs()) {
					  } else {
					  }
					}catch(Exception e){
					  e.printStackTrace();
					}
				filename = urldisplay.substring(urldisplay.indexOf('/')+1);
				File newfile = new File(newdir+"/"+filename);
				if(!newfile.exists()){
					downloadFile(urltoDropBox+urldisplay, newfile);
					try {
			            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
			            mIcon11 = BitmapFactory.decodeStream(in);
			        } catch (Exception e) {
			            Log.e("Error", e.getMessage());
			            e.printStackTrace();
			        }
				}else{
					mIcon11 = BitmapFactory.decodeFile(newfile.getAbsolutePath());
				}
	        }else{
	        	try {
		            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
		            mIcon11 = BitmapFactory.decodeStream(in);
		        } catch (Exception e) {
		            Log.e("Error", e.getMessage());
		            e.printStackTrace();
		        }
	        }
	        /*try {
	            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }*/
	        return mIcon11;
	    }

	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}
	
	public boolean getCardState(){
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		return mExternalStorageAvailable;
	}
	
	private static void downloadFile(String url, File outputFile) {
		  try {
		      URL u = new URL(url);
		      URLConnection conn = u.openConnection();
		      int contentLength = conn.getContentLength();
		      if(contentLength != -1){
		      DataInputStream stream = new DataInputStream(u.openStream());
		      
		        byte[] buffer = new byte[contentLength];
		        stream.readFully(buffer);
		        stream.close();

		        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
		        fos.write(buffer);
		        fos.flush();
		        fos.close();
		      }
		  } catch(FileNotFoundException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  } catch (IOException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  }
		}
	
	private void setImage(ImageView imageView,String url){
		String link = "http://dl.dropbox.com/u/1480498/bf3/"+url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}

}
