package com.bf3stats;

import java.util.Arrays;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;

public class UnlockView extends Activity{

	private String[][] sortedStatistics;
	private Player player;
	private HashMap<String, String> meMap;
	private HashMap<String, String> weaponHash;
	private String weponCode;
	private JSONObject weaponJSON;
	private JSONObject separateWeaponString;
	private JSONArray weaponUnlocksArray;
	private String stringKit;
	private JSONArray unlockedWeaponsArray;
	private JSONObject unlockedWeaponFromKit;
	private JSONObject weaponUnlocksSeparated;
	private HashMap<String, String> vehicleHash;
	private String vehicleCode;
	private JSONObject vehicleJSON;
	private JSONObject separateVehicleString;
	private JSONArray separateWehiclesUnlocks;
	private JSONObject separateWehicleUnlockSingle;
	private HashMap<String, String> equipementHash;
	private String equipementCode;
	private JSONObject equipementJSON;
	private JSONObject separateEquipement;
	private JSONObject medalJSON;
	private JSONObject kitAssault;
	private JSONObject kitSuport;
	private JSONObject kitEnginer;
	private JSONObject kitReacon;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unlockviewlayout);
        initialyzeComponents();
	}

	private void initialyzeComponents() {
		getPlayer();
		//sortedStatistics = sortStatistics();
		
	}

	/*private String[][] sortStatistics() {
		String[] weaponString = getWeaponString();
		String[] vehicleString = getVehicleString();
		String[] equipementString = getEquipementString();
		String[] medalString = getVehicleString();
		String[] serviceStarString = getServiceStarString();
		int numberOfElements = 0;
		//Check weapons and their unlocks
		String[][] weaponKillsArray = new String [weaponString.length][2];
		for(int i = 0; weaponString.length>i;i++){
			weaponHash = weaponHashMap();
			weponCode = weaponHash.get(weaponString[i]);
			try {
				weaponJSON = player.getWeapons();
				separateWeaponString = weaponJSON.getJSONObject(weponCode);
				weaponUnlocksArray = separateWeaponString.getJSONArray("unlocks");
				for(int c = 0;weaponUnlocksArray.length()>c;c++){
					weaponUnlocksSeparated = weaponUnlocksArray.getJSONObject(c);
					if(weaponUnlocksSeparated.getLong("needed")>weaponUnlocksSeparated.getLong("curr")){
						numberOfElements++;
					}
				}
				//Check is weapon unlocked
				stringKit = separateWeaponString.getString("kit");
				try {
				unlockedWeaponsArray = player.getKit().getJSONArray(stringKit);
				for(int y = 0;unlockedWeaponsArray.length()>y;y++){
					unlockedWeaponFromKit = unlockedWeaponsArray.getJSONObject(y);
					if(unlockedWeaponFromKit.getString("curr").equals("0")){
						numberOfElements++;
					}
				}
				}catch (JSONException e){
					e.printStackTrace();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//Vehicles unlocks
		for(int i = 0; vehicleString.length>i;i++){
			vehicleHash = vehicleHashMap();
			vehicleCode = vehicleHash.get(vehicleString[i]);
			try {
				vehicleJSON = player.getVehicles();
				separateVehicleString = vehicleJSON.getJSONObject(vehicleCode);
				separateWehiclesUnlocks = separateVehicleString.getJSONArray("unlocks");
				if(separateWehiclesUnlocks!=null){
					for(int x = 0; separateWehiclesUnlocks.length()>0;x++){
						separateWehicleUnlockSingle = separateWehiclesUnlocks.getJSONObject(x);
						if(separateWehicleUnlockSingle.getLong("needed")>separateWehicleUnlockSingle.getLong("curr")){
							numberOfElements++;
						}
						
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//Equipement
		for(int z = 0;equipementString.length>z;z++){
			equipementHash = equipementHashmap();
			equipementCode = equipementHash.get(equipementString[z]);
			try{
				equipementJSON = player.getEquipements();
				separateEquipement = equipementJSON.getJSONObject(equipementCode);
				if(!separateEquipement.getJSONObject("unlock").equals(null)){
					if(separateEquipement.getJSONObject("unlock").getLong("needed")>separateEquipement.getJSONObject("unlock").getLong("curr")){
						numberOfElements++;
					}
				}
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//Medals
		for(int a = 0;medalString.length<a;a++){
			medalJSON = player.getMedals();
			try {
				if(medalJSON.getJSONObject(medalString[a]).getLong("needed")>medalJSON.getJSONObject(medalString[a]).getLong("curr")){
					numberOfElements++;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//ServiceStars
		try {
		kitAssault = player.getKit().getJSONObject("assault");
		kitSuport = player.getKit().getJSONObject("support");
		kitEnginer = player.getKit().getJSONObject("engineer");
		kitReacon = player.getKit().getJSONObject("recon");
		if(kitAssault.getJSONObject("star").getLong("needed")>kitAssault.getJSONObject("star").getLong("curr"))
		for(int y = 0;unlockedWeaponsArray.length()>y;y++){
			unlockedWeaponFromKit = unlockedWeaponsArray.getJSONObject(y);
			if(unlockedWeaponFromKit.getString("curr").equals("0")){
				numberOfElements++;
			}
		}
		}catch (JSONException e){
			e.printStackTrace();
		}
		numberOfElements = numberOfElements+4;//for service stars
		String [][] temp = new String[numberOfElements][4];
		//Filing String
		int fillingNumber = 0;
		
		for(int i = 0; weaponString.length>i;i++){
			weaponHash = weaponHashMap();
			weponCode = weaponHash.get(weaponString[i]);
			try {
				weaponJSON = player.getWeapons();
				separateWeaponString = weaponJSON.getJSONObject(weponCode);
				weaponUnlocksArray = separateWeaponString.getJSONArray("unlocks");
				for(int c = 0;weaponUnlocksArray.length()>c;c++){
					weaponUnlocksSeparated = weaponUnlocksArray.getJSONObject(c);
					if(weaponUnlocksSeparated.getLong("needed")>weaponUnlocksSeparated.getLong("curr")){
						temp[fillingNumber][1]=weaponString[i];
						temp[fillingNumber][2]=weaponUnlocksSeparated.getString("name");
						temp[fillingNumber][3]="weaponunlock";
						fillingNumber++;
					}
				}
				//Check is weapon unlocked
				stringKit = separateWeaponString.getString("kit");
				try {
				unlockedWeaponsArray = player.getKit().getJSONArray(stringKit);
				for(int y = 0;unlockedWeaponsArray.length()>y;y++){
					unlockedWeaponFromKit = unlockedWeaponsArray.getJSONObject(y);
					if(unlockedWeaponFromKit.getString("curr").equals("0")){
						temp[fillingNumber][1]=stringKit;
						temp[fillingNumber][2]=unlockedWeaponFromKit.getString("name");
						temp[fillingNumber][3]="weapon";
						fillingNumber++;
					}
				}
				}catch (JSONException e){
					e.printStackTrace();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		//Vehicles unlocks
		for(int i = 0; vehicleString.length>i;i++){
			vehicleHash = vehicleHashMap();
			vehicleCode = vehicleHash.get(vehicleString[i]);
			try {
				vehicleJSON = player.getVehicles();
				separateVehicleString = vehicleJSON.getJSONObject(vehicleCode);
				separateWehiclesUnlocks = separateVehicleString.getJSONArray("unlocks");
				if(separateWehiclesUnlocks!=null){
					for(int x = 0; separateWehiclesUnlocks.length()>0;x++){
						separateWehicleUnlockSingle = separateWehiclesUnlocks.getJSONObject(x);
						if(separateWehicleUnlockSingle.getLong("needed")>separateWehicleUnlockSingle.getLong("curr")){
							temp[fillingNumber][1]=vehicleCode;
							temp[fillingNumber][2]=separateWehicleUnlockSingle.getString("name");
							temp[fillingNumber][3]="vehicleunlock";
							fillingNumber++;
						}
						
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//Equipement
		for(int z = 0;equipementString.length>z;z++){
			equipementHash = equipementHashmap();
			equipementCode = equipementHash.get(equipementString[z]);
			try{
				equipementJSON = player.getEquipements();
				separateEquipement = equipementJSON.getJSONObject(equipementCode);
				if(!separateEquipement.getJSONObject("unlock").equals(null)){
					if(separateEquipement.getJSONObject("unlock").getLong("needed")>separateEquipement.getJSONObject("unlock").getLong("curr")){
						temp[fillingNumber][1]=vehicleCode;
						temp[fillingNumber][2]=separateWehicleUnlockSingle.getString("name");
						temp[fillingNumber][3]="vehicleunlock";
						fillingNumber++;
					}
				}
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//Medals
		for(int a = 0;medalString.length<a;a++){
			medalJSON = player.getMedals();
			try {
				if(medalJSON.getJSONObject(medalString[a]).getLong("needed")>medalJSON.getJSONObject(medalString[a]).getLong("curr")){
					numberOfElements++;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		for(int counter=0; weaponString.length>counter; counter++) { //Loop once for each element in the array.
	            for (int i = 0; i < weaponKillsArray.length - 1; i++){
	        		int min = i;

	        		for (int  j = i + 1; j < weaponKillsArray.length; j++){
	        			if (Long.parseLong(weaponKillsArray[j][1]) > Long.parseLong(weaponKillsArray[min][1])){

	        				min = j;

	        			}

	        		}
	        		if (i != min){ 
	        			temp[0][0] = weaponKillsArray[i][0];
	        			temp[0][1] = weaponKillsArray[i][1];
	        			//int tmp = data[i];
	        			weaponKillsArray[i][0]=weaponKillsArray[min][0];
	        			weaponKillsArray[i][1]=weaponKillsArray[min][1];
	        			//data[i] = data[min];
	        			weaponKillsArray[min][0]=temp[0][0];
	        			weaponKillsArray[min][1]=temp[0][1];
	        			//data[min] = tmp;

	    	      }

	        	}
	        }
		String[] arrayToReturn= new String[weaponKillsArray.length];
		for(int i = 0; weaponKillsArray.length>i;i++){
			arrayToReturn[i]=weaponKillsArray[i][0];
		}
		return arrayToReturn;
	}*/

	private String[] getPlayerVehiclesString() {
		// TODO Auto-generated method stub
		return null;
	}

	private String[] getServiceStarString() {
		// TODO Auto-generated method stub
		return null;
	}

	
	private String[] getEquipementString() {
		String[] menuList = new String[] {"RADIO BEACON","M67 GRENADE","REPAIR TOOL","C4 EXPLOSIVES","DEFIBRILLATOR","T-UGS","M15 AT MINE","M18 CLAYMORE","EOD BOT","SOFLAM","M224 MORTAR",
				"MAV"};
		
		return menuList;
	}

	private String[] getVehicleString() {
		String[] menuList = new String[] {                   "UH-1Y VENOM",
                "AAV-7A1 AMTRAC",
                "M220 TOW LAUNCHER",
                "F-35",
                "T-90A",
                "SKID LOADER",
                "LAV-AD",
                "MI-28 HAVOC",
                "GAZ-3937 VODNIK",
                "DPV",
                "9K22 TUNGUSKA-M",
                "AH-1Z VIPER",
                "BMP-2M",
                "LAV-25",
                "M1 ABRAMS",
                "RHIB BOAT",
                "A-10 THUNDERBOLT",
                "KA-60 KASATKA",
                "PANTSIR-S1",
                "F/A-18E SUPER HORNET",
                "SU-25TM FROGFOOT",
                "M1114 HMMWV",
                "CENTURION C-RAM",
                "VDV BUGGY",
                "Z-11W",
                "9M133 KORNET LAUNCHER",
                "GROWLER ITV",
                "SU-35BM FLANKER-E",
                "BTR-90",
                "AH-6J LITTLE BIRD"};
		Arrays.sort(menuList);
		return menuList;
	}

	private String[] getWeaponString() {
		String[] menuList = new String[] {"MP7", "G17C", "M98B", "SG553","870MCS","MP443 SUPP.","RPG-7V2", "MTAR-21", "KH2002", "AKS-74u", "M1911 SUPP.", 
				"M27 IAR", "G36C", ".44 MAGNUM", "AS VAL", "DAO-12", "M320", "UMP-45", "SA-18 IGLA", "SCAR-H", "MP412 REX", "M416", "AK-74M", "PKP PECHENEG", 
				"M60E4", "M240B", "M16A4", "M1911 TACT.", "PP-2000", "G3A3", "M4A1", "AEK-971", "AUG A3", "MK11 MOD 0", "AN-94", "SPAS-12", ".44 SCOPED", 
				"USAS-12", "MP443", "RPK-74M", "ACB-90", "L85A2", "SV98", "M40A5", "SCAR-L", "ACW-R", "MG36", "SAIGA 12K", "FAMAS", "M1911 S-TAC", "93R", 
				"M9 SUPP.", "M1911", "SVD", "MK3A1", "G18 SUPP.", "M249", "G17C SUPP.", "QBU-88", "M9", "M26 MASS", "JNG-90", "L86A2", "LSAT", "SMAW", "P90", 
				"M417", "A-91", "SKS", "L96", "PP-19", "TYPE 88 LMG", "QBZ-95B", "M9 TACT.", "G18", "F2000", "FGM-148 JAVELIN", "M5K", "MP443 TACT.", "G53", 
				"FIM-92 STINGER", "M39 EMR", "M1014", "PDW-R", "QBB-95"};
		Arrays.sort(menuList);
		return menuList;
	}

	private void getPlayer() {
		this.player = Player.getInstance();
	}
	
	public HashMap<String, String> equipementHashmap(){
		meMap = new HashMap<String, String>();
		meMap.put("RADIO BEACON","seqRad");
		meMap.put("M67 GRENADE","waeM67");
		meMap.put("REPAIR TOOL","wasRT");
		meMap.put("C4 EXPLOSIVES","waeC4");
		meMap.put("DEFIBRILLATOR","wasDef");
		meMap.put("T-UGS","seqUGS");
		meMap.put("M15 AT MINE","waeMine");
		meMap.put("M18 CLAYMORE","waeClay");
		meMap.put("EOD BOT","seqEOD");
		meMap.put("SOFLAM","seqSOF");
		meMap.put("M224 MORTAR","waeMort");
		meMap.put("MAV","seqMAV");
		return meMap;
	}
	
	public HashMap<String, String> weaponHashMap(){
		meMap = new HashMap<String, String>();
		meMap.put("MP7","smMP7");
		meMap.put("G17C","pG17");
		meMap.put("M98B","srM98");
		meMap.put("SG553","caSG553");
		meMap.put("870MCS","sg870");
		meMap.put("MP443 SUPP.","pMP443S");
		meMap.put("RPG-7V2","wLATRPG");
		meMap.put("MTAR-21","caMTAR21");
		meMap.put("KH2002","arKH");
		meMap.put("AKS-74u","caAKS");
		meMap.put("M1911 SUPP.","pM1911S");
		meMap.put("M27 IAR","mgM27");
		meMap.put("G36C", "caG36");
		meMap.put(".44 MAGNUM", "pTaur");
		meMap.put("AS VAL", "smVAL");
		meMap.put("DAO-12", "sgDAO");
		meMap.put("M320", "wahUGL");
		meMap.put("UMP-45", "smUMP");
		meMap.put("SA-18 IGLA", "wLAAIGL");
		meMap.put("SCAR-H", "caSCAR");
		meMap.put("MP412 REX", "pM412");
		meMap.put("M416", "arM416");
		meMap.put("AK-74M", "arAK74");
		meMap.put("PKP PECHENEG", "mgPech");
		meMap.put("M60E4", "mgM60");
		meMap.put("M240B","mgM240");
		meMap.put("M16A4","arM16");
		meMap.put("M1911 TACT.", "pM1911L");
		meMap.put("PP-2000", "smPP2000");
		meMap.put("G3A3","arG3");
		meMap.put("M4A1", "caM4");
		meMap.put("AEK-971","arAEK");
		meMap.put("AUG A3", "arAUG");
		meMap.put("MK11 MOD 0", "srMK11");
		meMap.put("AN-94","arAN94");
		meMap.put("SPAS-12", "sgSPAS12");
		meMap.put(".44 SCOPED","pTaurS");
		meMap.put("USAS-12","sgUSAS");
		meMap.put("MP443", "pMP443");
		meMap.put("RPK-74M", "mgRPK");
		meMap.put("ACB-90", "wasK");
		meMap.put("L85A2", "arL85A2");
		meMap.put("SV98", "srSV98");
		meMap.put("M40A5", "srM40");
		meMap.put("SCAR-L", "arSCARL");
		meMap.put("ACW-R", "caACR");
		meMap.put("MG36", "mgMG36");
		meMap.put("SAIGA 12K", "sgSaiga");
		meMap.put("FAMAS","arFAMAS");
		meMap.put("M1911 S-TAC", "pM1911T");
		meMap.put("93R", "pM93R");
		meMap.put("M9 SUPP.","pM9S");
		meMap.put("M1911", "pM1911");
		meMap.put("SVD", "srSVD");
		meMap.put("MK3A1", "sgJackH");
		meMap.put("G18 SUPP.", "pg18S");
		meMap.put("M249", "mgM249");
		meMap.put("G17C SUPP.","pG17S");
		meMap.put("QBU-88","srQBU88");
		meMap.put("M9", "pM9");
		meMap.put("M26 MASS", "wahUSG");
		meMap.put("JNG-90", "srJNG90");
		meMap.put("L86A2","mgL86A1");
		meMap.put("LSAT", "mgLSAT");
		meMap.put("SMAW", "wLATSMAW");
		meMap.put("P90", "smP90");
		meMap.put("M417","srHK417");
		meMap.put("A-91","caA91");
		meMap.put("SKS","srSKS");
		meMap.put("L96", "srL96");
		meMap.put("PP-19", "smPP19");
		meMap.put("TYPE 88 LMG", "mgT88");
		meMap.put("QBZ-95B", "caQBZ95B");
		meMap.put("M9 TACT.","pM9F");
		meMap.put("G18", "pg18");
		meMap.put("F2000", "arF2");
		meMap.put("FGM-148 JAVELIN", "wLATJAV");
		meMap.put("M5K","smM5K");
		meMap.put("MP443 TACT.", "pMP443L");
		meMap.put("G53", "caHK53");
		meMap.put("FIM-92 STINGER","wLAAFIM");
		meMap.put("M39 EMR", "srM39");
		meMap.put("M1014","sgM1014");
		meMap.put("PDW-R", "smPDR");
		meMap.put("QBB-95","mgQBB95");
		return meMap;
	}
	
	private HashMap<String, String> vehicleHashMap() {
		meMap = new HashMap<String, String>();
         meMap.put("UH-1Y VENOM","trUH1");
         meMap.put("AAV-7A1 AMTRAC","trAAV");
         meMap.put("M220 TOW LAUNCHER","atTOW");
         meMap.put("F-35","jfx1F35JSF");
         meMap.put("T-90A","mbtT90");
         meMap.put("SKID LOADER","trSkid");
         meMap.put("LAV-AD","aaLAV");
         meMap.put("MI-28 HAVOC","ahMi28");
         meMap.put("GAZ-3937 VODNIK","trVod");
         meMap.put("DPV","trDpv");
         meMap.put("9K22 TUNGUSKA-M","aa9k22");
         meMap.put("AH-1Z VIPER","ahAH1Z");
         meMap.put("BMP-2M","ifvBMP");
         meMap.put("LAV-25","ifvLAV");
         meMap.put("M1 ABRAMS","mbtM1A");
         meMap.put("RHIB BOAT","trRIB");
         meMap.put("A-10 THUNDERBOLT","jaA10");
         meMap.put("KA-60 KASATKA","trKA60");
         meMap.put("PANTSIR-S1","saaPant");
         meMap.put("F/A-18E SUPER HORNET","jfF18");
         meMap.put("SU-25TM FROGFOOT","jaSU25");
         meMap.put("M1114 HMMWV","trHum");
         meMap.put("CENTURION C-RAM","saaCRAM");
         meMap.put("VDV BUGGY","trVDV");
         meMap.put("Z-11W","shz11");
         meMap.put("9M133 KORNET LAUNCHER","sKorn");
         meMap.put("GROWLER ITV","trITV");
         meMap.put("SU-35BM FLANKER-E","jfSu35");
         meMap.put("BTR-90","ifvBTR90");
         meMap.put("AH-6J LITTLE BIRD","shAH6");
	return meMap;
}
}
