package com.bf3stats;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class Tabs extends TabActivity {
	private int statname = 0;
	private Context context;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabslayout);
		context = getApplicationContext();
		Resources ressources = getResources(); 
		TabHost tabHost = getTabHost(); 
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
        	int value1 = extras.getInt("stats");
	        statname=value1;
	        
        }
		//Overview
		Intent intentOverview = new Intent().setClass(this, PlayerInfo.class);
		TabSpec tabSpecOverview = tabHost
			.newTabSpec("Overview")
			.setIndicator("", ressources.getDrawable(R.drawable.overview))
			.setContent(intentOverview);
		
		//Weapons
		Intent intentWeapons = new Intent().setClass(this, ShowStatistic.class);
		intentWeapons.putExtra("stats", "Weapon Stats");
		TabSpec tabSpecWeapons = tabHost
				.newTabSpec("Weapons")
				.setIndicator("", ressources.getDrawable(R.drawable.weapons))
				.setContent(intentWeapons);
		
		// Vehicle Stats tab
		Intent intentVehicle = new Intent().setClass(this, ShowStatistic.class);
		intentVehicle.putExtra("stats", "Vehicle Stats"); 
		TabSpec tabSpecVehicle = tabHost
				.newTabSpec("Vehicle Stats")
				.setIndicator("", ressources.getDrawable(R.drawable.vehicles))
				.setContent(intentVehicle);
		
		// Medal tab
		Intent intentMedal = new Intent().setClass(this, MedalsView.class);
		TabSpec tabSpecMedals = tabHost
			.newTabSpec("Medals")
			.setIndicator("", ressources.getDrawable(R.drawable.medals))
			.setContent(intentMedal);
		
		// Ribons tab
		Intent intentRibons = new Intent().setClass(this, RibonView.class);
		TabSpec tabSpecRibons = tabHost
				.newTabSpec("Ribons")
				.setIndicator("", ressources.getDrawable(R.drawable.ribons))
				.setContent(intentRibons);
		// Assighnments tab
		Intent intentAssighnments = new Intent().setClass(this, ShowStatistic.class);
		intentAssighnments.putExtra("stats", "Assignments");
		TabSpec tabSpecAssighnments = tabHost
				.newTabSpec("Assighnments")
				.setIndicator("", ressources.getDrawable(R.drawable.assighments))
				.setContent(intentAssighnments);
		// Equipement TAB tab
		Intent intentEquipement = new Intent().setClass(this, ShowStatistic.class);
		intentEquipement.putExtra("stats", "Equipement");
		TabSpec tabSpecEquipement = tabHost
				.newTabSpec("Equipement")
				.setIndicator("", ressources.getDrawable(R.drawable.equipement))
				.setContent(intentEquipement);
		// Weapon Unlocks tab
		Intent intentWeaponUnlocks = new Intent().setClass(this, ShowStatistic.class);
		intentWeaponUnlocks.putExtra("stats", "Weapon Unlocks");
		TabSpec tabSpecWeaponUnlocks = tabHost
				.newTabSpec("Weapons Unlocks")
				.setIndicator("", ressources.getDrawable(R.drawable.weaponunlocks))
				.setContent(intentWeaponUnlocks);
		
		// Vehicle Unlocks tab
		Intent intentVehicleUnlocks = new Intent().setClass(this, ShowStatistic.class);
		intentVehicleUnlocks.putExtra("stats", "Vehicle Unlocks");
		TabSpec tabSpecVehicleUnlocks = tabHost
				.newTabSpec("Vehicle Unlocks")
				.setIndicator("", ressources.getDrawable(R.drawable.vehiclesunlocks))
				.setContent(intentVehicleUnlocks);
		
		// Medal Unlocks tab
		Intent intentMedalsUnlocks = new Intent().setClass(this, ShowStatistic.class);
		intentMedalsUnlocks.putExtra("stats", "Medals To Unlock");
		TabSpec tabSpecMedalsUnlocks = tabHost
				.newTabSpec("Medal Unlocks")
				.setIndicator("", ressources.getDrawable(R.drawable.medaltounlock))
				.setContent(intentMedalsUnlocks);
		
		// ABOUT tab
		Intent intentAbout = new Intent().setClass(this, About.class);
		TabSpec tabSpecAbout = tabHost
				.newTabSpec("About")
				.setIndicator("", ressources.getDrawable(R.drawable.about))
				.setContent(intentAbout);
		
				
		// add all tabs 
		tabHost.addTab(tabSpecOverview);
		tabHost.addTab(tabSpecWeapons);
		tabHost.addTab(tabSpecVehicle);
		tabHost.addTab(tabSpecMedals);
		tabHost.addTab(tabSpecRibons);
		tabHost.addTab(tabSpecAssighnments);
		tabHost.addTab(tabSpecEquipement);
		tabHost.addTab(tabSpecWeaponUnlocks);
		tabHost.addTab(tabSpecVehicleUnlocks);
		tabHost.addTab(tabSpecMedalsUnlocks);
		tabHost.addTab(tabSpecAbout);
		
		//set Windows tab as default (zero based)
		tabHost.setCurrentTab(statname);
	}
	
	@Override
    public void onBackPressed(){
		Intent i = new Intent(context, PlayerSelector.class);
		startActivity(i);
	}

}
