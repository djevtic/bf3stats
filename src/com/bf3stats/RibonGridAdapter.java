package com.bf3stats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class RibonGridAdapter extends BaseAdapter {
	private Context context;
	private final String[] ribonValues;
	private Player player;
	private JSONObject medals;
	private String ribonCount = "";
	private String imageLink = "";
	private String tag="djevtic";
	private String forLogName = "";
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;
 
	public RibonGridAdapter(Context context, String[] ribonValues) {
		this.context = context;
		this.ribonValues = ribonValues;
		player = player.getInstance();
		medals = player.getRibons();
		//Odavde poceto sa image JAR implementacijom
		LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(context);
	    imageManager = new ImageManager(context, settings);
	    imageTagFactory = new ImageTagFactory(context, R.drawable.oldguysscaledmenu);
	    imageTagFactory.setErrorImageId(R.drawable.oldguysscaledmenu);
	    imageLoader = imageManager.getLoader();
	    //Ovde je kraj
	}
 
	public View getView(int position, View convertView, ViewGroup parent) {
 
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View gridView;
 			gridView = new View(context);
			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.ribongridlayout, null);
			// set value into textview
			TextView textView = (TextView) gridView.findViewById(R.id.ribonCount);
			textView.setText(ribonValues[position]);
			// set image based on selected text
			ImageView imageView = (ImageView) gridView.findViewById(R.id.ribonImage);
			String medal = ribonValues[position];
			try {
				imageLink = medals.getJSONObject(medal).getString("img_medium");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			TextView medalID = (TextView) gridView.findViewById(R.id.ribonID);
			medalID.setText(medal);
			setImage(imageView,imageLink);
			//new DownloadImageTask(imageView).execute(/*"http://dl.dropbox.com/u/1480498/bf3/"+*/imageLink);
			try {
				ribonCount = "x"+medals.getJSONObject(medal).getInt("count");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!ribonCount.equals("x0")){
				textView.setText(""+ribonCount);
			}else{
				textView.setText(ribonCount);
				imageView.setAlpha(100);
			}

 
		return gridView;
	}
 
	public int getCount() {
		return ribonValues.length;
	}
 
	public Object getItem(int position) {
		return null;
	}
 
	public long getItemId(int position) {
		return 0;
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;
		private String extStorageDirectory;

	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    protected Bitmap doInBackground(String... urls) {
	    	String filename = null;
	        String urldisplay = urls[0];
			extStorageDirectory = Environment.getExternalStorageDirectory().toString();
	        String[] result = urldisplay.split("/");
	        String urltoDropBox = "http://dl.dropbox.com/u/1480498/bf3/";
	        Bitmap mIcon11 = null;
	        boolean cardStatus = getCardState();
	        if(cardStatus){
	        	File dir = new File(extStorageDirectory+"/bf3statistic");
				try{
				  if(dir.mkdirs()) {
				  } else {
				  }
				}catch(Exception e){
				  e.printStackTrace();
				}
				File newdir = new File(extStorageDirectory+"/bf3statistic/"+urldisplay.substring(0, urldisplay.indexOf('/')));
				try{
					  if(newdir.mkdirs()) {
					  } else {
					  }
					}catch(Exception e){
					  e.printStackTrace();
					}
				filename = urldisplay.substring(urldisplay.indexOf('/')+1);
				File newfile = new File(newdir+"/"+filename);
				if(!newfile.exists()){
					downloadFile(urltoDropBox+urldisplay, newfile);
					try {
			            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
			            mIcon11 = BitmapFactory.decodeStream(in);
			        } catch (Exception e) {
			            Log.e("Error", e.getMessage());
			            e.printStackTrace();
			        }
				}else{
					mIcon11 = BitmapFactory.decodeFile(newfile.getAbsolutePath());
				}
	        }else{
	        	try {
		            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
		            mIcon11 = BitmapFactory.decodeStream(in);
		        } catch (Exception e) {
		            Log.e("Error", e.getMessage());
		            e.printStackTrace();
		        }
	        }
	        /*try {
	            InputStream in = new java.net.URL(urltoDropBox+urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }*/
	        return mIcon11;
	    }

	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}
	
	public boolean getCardState(){
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		return mExternalStorageAvailable;
	}
	
	private static void downloadFile(String url, File outputFile) {
		  try {
		      URL u = new URL(url);
		      URLConnection conn = u.openConnection();
		      int contentLength = conn.getContentLength();
		      if(contentLength != -1){
		      DataInputStream stream = new DataInputStream(u.openStream());

		        byte[] buffer = new byte[contentLength];
		        stream.readFully(buffer);
		        stream.close();

		        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
		        fos.write(buffer);
		        fos.flush();
		        fos.close();
		      }
		  } catch(FileNotFoundException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  } catch (IOException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  }
		}

	private void setImage(ImageView imageView,String url){
		String link = "http://dl.dropbox.com/u/1480498/bf3/"+url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}
}
