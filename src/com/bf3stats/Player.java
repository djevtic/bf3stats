package com.bf3stats;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class Player extends Application{
	
	/*
	 * Static parameters
	 */
	private static final String url = "http://api.bf3stats.com/";
	//TAG_NAME use for all kinds of names
	private static final String TAG_NAME = "name";
	//used for getting platform
	private static final String TAG_PLATFORM = "plat";
	//ussed for getting language player use
	private static final String TAG_LANGUAGE = "language";
	//used for getting country name
	private static final String TAG_COUNTRY = "country_name";
	//getting country flag
	private static final String TAG_COUNTRY_FLAG = "country_img";
	//getting dogtags
	private static final String TAG_DOGTAGS = "dogtags";
	private static final String TAG_BASIC = "basic";
	private static final String TAG_ADVANCE = "advanced";
	private static final String TAG_DESC = "desc";
	//getting smal immage
	private static final String TAG_SMALL_IMAGE = "image_s";
	//getting stats
	private static final String TAG_STATS = "stats";
	private static final String TAG_RANK = "rank";
	//getting level of player
	private static final String TAG_LEVEL = "nr";
	//getting score
	private static final String TAG_SCORE = "score";
	//small image for rank
	private static final String TAG_TINY_IMAGE = "img_tiny";
	//medium image for rank
	private static final String TAG_MEDIUM_IMAGE = "img_medium";
	//returning scores per class and some more stats
	private static final String TAG_SCORES = "scores";
	//award score
	private static final String TAG_AWARD_SCORE = "award";
	//bonus score?
	private static final String TAG_BONUS = "bonus";
	//general score
	private static final String TAG_GENERAL ="general";
	//objective?
	private static final String TAG_OBJECTIVE = "objective";
	//squad score?
	private static final String TAG_SQUAD = "squad";
	//team score
	private static final String TAG_TEAM_SCORE = "team";
	//score from unlocks
	private static final String TAG_UNLOCKS = "unlock";
	//assault 
	private static final String TAG_ASSAULT = "assault";
	//enginer
	private static final String TAG_ENGINER = "engineer";
	//reacon
	private static final String TAG_REACON = "recon";
	//suport
	private static final String TAG_SUPORT = "support";
	//vehicles ALL
	private static final String TAG_VEHICLEALL="vehicleall";
	//AntiAir Vehicle score
	private static final String TAG_AAVEHICLE= "vehicleaa";
	//Attack helicopter 
	private static final String TAG_AHELI= "vehicleah";
	//Infantru fighting vehicle
	private static final String TAG_IFV="vehicleifv";
	//Jet airplane
	private static final String TAG_JET="vehiclejet";
	//Main Battle tank
	private static final String TAG_MBT="vehiclembt";
	//Scoute Helicopter
	private static final String TAG_SH="vehiclesh";
	//global tag
	private static final String TAG_GLOBALS="global";
	//enemies killed
	private static final String TAG_KILLS="kills";
	//player deatch
	private static final String TAG_DEATHS="deaths";
	//game won
	private static final String TAG_WINS="wins";
	//game lost
	private static final String TAG_LOST="losses";
	//shoots fired
	private static final String TAG_SHOOTS_FIRED="shots";
	//hit target
	private static final String TAG_HITS="hits";
	//headshots
	private static final String TAG_HEADSHOOTS="headshots";
	//longest headshot
	private static final String TAG_LONGEST_HEADSOOT="longesths";
	//time in game
	private static final String TAG_TIME="time";
	//time in vehicle
	private static final String TAG_TIME_VEHICLE="vehicletime";
	//vehicle kills
	private static final String TAG_VEHICLE_KILLS="vehiclekills";
	//revives
	private static final String TAG_REVIVE="revives";
	//kill assistt
	private static final String TAG_KILL_ASSIST="killassists";
	//resuplies
	private static final String TAG_RESUPLY="resupplies";
	//heals
	private static final String TAG_HEALS="heals";
	//repairs
	private static final String TAG_REPAIRES="repairs";
	//rounds played
	private static final String TAG_ROUNDS_PLAYES="rounds";
	//finished rounds??????
	private static final String TAG_FINISHED_ROUNDS="elo_games";
	//killstreakbonus
	private static final String TAG_KILLSTREEK="killstreakbonus";
	//vehicledestroyassist
	private static final String TAG_VEHICLE_DESTROY_ASSIST="vehicledestroyassist";
	//vehicledestroyed
	private static final String TAG_VEHICLE_DESTROY="vehicledestroyed";
	//dogtags collected
	private static final String TAG_DOGTAGS_TAKEN="dogtags";
	//avanger kills
	private static final String TAG_AVANGER_KILLS="avengerkills";
	//saviorkills
	private static final String TAG_SAVIOR_KILLS="saviorkills";
	//damagaassists
	private static final String TAG_DAMAGE_ASSIST="damagaassists";
	//suppression
	private static final String TAG_SUPPRESION="suppression";
	//nemesisstreak
	private static final String TAG_NEMMESIS_STREAK="nemesisstreak";
	//nemesiskills
	private static final String TAG_NEMESSIS_KILLS="nemesiskills";
	//mcomdest
	private static final String TAG_MCOM_DESTROYED="mcomdest";
	//mcomdefkills number of players killed during deffence
	private static final String TAG_MCOM_DEFENDING_KILLS="mcomdefkills";
	//flagcaps
	private static final String TAG_FLAG_CAP="flagcaps";
	//flagdef
	private static final String TAG_FLAG_DEFENDED_KILS="flagdef";
	//skils
	private static final String TAG_SKILL="elo";
	//getting weapons
	private static final String TAG_WEAPONS = "weapons";
	//getting vehicles
	private static final String TAG_VEHICLES = "vehicles";
	private static final int IMAGE_MAX_SIZE = 70;
	/*
	 * Data which we need to show to user
	 */
	private String playerName, playerPlatform, playerLanguage, playerCountryName, playerCountryImage, playerBasicDoggtagName, playerBasicDogtagImage, 
					playerBasicDogtagDesc, playerAddvenceDogtagDesc, playerAddvenceDogtagName, playerAddvanceDogtagImage, playerRank, playerRankImmage;
	
	private long playerScoutHellyScore, playerScore,playerAwardPoints,playerAssaultScore,playerBonusPoints,playerEnginerScore,
					playerGeneralScore,playerObjectiveScore,playerReaconScore, playerSquadScore,playerSuportScore, playerTeamScore,playerUnlocksScore,playerAttackHeliScore,
					playerAAVehicleScore,playerVehicleScore,playerIFVScore,playerJetScore,playerMBTScore,playerGlobalKills, playerGlobalDeaths, 
					playerGameWin, playerGameLose, playerShootsFired, playerHits, playerHeadShoots, playerLongestHeadshoot, playerGameTime, 
					playerVehicleTime, playerVehicleKills, playerRevives, playerKillAssist, playerResuply,	playerHeals, playerRepaires, 
					playerRoundsPlayed, playerRoundsFinished, playerKillStreek, playerVehicleDestroyedAssistance, playerVehicleDestroyed, playerDogtagsTaken, 
					playerAvangerKills, playerSavioureKills, playerDamageAssist, playerSupression, playerNemmesisStreak, playerNemessiskills, 
					playerMCOMDestroyed, playerMCOMDefendKills, playerFlagCaptured, playerFlagDefendedKills, playerSkill, playerScoreLeftToNextRank, playerNextRankScore, playerPreviouseRankScore;
	/*
	 * Player data for comparison, secound player data
	 */
	private String playerName2, playerPlatform2, playerLanguage2;
	
	private long playerScoutHellyScore2, playerScore2,playerAwardPoints2,playerAssaultScore2,playerBonusPoints2,playerEnginerScore2,
	playerGeneralScore2,playerObjectiveScore2,playerReaconScore2, playerSquadScore2,playerSuportScore2, playerTeamScore2,playerUnlocksScore2,playerAttackHeliScore2,
	playerAAVehicleScore2,playerVehicleScore2,playerIFVScore2,playerJetScore2,playerMBTScore2,playerGlobalKills2, playerGlobalDeaths2, 
	playerGameWin2, playerGameLose2, playerShootsFired2, playerHits2, playerHeadShoots2, playerLongestHeadshoot2, playerGameTime2, 
	playerVehicleTime2, playerVehicleKills2, playerRevives2, playerKillAssist2, playerResuply2,	playerHeals2, playerRepaires2, 
	playerRoundsPlayed2, playerRoundsFinished2, playerKillStreek2, playerVehicleDestroyedAssistance2, playerVehicleDestroyed2, playerDogtagsTaken2, 
	playerAvangerKills2, playerSavioureKills2, playerDamageAssist2, playerSupression2, playerNemmesisStreak2, playerNemessiskills2, 
	playerMCOMDestroyed2, playerMCOMDefendKills2, playerFlagCaptured2, playerFlagDefendedKills2, playerSkill2, playerScoreLeftToNextRank2, playerNextRankScore2, playerPreviouseRankScore2;
	
	/*
	 * From where we get data
	 */
	private String jsonString = null;
	private JSONObject jsonObject = null;
	private JSONObject dogtags = null;
	private JSONObject basicdogtag = null;
	private JSONObject advancedogtag = null;
	private JSONObject rank = null;
	private JSONObject rankAndScore = null;
	private JSONObject statPoints = null;
	private JSONObject globals = null;
	private JSONObject weapons = null;
	private JSONObject vehicles = null;
	private JSONObject medals = null;
	private JSONObject ribons = null;
	private JSONObject vehicleUnlocks = null;
	private JSONObject kit = null;
	private JSONObject equipement = null;
	private JSONObject vehiclekategories = null;
	private JSONObject assighments = null;
	
	/*
	 * Player 2 Data for comparison
	 */
	private String jsonString2 = null;
	private JSONObject jsonObject2 = null;
	private String rank2 = null;
	private JSONObject rankAndScore2 = null;
	private JSONObject score2 = null;
	private JSONObject statPoints2 = null;
	private JSONObject globals2 = null;
	private JSONObject weapons2 = null;
	private JSONObject vehicles2 = null;
	private JSONObject medals2 = null;
	private JSONObject ribons2 = null;
	private JSONObject vehicleUnlocks2 = null;
	private JSONObject kit2 = null;
	private JSONObject equipement2 = null;
	private JSONObject vehiclekategories2 = null;
	private JSONObject assighments2 = null;
	
	/*
	 * Utility stuff
	 */
	private String tag = "djevtic";
	private boolean connectionprobelem = false;
	public boolean weAreGood = true;
	private String sendURL = "";
	Runnable runnable = null;
	HashMap<String, Integer> meMap;
	String extStorageDirectory;
	private static String TAG = "MyMainApplication";
    private static Player singleton;
    public static Player getInstance(){return singleton;}
    private int count = 0;
	private Context context;
	InputStream content = null;
	HttpEntity entity = null;
	StatusLine statusLine = null;
	private JSONArray nextRank;
	private JSONObject row;
	private String sicretKey = "mu61oFEzPTqbTHmHTwNuk8ZRzA8JUu9T";
	private String appidentification = "zT6tePls8c";
	private Database db;
	private Cursor c = null;
	private boolean keyIsPressent;
	private StringBuffer something;
	private JSONObject jsonKey;
	private String jsonKeyString;
	private String status = "error";
	private String localIdent;
	private String localKey;
	private String player2name;
	private String player2platform;
	private HttpPost post;
	private int state;
	
	
	
	@Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
	public static long now()
	  {
	    return toUnixTime(Calendar.getInstance().getTime());
	  }

	  public static long toUnixTime(Date paramDate)
	  {
	    return paramDate.getTime() / 1000L;
	  }
	public int GetPlayer(final String playername, final String platform){
		setState(0);
		setPlayerName(playername);
		setPlayerPlatform(platform);
		context = getApplicationContext();
		weAreGood = false;
		runnable = new Runnable(){
	        public void run() {
	        	CheckAppKey();
	        	StringBuffer updatePlayer = UpdatePlayer(playername,platform,localIdent,""+now());
	        	if(updatePlayer!=null||updatePlayer.equals("ERROR")){
	        		ConnectingToStats(1);
	        	}else{
	        		StringBuffer addPlayer = AddPlayerToBfstats(playername,platform,localIdent,""+now());
	        		if(addPlayer!=null||addPlayer.equals("ERROR")){
	        			ConnectingToStats(1);
	        		}else{
	        			setState(1);
	        		}
	        	}
	        }
	    };
		new Thread(runnable).start();
		return this.state;
	}
	
	public int GetPlayers(final String playername1, final String platform1,final String playername2, final String platform2){
		setState(0);
		setPlayerName(playername1);
		setPlayerPlatform(platform1);
		setPlayer2Name(playername2);
		setPlayer2Platform(platform2);
		context = getApplicationContext();
		weAreGood = false;
		runnable = new Runnable()
	    {
	        public void run() {
	        	CheckAppKey();
	        	StringBuffer updatePlayer = UpdatePlayer(playername1,platform1,localIdent,""+now());
	        	if(updatePlayer!=null||updatePlayer.equals("ERROR")){
	        		ConnectingToStats(1);
	        	}else{
	        		setState(1);
	        	}
	        	StringBuffer updatePlayer2 = UpdatePlayer2(playername2,platform2,localIdent,""+now());
	        	if(updatePlayer2!=null||updatePlayer2.equals("ERROR")){
	        		ConnectingToStats(2);
	        	}else{
	        		setState(1);
	        	}
	        }
	    };
		new Thread(runnable).start();
		return this.state;
	}

	private void setState(int tempState){
		this.state = tempState;
	}
	
	private int getState(){
		return this.state;
	}
	
	private void setPlayer2Platform(String platform) {
		this.setPlayer2platform(platform);
		
	}
	private void setPlayer2Name(String playername3) {
		this.setPlayer2name(playername3);
		
	}
	protected StringBuffer UpdatePlayer2(String playerName, String platform, String appIdent, String timestamp) {
		//Build parameter string
        //String data = "time="+timestamp+"&ident="+appIdent+"&player="+playerName;
    	String data;
    	String sig;
    	
        try {
            
            // Send the request
            URL url = new URL("http://api.bf3stats.com/"+platform+"/playerupdate/");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            
            //JSON and Base64 encoding for "data" string
            JSONObject json = new JSONObject();
            try {
            	json.put("time", timestamp);
            	json.put("ident", appIdent);
            	json.put("player", playerName);
            } catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            data = json.toString();
            String encoded64data = str_replace(Base64.encodeToString(data.getBytes(),10));
            
            
            //SHA256-HMAC and Base64 encoding for "sig" string
            
            String encoded64sig = null;
            try {
	            SecretKey signingKey = new SecretKeySpec(localKey.getBytes(), "HMACSHA256");  
	            Mac mac = Mac.getInstance("HMACSHA256");
	            mac.init(signingKey);  
	            byte[] digest = mac.doFinal(encoded64data.getBytes());     //output of sha256  
	            encoded64sig = str_replace(Base64.encodeToString(digest,10));   // using it as an input  
            	} 
            catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
            catch (InvalidKeyException e) {
				e.printStackTrace();
			}  
            
            //write parameters
            writer.write("data="+encoded64data+"&sig="+encoded64sig);
            writer.flush();
            
            // Get the response
            StringBuffer answer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                answer.append(line);
            }
            writer.close();
            reader.close();
            
            //Return the response
            return answer;
            
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return null;
    }
	
	/*protected void FillHasMap() {
		meMap = new HashMap<String, Integer>();
		meMap.put("rankstiny/r0.png",R.drawable.r0);
		meMap.put("rankstiny/r1.png",R.drawable.r1);
		meMap.put("rankstiny/r2.png",R.drawable.r2);
		meMap.put("rankstiny/r3.png",R.drawable.r3);
		meMap.put("rankstiny/r4.png",R.drawable.r4);
		meMap.put("rankstiny/r5.png",R.drawable.r5);
		meMap.put("rankstiny/r6.png",R.drawable.r6);
		meMap.put("rankstiny/r7.png",R.drawable.r7);
		meMap.put("rankstiny/r8.png",R.drawable.r8);
		meMap.put("rankstiny/r9.png",R.drawable.r9);
		meMap.put("rankstiny/r10.png",R.drawable.r10);
		meMap.put("rankstiny/r11.png",R.drawable.r11);
		meMap.put("rankstiny/r12.png",R.drawable.r12);
		meMap.put("rankstiny/r13.png",R.drawable.r13);
		meMap.put("rankstiny/r14.png",R.drawable.r14);
		meMap.put("rankstiny/r15.png",R.drawable.r15);
		meMap.put("rankstiny/r16.png",R.drawable.r16);
		meMap.put("rankstiny/r17.png",R.drawable.r17);
		meMap.put("rankstiny/r18.png",R.drawable.r18);
		meMap.put("rankstiny/r19.png",R.drawable.r19);
		meMap.put("rankstiny/r20.png",R.drawable.r20);
		meMap.put("rankstiny/r21.png",R.drawable.r21);
		meMap.put("rankstiny/r22.png",R.drawable.r22);
		meMap.put("rankstiny/r23.png",R.drawable.r23);
		meMap.put("rankstiny/r24.png",R.drawable.r24);
		meMap.put("rankstiny/r25.png",R.drawable.r25);
		meMap.put("rankstiny/r26.png",R.drawable.r26);
		meMap.put("rankstiny/r27.png",R.drawable.r27);
		meMap.put("rankstiny/r28.png",R.drawable.r28);
		meMap.put("rankstiny/r29.png",R.drawable.r29);
		meMap.put("rankstiny/r30.png",R.drawable.r30);
		meMap.put("rankstiny/r31.png",R.drawable.r31);
		meMap.put("rankstiny/r32.png",R.drawable.r32);
		meMap.put("rankstiny/r33.png",R.drawable.r33);
		meMap.put("rankstiny/r34.png",R.drawable.r34);
		meMap.put("rankstiny/r35.png",R.drawable.r35);
		meMap.put("rankstiny/r36.png",R.drawable.r36);
		meMap.put("rankstiny/r37.png",R.drawable.r37);
		meMap.put("rankstiny/r38.png",R.drawable.r38);
		meMap.put("rankstiny/r39.png",R.drawable.r39);
		meMap.put("rankstiny/r40.png",R.drawable.r40);
		meMap.put("rankstiny/r41.png",R.drawable.r41);
		meMap.put("rankstiny/r42.png",R.drawable.r42);
		meMap.put("rankstiny/r43.png",R.drawable.r43);
		meMap.put("rankstiny/r44.png",R.drawable.r44);
		meMap.put("rankstiny/r45.png",R.drawable.r45);
		meMap.put("rankstiny/ss1.png",R.drawable.ss1);
		meMap.put("rankstiny/ss2.png",R.drawable.ss2);
		meMap.put("rankstiny/ss3.png",R.drawable.ss3);
		meMap.put("rankstiny/ss4.png",R.drawable.ss4);
		meMap.put("rankstiny/ss5.png",R.drawable.ss5);
		meMap.put("rankstiny/ss6.png",R.drawable.ss6);
		meMap.put("rankstiny/ss7.png",R.drawable.ss7);
		meMap.put("rankstiny/ss8.png",R.drawable.ss8);
		meMap.put("rankstiny/ss9.png",R.drawable.ss9);
		meMap.put("rankstiny/ss10.png",R.drawable.ss10);
		meMap.put("rankstiny/ss11.png",R.drawable.ss11);
		meMap.put("rankstiny/ss12.png",R.drawable.ss12);
		meMap.put("rankstiny/ss13.png",R.drawable.ss13);
		meMap.put("rankstiny/ss14.png",R.drawable.ss14);
		meMap.put("rankstiny/ss15.png",R.drawable.ss15);
		meMap.put("rankstiny/ss16.png",R.drawable.ss16);
		meMap.put("rankstiny/ss17.png",R.drawable.ss17);
		meMap.put("rankstiny/ss18.png",R.drawable.ss18);
		meMap.put("rankstiny/ss19.png",R.drawable.ss19);
		meMap.put("rankstiny/ss20.png",R.drawable.ss20);
		meMap.put("rankstiny/ss21.png",R.drawable.ss21);
		meMap.put("rankstiny/ss22.png",R.drawable.ss22);
		meMap.put("rankstiny/ss23.png",R.drawable.ss23);
		meMap.put("rankstiny/ss24.png",R.drawable.ss24);
		meMap.put("rankstiny/ss25.png",R.drawable.ss25);
		meMap.put("rankstiny/ss26.png",R.drawable.ss26);
		meMap.put("rankstiny/ss27.png",R.drawable.ss27);
		meMap.put("rankstiny/ss28.png",R.drawable.ss28);
		meMap.put("rankstiny/ss29.png",R.drawable.ss29);
		meMap.put("rankstiny/ss30.png",R.drawable.ss30);
		meMap.put("rankstiny/ss31.png",R.drawable.ss31);
		meMap.put("rankstiny/ss32.png",R.drawable.ss32);
		meMap.put("rankstiny/ss33.png",R.drawable.ss33);
		meMap.put("rankstiny/ss34.png",R.drawable.ss34);
		meMap.put("rankstiny/ss35.png",R.drawable.ss35);
		meMap.put("rankstiny/ss36.png",R.drawable.ss36);
		meMap.put("rankstiny/ss37.png",R.drawable.ss37);
		meMap.put("rankstiny/ss38.png",R.drawable.ss38);
		meMap.put("rankstiny/ss39.png",R.drawable.ss39);
		meMap.put("rankstiny/ss40.png",R.drawable.ss40);
		meMap.put("rankstiny/ss41.png",R.drawable.ss41);
		meMap.put("rankstiny/ss42.png",R.drawable.ss42);
		meMap.put("rankstiny/ss43.png",R.drawable.ss43);
		meMap.put("rankstiny/ss44.png",R.drawable.ss44);
		meMap.put("rankstiny/ss45.png",R.drawable.ss45);
		meMap.put("rankstiny/ss46.png",R.drawable.ss46);
		meMap.put("rankstiny/ss47.png",R.drawable.ss47);
		meMap.put("rankstiny/ss48.png",R.drawable.ss48);
		meMap.put("rankstiny/ss49.png",R.drawable.ss49);
		meMap.put("rankstiny/ss50.png",R.drawable.ss50);
		meMap.put("rankstiny/ss51.png",R.drawable.ss51);
		meMap.put("rankstiny/ss52.png",R.drawable.ss52);
		meMap.put("rankstiny/ss53.png",R.drawable.ss53);
		meMap.put("rankstiny/ss54.png",R.drawable.ss54);
		meMap.put("rankstiny/ss55.png",R.drawable.ss55);
		meMap.put("rankstiny/ss56.png",R.drawable.ss56);
		meMap.put("rankstiny/ss57.png",R.drawable.ss57);
		meMap.put("rankstiny/ss58.png",R.drawable.ss58);
		meMap.put("rankstiny/ss59.png",R.drawable.ss59);
		meMap.put("rankstiny/ss60.png",R.drawable.ss60);
		meMap.put("rankstiny/ss61.png",R.drawable.ss61);
		meMap.put("rankstiny/ss62.png",R.drawable.ss62);
		meMap.put("rankstiny/ss63.png",R.drawable.ss63);
		meMap.put("rankstiny/ss64.png",R.drawable.ss64);
		meMap.put("rankstiny/ss65.png",R.drawable.ss65);
		meMap.put("rankstiny/ss66.png",R.drawable.ss66);
		meMap.put("rankstiny/ss67.png",R.drawable.ss67);
		meMap.put("rankstiny/ss68.png",R.drawable.ss68);
		meMap.put("rankstiny/ss69.png",R.drawable.ss69);
		meMap.put("rankstiny/ss70.png",R.drawable.ss70);
		meMap.put("rankstiny/ss71.png",R.drawable.ss71);
		meMap.put("rankstiny/ss72.png",R.drawable.ss72);
		meMap.put("rankstiny/ss73.png",R.drawable.ss73);
		meMap.put("rankstiny/ss74.png",R.drawable.ss74);
		meMap.put("rankstiny/ss75.png",R.drawable.ss75);
		meMap.put("rankstiny/ss76.png",R.drawable.ss76);
		meMap.put("rankstiny/ss77.png",R.drawable.ss77);
		meMap.put("rankstiny/ss78.png",R.drawable.ss78);
		meMap.put("rankstiny/ss79.png",R.drawable.ss79);
		meMap.put("rankstiny/ss80.png",R.drawable.ss80);
		meMap.put("rankstiny/ss81.png",R.drawable.ss81);
		meMap.put("rankstiny/ss82.png",R.drawable.ss82);
		meMap.put("rankstiny/ss83.png",R.drawable.ss83);
		meMap.put("rankstiny/ss84.png",R.drawable.ss84);
		meMap.put("rankstiny/ss85.png",R.drawable.ss85);
		meMap.put("rankstiny/ss86.png",R.drawable.ss86);
		meMap.put("rankstiny/ss87.png",R.drawable.ss87);
		meMap.put("rankstiny/ss88.png",R.drawable.ss88);
		meMap.put("rankstiny/ss89.png",R.drawable.ss89);
		meMap.put("rankstiny/ss90.png",R.drawable.ss90);
		meMap.put("rankstiny/ss91.png",R.drawable.ss91);
		meMap.put("rankstiny/ss92.png",R.drawable.ss92);
		meMap.put("rankstiny/ss93.png",R.drawable.ss93);
		meMap.put("rankstiny/ss94.png",R.drawable.ss94);
		meMap.put("rankstiny/ss95.png",R.drawable.ss95);
		meMap.put("rankstiny/ss96.png",R.drawable.ss96);
		meMap.put("rankstiny/ss97.png",R.drawable.ss97);
		meMap.put("rankstiny/ss98.png",R.drawable.ss98);
		meMap.put("rankstiny/ss99.png",R.drawable.ss99);
		meMap.put("rankstiny/ss100.png",R.drawable.ss100);
	}*/

	public void downloadImageFiles() {
		String filename = null;
		String fileURL = "http://dl.dropbox.com/u/1480498/bf3/";
		String[] immagesarray = imageDownloadArray();
		extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		File dir = new File(extStorageDirectory+"/bf3statistic");
		try{
		  if(dir.mkdirs()) {
		  } else {
		  }
		}catch(Exception e){
		  e.printStackTrace();
		}
		for(int i=0; immagesarray.length>i;i++){
			//int y = immagesarray[i].indexOf('/');
			File newdir = new File(extStorageDirectory+"/bf3statistic/"+immagesarray[i].substring(0, immagesarray[i].indexOf('/')));
			try{
				  if(newdir.mkdirs()) {
				  } else {
				  }
				}catch(Exception e){
				  e.printStackTrace();
				}
			filename = immagesarray[i].substring(immagesarray[i].indexOf('/')+1);
			File newfile = new File(newdir+filename);
			if(!newfile.exists()){
				downloadFile(fileURL+immagesarray[i], newfile);
			}else{
				//File exist
			}
			
		}
		
	}
	
	private static void downloadFile(String url, File outputFile) {
		  try {
		      URL u = new URL(url);
		      URLConnection conn = u.openConnection();
		      int contentLength = conn.getContentLength();
		      if(contentLength != -1){
		      DataInputStream stream = new DataInputStream(u.openStream());
		      
		        byte[] buffer = new byte[contentLength];
		        stream.readFully(buffer);
		        stream.close();

		        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
		        fos.write(buffer);
		        fos.flush();
		        fos.close();
		      }
		  } catch(FileNotFoundException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  } catch (IOException e) {
			  e.printStackTrace();
		      return; // swallow a 404
		  }
		}
	
	
	
	public boolean getCardState(){
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		return mExternalStorageAvailable;
	}
	public void getImage(String link, ImageView imageview){
		//String imagePath = Environment.getExternalStorageDirectory().toString() + PATH_TO_IMAGE;
		/*BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(getResources(), R.drawable.m30, options);
		int imageHeight = options.outHeight;
		int imageWidth = options.outWidth;
		String imageType = options.outMimeType;
		*/
		//Bitmap bmImg = BitmapFactory.decodeFile(extStorageDirectory+"/bf3statistic/"+link.substring(link.indexOf('/')+1));
		//bmImg = decodeFile(extStorageDirectory+"/bf3statistic/"+link.substring(link.indexOf('/')+1));
		Bitmap bmImg = decodeFile(new  File(extStorageDirectory+"/bf3statistic/"+link/*.substring(link.indexOf('/')+1))*/));
		if(bmImg==null){
			imageview.setImageResource(R.drawable.empty);
		}else{
			imageview.setImageBitmap(bmImg);
		}
	}
	
	private Bitmap decodeFile(File f){
	    try {
	        //Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f),null,o);

	        //The new size we want to scale to
	        final int REQUIRED_SIZE=50;

	        //Find the correct scale value. It should be the power of 2.
	        int scale=1;
	        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
	            scale*=2;

	        //Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {}
	    return null;
	}
	

	
	//Returning array of strings with target for every image we need to download
	private String[] imageDownloadArray(){
		String[] menuList = new String[] {
				//rank immages
			"ranksmedium/ss1.png",
			  "ranksmedium/ss2.png",
			  "ranksmedium/ss3.png",
			  "ranksmedium/ss4.png",
			  "ranksmedium/ss5.png",
			  "ranksmedium/ss6.png",
			  "ranksmedium/ss7.png",
			  "ranksmedium/ss8.png",
			  "ranksmedium/ss9.png",
			  "ranksmedium/ss10.png",
			  "ranksmedium/ss11.png",
			  "ranksmedium/ss12.png",
			  "ranksmedium/ss13.png",
			  "ranksmedium/ss14.png",
			  "ranksmedium/ss15.png",
			  "ranksmedium/ss16.png",
			  "ranksmedium/ss17.png",
			  "ranksmedium/ss18.png",
			  "ranksmedium/ss19.png",
			  "ranksmedium/ss20.png",
			  "ranksmedium/ss21.png",
			  "ranksmedium/ss22.png", 
			  "ranksmedium/ss23.png", 
			  "ranksmedium/ss24.png", 
			  "ranksmedium/ss25.png", 
			  "ranksmedium/ss26.png",
			  "ranksmedium/ss27.png",
			  "ranksmedium/ss28.png",
			  "ranksmedium/ss29.png",
			  "ranksmedium/ss30.png", 
			  "ranksmedium/ss31.png",
			  "ranksmedium/ss32.png", 
			  "ranksmedium/ss33.png",
			  "ranksmedium/ss34.png", 
			  "ranksmedium/ss35.png", 
			  "ranksmedium/ss36.png", 
			  "ranksmedium/ss37.png", 
			  "ranksmedium/ss38.png",
			  "ranksmedium/ss39.png", 
			  "ranksmedium/ss40.png", 
			  "ranksmedium/ss41.png", 
			  "ranksmedium/ss42.png", 
			  "ranksmedium/ss43.png", 
			  "ranksmedium/ss44.png", 
			  "ranksmedium/ss45.png", 
			  "ranksmedium/ss46.png", 
			  "ranksmedium/ss47.png", 
			  "ranksmedium/ss48.png", 
			  "ranksmedium/ss49.png", 
			  "ranksmedium/ss50.png", 
			  "ranksmedium/ss51.png", 
			  "ranksmedium/ss52.png", 
			  "ranksmedium/ss53.png", 
			  "ranksmedium/ss54.png", 
			  "ranksmedium/ss55.png", 
			  "ranksmedium/ss56.png", 
			  "ranksmedium/ss57.png", 
			  "ranksmedium/ss58.png", 
			  "ranksmedium/ss59.png", 
			  "ranksmedium/ss60.png", 
			  "ranksmedium/ss61.png", 
			  "ranksmedium/ss62.png", 
			  "ranksmedium/ss63.png", 
			  "ranksmedium/ss64.png", 
			  "ranksmedium/ss65.png", 
			  "ranksmedium/ss66.png", 
			  "ranksmedium/ss67.png", 
			  "ranksmedium/ss68.png", 
			  "ranksmedium/ss69.png", 
			  "ranksmedium/ss70.png", 
			  "ranksmedium/ss71.png", 
			  "ranksmedium/ss72.png", 
			  "ranksmedium/ss73.png", 
			  "ranksmedium/ss74.png", 
			  "ranksmedium/ss75.png", 
			  "ranksmedium/ss76.png", 
			  "ranksmedium/ss77.png", 
			  "ranksmedium/ss78.png", 
			  "ranksmedium/ss79.png", 
			  "ranksmedium/ss80.png", 
			  "ranksmedium/ss81.png", 
			  "ranksmedium/ss82.png", 
			  "ranksmedium/ss83.png", 
			  "ranksmedium/ss84.png", 
			  "ranksmedium/ss85.png", 
			  "ranksmedium/ss86.png", 
			  "ranksmedium/ss87.png", 
			  "ranksmedium/ss88.png", 
			  "ranksmedium/ss89.png", 
			  "ranksmedium/ss90.png", 
			  "ranksmedium/ss91.png", 
			  "ranksmedium/ss92.png", 
			  "ranksmedium/ss93.png", 
			  "ranksmedium/ss94.png", 
			  "ranksmedium/ss95.png", 
			  "ranksmedium/ss96.png", 
			  "ranksmedium/ss97.png", 
			  "ranksmedium/ss98.png",
			  "ranksmedium/ss99.png",
			  "ranksmedium/ss100.png",
			  //ribons
               "awards_m/r01.png",
               "awards_m/r02.png",
               "awards_m/r03.png",
               "awards_m/r04.png",
               "awards_m/r05.png",
               "awards_m/r06.png",
               "awards_m/r07.png",
               "awards_m/r08.png",
               "awards_m/r09.png",
               "awards_m/r10.png",
               "awards_m/r11.png",
               "awards_m/r12.png",
               "awards_m/r13.png",
               "awards_m/r14.png",
               "awards_m/r15.png",
               "awards_m/r16.png",
               "awards_m/r17.png",
               "awards_m/r18.png",
               "awards_m/r19.png",
               "awards_m/r20.png",
               "awards_m/r21.png",
               "awards_m/r22.png",
               "awards_m/r23.png",
               "awards_m/r24.png",
               "awards_m/r25.png",
               "awards_m/r26.png",
               "awards_m/r27.png",
               "awards_m/r28.png",
               "awards_m/r29.png",
               "awards_m/r30.png",
               "awards_m/r31.png",
               "awards_m/r32.png",
               "awards_m/r33.png",
               "awards_m/r34.png",
               "awards_m/r35.png",
               "awards_m/r36.png",
               "awards_m/r37.png",
               "awards_m/r38.png",
               "awards_m/r39.png",
               "awards_m/r40.png",
               "awards_m/r41.png",
               "awards_m/r42.png",
               "awards_m/r43.png",
               "awards_m/r44.png",
               "awards_m/r45.png",
               //medals
               "awards_m/m01.png",
               "awards_m/m02.png",
               "awards_m/m03.png",
               "awards_m/m04.png",
               "awards_m/m05.png",
               "awards_m/m06.png",
               "awards_m/m07.png",
               "awards_m/m08.png",
               "awards_m/m09.png",
               "awards_m/m10.png",
               "awards_m/m11.png",
               "awards_m/m12.png",
               "awards_m/m13.png",
               "awards_m/m14.png",
               "awards_m/m15.png",
               "awards_m/m16.png",
               "awards_m/m17.png",
               "awards_m/m18.png",
               "awards_m/m19.png",
               "awards_m/m20.png",
               "awards_m/m21.png",
               "awards_m/m22.png",
               "awards_m/m23.png",
               "awards_m/m24.png",
               "awards_m/m25.png",
               "awards_m/m26.png",
               "awards_m/m27.png",
               "awards_m/m28.png",
               "awards_m/m29.png",
               "awards_m/m30.png",
               "awards_m/m31.png",
               "awards_m/m32.png",
               "awards_m/m33.png",
               "awards_m/m34.png",
               "awards_m/m35.png",
               "awards_m/m36.png",
               "awards_m/m37.png",
               "awards_m/m38.png",
               "awards_m/m39.png",
               "awards_m/m40.png",
               "awards_m/m41.png",
               "awards_m/m42.png",
               "awards_m/m43.png",
               "awards_m/m44.png",
               "awards_m/m45.png",
               "awards_m/m46.png",
               "awards_m/m47.png",
               "awards_m/m48.png",
               "awards_m/m49.png",
               "awards_m/m50.png", 
               //servicestar
               "servicestars/servicestar.png",
               //weapons
               	"weapons/mp7.png",
                 "weapons/glock17.png",
                 "weapons/m98b.png",
                 "weapons/sg553lb.png",
                 "weapons/remington870.png",
                 "weapons/mp443_silenced.png",
                 "weapons/rpg7.png",
                 "weapons/xp2_mtar.png",
                 "weapons/kh2002.png",
                 "weapons/aks74u.png",
                 "weapons/m1911silenced_fancy.png",
                 "weapons/m27.png",
                 "weapons/g36c.png",
                 "weapons/taurus44.png",
                 "weapons/asval.png",
                 "weapons/dao12.png",
                 "weapons/m320.png",
                 "weapons/ump.png",
                 "weapons/sa18igla.png",
                 "weapons/scarh.png",
                 "weapons/mp412rex.png",
                 "weapons/m416.png",
                 "weapons/ak74m.png",
                 "weapons/pecheneg.png",
                 "weapons/m60.png",
                 "weapons/m240.png",
                 "weapons/m16a4.png",
                 "weapons/m1911flashlight_fancy.png",
                 "weapons/pp2000.png",
                 "weapons/g3.png",
                 "weapons/m4a1.png",
                 "weapons/aek971.png",
                 "weapons/xp2_steyraug.png",
                 "weapons/mk11.png",
                 "weapons/an94.png",
                 "weapons/xp2_spas12.png",
                 "weapons/taurus_scope.png",
                 "weapons/usas12.png",
                 "weapons/mp443_grach.png",
                 "weapons/rpk.png",
                 "weapons/combatknife.png",
                 "weapons/xp1_l85a2.png",
                 "weapons/sv98.png",
                 "weapons/m40a5.png",
                 "weapons/xp2_scarl.png",
                 "weapons/xp2_acr.png",
                 "weapons/xp1_mg36.png",
                 "weapons/saiga12.png",
                 "weapons/xp1_famas.png",
                 "weapons/m1911tactical_fancy.png",
                 "weapons/m93r.png",
                 "weapons/m9_silenced.png",
                 "weapons/m1911.png",
                 "weapons/svd.png",
                 "weapons/xp1_jackhammer.png",
                 "weapons/glock18_silenced.png",
                 "weapons/m249.png",
                 "weapons/glock17_silenced.png",
                 "weapons/xp1_qbu88.png",
                 "weapons/m9.png",
                 "weapons/m26mass.png",
                 "weapons/xp2_jng90.png",
                 "weapons/xp2_l86.png",
                 "weapons/xp2_lsat.png",
                 "weapons/smaw.png",
                 "weapons/p90.png",
                 "weapons/xp2_hk417.png",
                 "weapons/a91.png",
                 "weapons/sks.png",
                 "weapons/xp1_l96.png",
                 "weapons/xp1_pp19.png",
                 "weapons/type88.png",
                 "weapons/xp1_qbz95b.png",
                 "weapons/m9_flashlight.png",
                 "weapons/glock18.png",
                 "weapons/f2000.png",
                 "weapons/fgm148.png",
                 "weapons/xp2_mp5k.png",
                 "weapons/mp443_flashlight.png",
                 "weapons/xp1_hk53.png",
                 "weapons/fim92a_stinger.png",
                 "weapons/m39.png",
                 "weapons/m1014.png",
                 "weapons/magpul.png",
                 "weapons/xp1_qbb95.png",
                 //dogtags
                 "dogtags_s/40mmgl.png",
                 "dogtags_s/870-mcs.png",
                 "dogtags_s/a91_left.png",
                 "dogtags_s/a91_right.png",
                 "dogtags_s/aa.png",
                 "dogtags_s/aaservicestar100.png",
                 "dogtags_s/aaservicestar10.png",
                 "dogtags_s/aaservicestar25.png",
                 "dogtags_s/aaservicestar50.png",
                 "dogtags_s/aaservicestar5.png",
                 "dogtags_s/aek971.png",
                 "dogtags_s/aheliservicestar100.png",
                 "dogtags_s/aheliservicestar10.png",
                 "dogtags_s/aheliservicestar25.png",
                 "dogtags_s/aheliservicestar50.png",
                 "dogtags_s/aheliservicestar5.png",
                 "dogtags_s/ak-74m.png",
                 "dogtags_s/aks74u.png",
                 "dogtags_s/alienware.png",
                 "dogtags_s/an94-akaban.png",
                 "dogtags_s/asval_left.png",
                 "dogtags_s/asval_right.png",
                 "dogtags_s/at-mines.png",
                 "dogtags_s/attack-heli.png",
                 "dogtags_s/bf-1943.png",
                 "dogtags_s/bf-2142.png",
                 "dogtags_s/bf2.png",
                 "dogtags_s/bf-bc2.png",
                 "dogtags_s/bf-bc.png",
                 "dogtags_s/bf-p4f.png",
                 "dogtags_s/bfv.png",
                 "dogtags_s/bundle10.png",
                 "dogtags_s/bundle1.png",
                 "dogtags_s/bundle2.png",
                 "dogtags_s/bundle3.png",
                 "dogtags_s/bundle4.png",
                 "dogtags_s/bundle5.png",
                 "dogtags_s/bundle6.png",
                 "dogtags_s/bundle7.png",
                 "dogtags_s/bundle8.png",
                 "dogtags_s/bundle9.png",
                 "dogtags_s/c4.png",
                 "dogtags_s/claymore.png",
                 "dogtags_s/communitydogtag.png",
                 "dogtags_s/dao-12.png",
                 "dogtags_s/defaulttag_left.png",
                 "dogtags_s/defaulttag_right.png",
                 "dogtags_s/dice.png",
                 "dogtags_s/dogtags_razer_256.png",
                 "dogtags_s/dta001.png",
                 "dogtags_s/dta002.png",
                 "dogtags_s/dta003.png",
                 "dogtags_s/dta004.png",
                 "dogtags_s/dta005.png",
                 "dogtags_s/dta006.png",
                 "dogtags_s/dta007.png",
                 "dogtags_s/dta008.png",
                 "dogtags_s/dta009.png",
                 "dogtags_s/dta010.png",
                 "dogtags_s/dta011.png",
                 "dogtags_s/dta012.png",
                 "dogtags_s/dta013.png",
                 "dogtags_s/dta014.png",
                 "dogtags_s/dta015.png",
                 "dogtags_s/dta016.png",
                 "dogtags_s/dta017.png",
                 "dogtags_s/dta018.png",
                 "dogtags_s/dta019.png",
                 "dogtags_s/dta020.png",
                 "dogtags_s/dta021.png",
                 "dogtags_s/dta022.png",
                 "dogtags_s/dta023.png",
                 "dogtags_s/dta024.png",
                 "dogtags_s/dta025.png",
                 "dogtags_s/dta026.png",
                 "dogtags_s/dta027.png",
                 "dogtags_s/dta028.png",
                 "dogtags_s/dta029.png",
                 "dogtags_s/dta030.png",
                 "dogtags_s/dta031.png",
                 "dogtags_s/dta032.png",
                 "dogtags_s/dta033.png",
                 "dogtags_s/dta034.png",
                 "dogtags_s/dta035.png",
                 "dogtags_s/dta036.png",
                 "dogtags_s/dta037.png",
                 "dogtags_s/dta038.png",
                 "dogtags_s/dta039.png",
                 "dogtags_s/dta040.png",
                 "dogtags_s/dta041.png",
                 "dogtags_s/dta042.png",
                 "dogtags_s/dta043.png",
                 "dogtags_s/dta044.png",
                 "dogtags_s/dta045.png",
                 "dogtags_s/dta046.png",
                 "dogtags_s/dta047.png",
                 "dogtags_s/dta048.png",
                 "dogtags_s/dta049.png",
                 "dogtags_s/dta050.png",
                 "dogtags_s/dta051.png",
                 "dogtags_s/dta052.png",
                 "dogtags_s/dta053.png",
                 "dogtags_s/dta054.png",
                 "dogtags_s/dta055.png",
                 "dogtags_s/dta056.png",
                 "dogtags_s/dta057.png",
                 "dogtags_s/dta058.png",
                 "dogtags_s/dta059.png",
                 "dogtags_s/dta060.png",
                 "dogtags_s/dta061.png",
                 "dogtags_s/dta062.png",
                 "dogtags_s/dta063.png",
                 "dogtags_s/dta064.png",
                 "dogtags_s/dta065.png",
                 "dogtags_s/dta066.png",
                 "dogtags_s/dta067.png",
                 "dogtags_s/dta068.png",
                 "dogtags_s/dta069.png",
                 "dogtags_s/dta070.png",
                 "dogtags_s/dta071.png",
                 "dogtags_s/dta072.png",
                 "dogtags_s/dta073.png",
                 "dogtags_s/dta074.png",
                 "dogtags_s/dta075.png",
                 "dogtags_s/dta076.png",
                 "dogtags_s/dta077.png",
                 "dogtags_s/dta078.png",
                 "dogtags_s/dta079.png",
                 "dogtags_s/dta080.png",
                 "dogtags_s/dta081.png",
                 "dogtags_s/dta087.png",
                 "dogtags_s/dta088.png",
                 "dogtags_s/dta089.png",
                 "dogtags_s/dta090.png",
                 "dogtags_s/dta092-m16a4.png",
                 "dogtags_s/dtb001.png",
                 "dogtags_s/dtb002.png",
                 "dogtags_s/dtb003.png",
                 "dogtags_s/dtb004.png",
                 "dogtags_s/dtb005.png",
                 "dogtags_s/dtb006.png",
                 "dogtags_s/dtb007.png",
                 "dogtags_s/dtb008.png",
                 "dogtags_s/dtb009.png",
                 "dogtags_s/dtb010.png",
                 "dogtags_s/dtb011.png",
                 "dogtags_s/dtb012.png",
                 "dogtags_s/dtb013.png",
                 "dogtags_s/dtb014.png",
                 "dogtags_s/dtb015.png",
                 "dogtags_s/dtb016.png",
                 "dogtags_s/dtb017.png",
                 "dogtags_s/dtb018.png",
                 "dogtags_s/dtb019.png",
                 "dogtags_s/dtb020.png",
                 "dogtags_s/dtb021.png",
                 "dogtags_s/dtb022.png",
                 "dogtags_s/dtb023.png",
                 "dogtags_s/dtb024.png",
                 "dogtags_s/dtb025.png",
                 "dogtags_s/dtb026.png",
                 "dogtags_s/dtb027.png",
                 "dogtags_s/dtb028.png",
                 "dogtags_s/dtb029.png",
                 "dogtags_s/vdtb030.png",
                 "dogtags_s/dtb031.png",
                 "dogtags_s/dtb032.png",
                 "dogtags_s/dtb033.png",
                 "dogtags_s/dtb034.png",
                 "dogtags_s/dtb035.png",
                 "dogtags_s/dtb036.png",
                 "dogtags_s/dtb037.png",
                 "dogtags_s/dtb038.png",
                 "dogtags_s/dtb039.png",
                 "dogtags_s/dtb040.png",
                 "dogtags_s/dtb041.png",
                 "dogtags_s/dtb042.png",
                 "dogtags_s/dtb043.png",
                 "dogtags_s/dtb044.png",
                 "dogtags_s/dtb045.png",
                 "dogtags_s/dtb046.png",
                 "dogtags_s/dtb047.png",
                 "dogtags_s/dtb048.png",
                 "dogtags_s/dtb049.png",
                 "dogtags_s/dtb050.png",
                 "dogtags_s/dtb051.png",
                 "dogtags_s/dtb052.png",
                 "dogtags_s/dtb053.png",
                 "dogtags_s/dtb054.png",
                 "dogtags_s/dtb055.png",
                 "dogtags_s/dtb056.png",
                 "dogtags_s/dtb057.png",
                 "dogtags_s/dtb058.png",
                 "dogtags_s/dtb059.png",
                 "dogtags_s/dtb060.png",
                 "dogtags_s/dtb061.png",
                 "dogtags_s/dtb062.png",
                 "dogtags_s/dtb063.png",
                 "dogtags_s/dtb064.png",
                 "dogtags_s/dtb065.png",
                 "dogtags_s/dtb066.png",
                 "dogtags_s/dtb067.png",
                 "dogtags_s/dtb068.png",
                 "dogtags_s/dtb069.png",
                 "dogtags_s/dtb070.png",
                 "dogtags_s/dtb071.png",
                 "dogtags_s/dtb072.png",
                 "dogtags_s/dtb073.png",
                 "dogtags_s/dtb074.png",
                 "dogtags_s/dtb075.png",
                 "dogtags_s/dtb076.png",
                 "dogtags_s/dtb077.png",
                 "dogtags_s/dtb078.png",
                 "dogtags_s/dtb079.png",
                 "dogtags_s/dtb080.png",
                 "dogtags_s/dtb081.png",
                 "dogtags_s/dtb082.png",
                 "dogtags_s/dtb083.png",
                 "dogtags_s/dtb084.png",
                 "dogtags_s/dtb085.png",
                 "dogtags_s/dtb086.png",
                 "dogtags_s/dtb087.png",
                 "dogtags_s/dtb088.png",
                 "dogtags_s/dtb090.png",
                 "dogtags_s/dtb091.png",
                 "dogtags_s/dtb092.png",
                 "dogtags_s/dtb093.png",
                 "dogtags_s/dtb094.png",
                 "dogtags_s/dtb095.png",
                 "dogtags_s/dtb096.png",
                 "dogtags_s/dtb097.png",
                 "dogtags_s/dtb098.png",
                 "dogtags_s/dtb099.png",
                 "dogtags_s/dtb100.png",
                 "dogtags_s/dtb101.png",
                 "dogtags_s/dtb102.png",
                 "dogtags_s/dtb103.png",
                 "dogtags_s/dtb104.png",
                 "dogtags_s/dtb105.png",
                 "dogtags_s/dtb106.png",
                 "dogtags_s/dtb107.png",
                 "dogtags_s/dtb108.png",
                 "dogtags_s/dtb109.png",
                 "dogtags_s/dtb110.png",
                 "dogtags_s/dtb111.png",
                 "dogtags_s/dtb112.png",
                 "dogtags_s/dtb113.png",
                 "dogtags_s/dtb114.png",
                 "dogtags_s/dtb115.png",
                 "dogtags_s/dtb116.png",
                 "dogtags_s/dtb117.png",
                 "dogtags_s/dtb118.png",
                 "dogtags_s/dtb119.png",
                 "dogtags_s/dtb120.png",
                 "dogtags_s/dtb121.png",
                 "dogtags_s/dtb122.png",
                 "dogtags_s/dtb123.png",
                 "dogtags_s/dtb124.png",
                 "dogtags_s/dtb125.png",
                 "dogtags_s/dtb126.png",
                 "dogtags_s/dtb127.png",
                 "dogtags_s/dtb128.png",
                 "dogtags_s/dtb129.png",
                 "dogtags_s/dtb130.png",
                 "dogtags_s/dtb131.png",
                 "dogtags_s/dtb132.png",
                 "dogtags_s/dtb133.png",
                 "dogtags_s/dtb134.png",
                 "dogtags_s/dtb135.png",
                 "dogtags_s/dtb136.png",
                 "dogtags_s/dtb137.png",
                 "dogtags_s/dtb138.png",
                 "dogtags_s/dtb139.png",
                 "dogtags_s/dtb140.png",
                 "dogtags_s/dtb141.png",
                 "dogtags_s/dtb142.png",
                 "dogtags_s/dtb143.png",
                 "dogtags_s/dtb144.png",
                 "dogtags_s/dtb145.png",
                 "dogtags_s/dtb146.png",
                 "dogtags_s/dtb147.png",
                 "dogtags_s/dtb148.png",
                 "dogtags_s/dtb149.png",
                 "dogtags_s/dtb150.png",
                 "dogtags_s/dtb151.png",
                 "dogtags_s/dtb152.png",
                 "dogtags_s/dtb153.png",
                 "dogtags_s/dtb154.png",
                 "dogtags_s/dtb155.png",
                 "dogtags_s/dtb156.png",
                 "dogtags_s/f2000.png",
                 "dogtags_s/fgm-148-javelin.png",
                 "dogtags_s/fim-92-stinger.png",
                 "dogtags_s/g36c.png",
                 "dogtags_s/g3.png",
                 "dogtags_s/glock-17.png",
                 "dogtags_s/glock-18.png",
                 "dogtags_s/grach.png",
                 "dogtags_s/grach-silenced.png",
                 "dogtags_s/ifv.png",
                 "dogtags_s/ifvservicestar100.png",
                 "dogtags_s/ifvservicestar10.png",
                 "dogtags_s/ifvservicestar25.png",
                 "dogtags_s/ivfvservicestar50.png",
                 "dogtags_s/ifvservicestar5.png",
                 "dogtags_s/jet.png",
                 "dogtags_s/jetservicestar100.png",
                 "dogtags_s/jetservicestar10.png",
                 "dogtags_s/jetservicestar25.png",
                 "dogtags_s/jetservicestar50.png",
                 "dogtags_s/jetservicestar5.png",
                 "dogtags_s/kh2000.png",
                 "dogtags_s/laser-range-fighter.png",
                 "dogtags_s/m1014.png",
                 "dogtags_s/m240.png",
                 "dogtags_s/m249.png",
                 "dogtags_s/m26_left.png",
                 "dogtags_s/m26_right.png",
                 "dogtags_s/m27-lmg.png",
                 "dogtags_s/m39-mbr.png",
                 "dogtags_s/m40.png",
                 "dogtags_s/m416.png",
                 "dogtags_s/m45.png",
                 "dogtags_s/m4a1.png",
                 "dogtags_s/m60e4.png",
                 "dogtags_s/m93r.png",
                 "dogtags_s/m9.png",
                 "dogtags_s/m9-silenced.png",
                 "dogtags_s/magpul.png",
                 "dogtags_s/mbt.png",
                 "dogtags_s/me3_01.png",
                 "dogtags_s/me3_02.png",
                 "dogtags_s/mk11.png",
                 "dogtags_s/mk153-smaw.png",
                 "dogtags_s/model-98b.png",
                 "dogtags_s/mortar.png",
                 "dogtags_s/mp7.png",
                 "dogtags_s/muav.png",
                 "dogtags_s/nfs.png",
                 "dogtags_s/p90-tr.png",
                 "dogtags_s/pecheneg.png",
                 "dogtags_s/pepper1.png",
                 "dogtags_s/pepper2.png",
                 "dogtags_s/pepper3.png",
                 "dogtags_s/pepper4.png",
                 "dogtags_s/pepper5.png",
                 "dogtags_s/pp2000.png",
                 "dogtags_s/rex.png",
                 "dogtags_s/rpg-7.png",
                 "dogtags_s/rpk-74m.png",
                 "dogtags_s/sa18-igla-aa.png",
                 "dogtags_s/saiga-20k.png",
                 "dogtags_s/scar-h.png",
                 "dogtags_s/scout-heli.png",
                 "dogtags_s/scoutheliservicestar100.png",
                 "dogtags_s/scoutheliservicestar10.png",
                 "dogtags_s/scoutheliservicestar25.png",
                 "dogtags_s/scoutheliservicestar50.png",
                 "dogtags_s/scoutheliservicestar5.png",
                 "dogtags_s/sg553.png",
                 "dogtags_s/sks.png",
                 "dogtags_s/spice2.png",
                 "dogtags_s/spice3.png",
                 "dogtags_s/spice4.png",
                 "dogtags_s/spice5.png",
                 "dogtags_s/spice.png",
                 "dogtags_s/sv98.png",
                 "dogtags_s/svd-dragunov.png",
                 "dogtags_s/syndicate_256.png",
                 "dogtags_s/tankservicestar100.png",
                 "dogtags_s/tankservicestar10.png",
                 "dogtags_s/tankservicestar25.png",
                 "dogtags_s/tankservicestar50.png",
                 "dogtags_s/tankservicestar5.png",
                 "dogtags_s/taurus.png",
                 "dogtags_s/tournament1.png",
                 "dogtags_s/tournament2.png",
                 "dogtags_s/tournament3.png",
                 "dogtags_s/typ-88-lmg.png",
                 "dogtags_s/ump-45.png",
                 "dogtags_s/usa-s12.png",
                 "dogtags_s/xp1_dta001.png",
                 "dogtags_s/xp1_dta002.png",
                 "dogtags_s/xp1_dta003.png",
                 "dogtags_s/xp1_dta004.png",
                 "dogtags_s/xp1_dta005.png",
                 "dogtags_s/xp1_dta006.png",
                 "dogtags_s/xp1_dta007.png",
                 "dogtags_s/xp1_dta008.png",
                 "dogtags_s/xp1_dta009.png",
                 "dogtags_s/xp1_dta010.png",
                 "dogtags_s/xp1_dtb001.png",
                 "dogtags_s/xp1_dtb002.png",
                 "dogtags_s/xp1_dtb003.png",
                 "dogtags_s/xp1_dtb004.png",
                 "dogtags_s/xp1_dtb005.png",
                 "dogtags_s/xp1_dtb006.png",
                 "dogtags_s/xp1_dtb007.png",
                 "dogtags_s/xp1_dtb008.png",
                 "dogtags_s/xp1_dtb009.png",
                 "dogtags_s/xp1_dtb010.png",
                 "dogtags_s/xp1_dtb011.png",
                 "dogtags_s/xp1_dtb012.png",
                 "dogtags_s/xp1_dtb013.png",
                 "dogtags_s/xp1_dtb014.png",
                 "dogtags_s/xp1_dtb015.png",
                 "dogtags_s/xp2_dangerclose.png",
                 "dogtags_s/xp2_destroyer.png",
                 "dogtags_s/xp2_dta001.png",
                 "dogtags_s/xp2_dta002.png",
                 "dogtags_s/xp2_dta003.png",
                 "dogtags_s/xp2_dta004.png",
                 "dogtags_s/xp2_dta005.png",
                 "dogtags_s/xp2_dta006.png",
                 "dogtags_s/xp2_dta007.png",
                 "dogtags_s/xp2_dta008.png",
                 "dogtags_s/xp2_dta009.png",
                 "dogtags_s/xp2_dta010.png",
                 "dogtags_s/xp2_dtb001.png",
                 "dogtags_s/xp2_dtb002.png",
                 "dogtags_s/xp2_dtb003.png",
                 "dogtags_s/xp2_dtb004.png",
                 "dogtags_s/xp2_dtb005.png",
                 "dogtags_s/xp2_dtb006.png",
                 "dogtags_s/xp2_dtb007.png",
                 "dogtags_s/xp2_dtb008.png",
                 "dogtags_s/xp2_dtb009.png",
                 "dogtags_s/xp2_dtb010.png",
                 "dogtags_s/xp2_moh.png",
                 "dogtags_s/xp2_prem1_dtb001_assault.png",
                 "dogtags_s/xp2_prem1_dtb002_support.png",
                 "dogtags_s/xp2_prem1_dtb003_engineer.png",
                 "dogtags_s/xp2_prem1_dtb004_recon.png",
                 "dogtags_s/xp2_prem1_dtb005_common.png",
                 "dogtags_s/xp2_prem1_dtb006_welcome.png",
                 "dogtags_s/xp2_toe2toe.png",
                 "dogtags_s/xp2_visceral.png",
                 //weapon attachments
                  "weaponaccessory/rx01.png",
                      "weaponaccessory/targetpointer.png",
                      "weaponaccessory/soundsuppressor.png",
                      "weaponaccessory/eotech.png",
                      "weaponaccessory/flashlight.png",
                      "weaponaccessory/ext_mag01.png",
                      "weaponaccessory/m145.png",
                      "weaponaccessory/acog.png",
                      "weaponaccessory/kobra.png",
                      "weaponaccessory/irnv.png",
                      "weaponaccessory/pkas.png",
                      "weaponaccessory/pka.png",
                      "weaponaccessory/ballistic.png",
                      "weaponaccessory/straightbolt.png",
                      "weaponaccessory/heavybarrel.png",
                      "weaponaccessory/riflescope.png",
                      "weaponaccessory/pks07.png",
                      "weaponaccessory/pso.png",
                      "weaponaccessory/bipod.png",
                      "weaponaccessory/flechette.png",
                      "weaponaccessory/frag.png",
                      "weaponaccessory/slug.png",
                      "weaponaccessory/40mm_shotgunshell.png",
                      "weaponaccessory/40mm_smoke.png"
                  //Jet fighter unlocks
                      ,"vehicleunlocks/jetflares.png"
                          ,"vehicleunlocks/jetsidewinder.png"
                          ,"vehicleunlocks/jetstealth.png"
                          ,"vehicleunlocks/jetweaponefficiency.png"
                          ,"vehicleunlocks/jetproximity.png"
                          ,"vehicleunlocks/rocketpod.png"
                          ,"vehicleunlocks/jetradar.png"
                          ,"vehicleunlocks/jetbelowradar.png"
                          ,"vehicleunlocks/jetextinguisher.png"
                          ,"vehicleunlocks/jetpreventive.png"
                          ,"vehicleunlocks/jetmaverik.png"
                          ,"vehicleunlocks/jetavionics.png"
                          ,"vehicleunlocks/jetecm.png"
                  //AntyAir unlocks
                          ,"vehicleunlocks/aasmoke.png"
                              ,"vehicleunlocks/aastinger.png"
                              ,"vehicleunlocks/aaweaponefficiency.png"
                              ,"vehicleunlocks/aazoom.png"
                              ,"vehicleunlocks/aaproximity.png"
                              ,"vehicleunlocks/aaenvg.png"
                              ,"vehicleunlocks/aaradar.png"
                              ,"vehicleunlocks/aapreventive.png"
                              ,"vehicleunlocks/aastealth.png"
                              ,"vehicleunlocks/aaarmor.png"
                  //tank Ulocks
                              ,"vehicleunlocks/mbtsmoke.png"
                                  ,"vehicleunlocks/mbtcoax.png"
                                  ,"vehicleunlocks/mbtweaponeff.png"
                                  ,"vehicleunlocks/mbtzoom.png"
                                  ,"vehicleunlocks/mbtpreventive.png"
                                  ,"vehicleunlocks/mbthmg.png"
                                  ,"vehicleunlocks/mbtproximity.png"
                                  ,"vehicleunlocks/mbtatgm.png"
                                  ,"vehicleunlocks/mbtenvg.png"
                                  ,"vehicleunlocks/mbtstealth.png"
                                  ,"vehicleunlocks/mbtcanister.png"
                                  ,"vehicleunlocks/mbtarmor.png"
                                  ,"vehicleunlocks/mbtcitv.png"
                 //attack hely unlocks
                                  ,"vehicleunlocks/attackheliflares.png"
                                      ,"vehicleunlocks/attackhelisidewinders.png"
                                      ,"vehicleunlocks/attackhelistealth.png"
                                      ,"vehicleunlocks/attackheliweaponefficiency.png"
                                      ,"vehicleunlocks/attackhelizoom.png"
                                      ,"vehicleunlocks/attackheliproximity.png"
                                      ,"vehicleunlocks/attackheliradar.png"
                                      ,"vehicleunlocks/attackhelihellfire.png"
                                      ,"vehicleunlocks/attackheliextinguisher.png"
                                      ,"vehicleunlocks/attackhelipreventive.png"
                                      ,"vehicleunlocks/attackhelienvg.png"
                                      ,"vehicleunlocks/attackhelilaser.png"
                                      ,"vehicleunlocks/attackhelibelowradar.png"
                                      ,"vehicleunlocks/attackheliecm.png"
                                      ,"vehicleunlocks/attackhelirocketguide.png"
                                      ,"vehicleunlocks/attackhelitvg.png"
                  //IFF unlocks
                                      ,"vehicleunlocks/ifvsmoke.png"
                                          ,"vehicleunlocks/ifvtow.png"
                                          ,"vehicleunlocks/ifvweaponefficiency.png"
                                          ,"vehicleunlocks/ifvcoax.png"
                                          ,"vehicleunlocks/ifvenvg.png"
                                          ,"vehicleunlocks/ifvproximity.png"
                                          ,"vehicleunlocks/ifvzoom.png"
                                          ,"vehicleunlocks/ifvpreventive.png"
                                          ,"vehicleunlocks/ifvapfsds-t.png"
                                          ,"vehicleunlocks/ifvstealth.png"
                                          ,"vehicleunlocks/ifvhellfire.png"
                                          ,"vehicleunlocks/ifvarmor.png"
                  //Jet attack
                                          ,"vehicleunlocks/jetflares.png"
                                              ,"vehicleunlocks/jetsidewinder.png"
                                              ,"vehicleunlocks/jetstealth.png"
                                              ,"vehicleunlocks/jetweaponefficiency.png"
                                              ,"vehicleunlocks/jetproximity.png"
                                              ,"vehicleunlocks/rocketpod.png"
                                              ,"vehicleunlocks/jetradar.png"
                                              ,"vehicleunlocks/jetbelowradar.png"
                                              ,"vehicleunlocks/jetextinguisher.png"
                                              ,"vehicleunlocks/jetpreventive.png"
                                              ,"vehicleunlocks/jetmaverik.png"
                                              ,"vehicleunlocks/jetavionics.png"
                                              ,"vehicleunlocks/jetecm.png"
                     //Scout choper
                                              ,"vehicleunlocks/scoutflares.png"
                                                  ,"vehicleunlocks/scoutsidewinder.png"
                                                  ,"vehicleunlocks/scoutstealth.png"
                                                  ,"vehicleunlocks/scoutweaponefficiency.png"
                                                  ,"vehicleunlocks/scoutproximity.png"
                                                  ,"vehicleunlocks/scoutradar.png"
                                                  ,"vehicleunlocks/scoutextinguisher.png"
                                                  ,"vehicleunlocks/scoutpreventive.png"
                                                  ,"vehicleunlocks/scouthellfire.png"
                                                  ,"vehicleunlocks/scoutbelowradar.png"
                                                  ,"vehicleunlocks/scoutlaser.png"
                                                  ,"vehicleunlocks/scoutecm.png"
       //Vehicles
                                                  ,"vehicles/venom.png"
                                                      ,"vehicles/aav.png"
                                                      ,"vehicles/tow.png"
                                                      ,"vehicles/f35.png"
                                                      ,"vehicles/t90.png"
                                                      ,"vehicles/skidloader.png"
                                                      ,"vehicles/lav-ad.png"
                                                      ,"vehicles/mi28.png"
                                                      ,"vehicles/vodnik.png"
                                                      ,"vehicles/dpv.png"
                                                      ,"vehicles/tunguska.png"
                                                      ,"vehicles/ah1z.png"
                                                      ,"vehicles/bmp2.png"
                                                      ,"vehicles/lav-25.png"
                                                      ,"vehicles/m1a2.png"
                                                      ,"vehicles/rhib.png"
                                                      ,"vehicles/a10.png"
                                                      ,"vehicles/ka60.png"
                                                      ,"vehicles/pantsir.png"
                                                      ,"vehicles/f18.png"
                                                      ,"vehicles/su39.png"
                                                      ,"vehicles/humvee.png"
                                                      ,"vehicles/centurion.png"
                                                      ,"vehicles/vdv.png"
                                                      ,"vehicles/z11.png"
                                                      ,"vehicles/kornet.png"
                                                      ,"vehicles/growler.png"
                                                      ,"vehicles/su35.png"
                                                      ,"vehicles/btr90.png"
                                                      ,"vehicles/ah6.png"       
                                                      //kit items
                                                      ,"kititems/radiobeacon.png"
                                                          ,"kititems/m67-grenade.png"
                                                          ,"kititems/repairtool.png"
                                                          ,"kititems/c4.png"
                                                          ,"kititems/defib.png"
                                                          ,"kititems/tugs.png"
                                                          ,"kititems/mine.png"
                                                          ,"kititems/claymore.png"
                                                          ,"kititems/eod.png"
                                                          ,"kititems/soflam.png"
                                                          ,"kititems/m224_mortar.png"
                                                          ,"kititems/mav.png"
             
                 };
		return menuList;
	}

	private String GetUrl(String platform){
		sendURL = url.concat(platform);
        sendURL = sendURL.concat("/");
        sendURL = sendURL.concat("player");
        sendURL = sendURL.concat("/");
        return sendURL;
	}
	
	private String GetUrlForUpdate(String platform){
		sendURL = url.concat(platform);
        sendURL = sendURL.concat("/");
        sendURL = sendURL.concat("playerupdate");
        sendURL = sendURL.concat("/");
        return sendURL;
	}
	
	public static int byteToUnsignedInt(byte b) {
	    return 0x00 << 24 | b & 0xff;
	  }
	
	public String hash_hmac(String type, String value, String key)
    {
    try {
        javax.crypto.Mac mac = javax.crypto.Mac.getInstance(type);
        javax.crypto.spec.SecretKeySpec secret = new javax.crypto.spec.SecretKeySpec(key.getBytes(), type);
        mac.init(secret);
        byte[] digest = mac.doFinal(value.getBytes());
        StringBuilder sb = new StringBuilder(digest.length*2);
        String s;
        for (byte b : digest){
        	s = Integer.toHexString(byteToUnsignedInt(b));
        	if(s.length() == 1) sb.append('0');
        	sb.append(s);
        }
        return sb.toString();
    } catch (Exception e) {
    }
        return "";
    }
	
protected StringBuffer UpdatePlayer(String playerName, String platform, String appIdent, String timestamp) {
        
        //Build parameter string
        //String data = "time="+timestamp+"&ident="+appIdent+"&player="+playerName;
    	String data;
    	String sig;
    	
        try {
            
            // Send the request
            URL url = new URL("http://api.bf3stats.com/"+platform+"/playerupdate/");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            
            //JSON and Base64 encoding for "data" string
            JSONObject json = new JSONObject();
            try {
            	json.put("time", timestamp);
            	json.put("ident", appIdent);
            	json.put("player", playerName);
            } catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            data = json.toString();
            String encoded64data = str_replace(Base64.encodeToString(data.getBytes(),10));
            
            
            //SHA256-HMAC and Base64 encoding for "sig" string
            
            String encoded64sig = null;
            try {
	            SecretKey signingKey = new SecretKeySpec(localKey.getBytes(), "HMACSHA256");  
	            Mac mac = Mac.getInstance("HMACSHA256");
	            mac.init(signingKey);  
	            byte[] digest = mac.doFinal(encoded64data.getBytes());     //output of sha256  
	            encoded64sig = str_replace(Base64.encodeToString(digest,10));   // using it as an input  
            	} 
            catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
            catch (InvalidKeyException e) {
				e.printStackTrace();
			}  
            
            //write parameters
            writer.write("data="+encoded64data+"&sig="+encoded64sig);
            writer.flush();
            
            // Get the response
            StringBuffer answer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                answer.append(line);
            }
            writer.close();
            reader.close();
            
            //Return the response
            return answer;
            
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (NullPointerException e) {
        	StringBuffer x = new StringBuffer();
        	x.append("ERROR");
        	return x;
		}
        
        return null;
    }
	
protected StringBuffer AddPlayerToBfstats(String playerName, String platform, String appIdent, String timestamp) {
    
    //Build parameter string
    //String data = "time="+timestamp+"&ident="+appIdent+"&player="+playerName;
	String data;
	String sig;
	
    try {
        
        // Send the request
        URL url = new URL("http://api.bf3stats.com/"+platform+"/playerlookup/");
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        
        //JSON and Base64 encoding for "data" string
        JSONObject json = new JSONObject();
        try {
        	json.put("time", timestamp);
        	json.put("ident", appIdent);
        	json.put("player", playerName);
        } catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        data = json.toString();
        String encoded64data = str_replace(Base64.encodeToString(data.getBytes(),10));
        
        
        //SHA256-HMAC and Base64 encoding for "sig" string
        //String sicretKey = "MTI85XVvZNO8W5Itw8hTzFHW29v2RrVI";
        String encoded64sig = null;
        try {
            SecretKey signingKey = new SecretKeySpec(localKey.getBytes(), "HMACSHA256");  
            Mac mac = Mac.getInstance("HMACSHA256");
            mac.init(signingKey);  
            byte[] digest = mac.doFinal(encoded64data.getBytes());     //output of sha256  
            encoded64sig = str_replace(Base64.encodeToString(digest,10));   // using it as an input  
        	} 
        catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}  
        catch (InvalidKeyException e) {
			e.printStackTrace();
		}  
        
        //write parameters
        writer.write("data="+encoded64data+"&sig="+encoded64sig);
        writer.flush();
        
        // Get the response
        StringBuffer answer = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            answer.append(line);
        }
        writer.close();
        reader.close();
        
        //Return the response
        return answer;
        
    } catch (MalformedURLException ex) {
        ex.printStackTrace();
    } catch (IOException ex) {
        ex.printStackTrace();
    }
    
    return null;
}
    
    public static String str_replace (String text) {
		text = text.replace('+', '-');
		text = text.replace('/', '_');
		text = text.replace("=", "");
    	return text;    	
    }
    
	//Connecting to player stats
	public boolean ConnectingToStats(int player){
		HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
        HttpResponse response = null;
        if(player==1){
        	post = new HttpPost(GetUrl(getPlayerPlatform()));
        }else{
        	post = new HttpPost(GetUrl(getPlayer2platform()));
        }
    	List<NameValuePair> params = new ArrayList<NameValuePair>();
    	if(player==1){
    		params.add(new BasicNameValuePair("player", getPlayerName()));
    	}else if(player==2){
    		params.add(new BasicNameValuePair("player", getPlayer2name()));
    	}
    	params.add(new BasicNameValuePair("opt", "assignments"));
    	UrlEncodedFormEntity ent = null;
		try {
			ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
    	post.setEntity(ent);
        try {
			response = client.execute(post);
			statusLine = response.getStatusLine();
	        entity = response.getEntity();
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			content = entity.getContent();
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        try{
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200 || statusCode == 400 || statusCode == 500) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    content, "iso-8859-1"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line + "n");
	            }
	            content.close();
	            if(sb.toString().equals(null)){
	            	connectionprobelem = true;
	            }else{
	            	if(player==1){
	            		setJsonString(sb.toString());
	            	}else if (player==2){
	            		setJsonString2(sb.toString());
	            	}
	            }
			} else {
				connectionprobelem = true;
			}
        }
        catch(Exception e){
            e.printStackTrace();
        }
        weAreGood = true;
		return connectionprobelem;
	}
	
	//Getting player for first time
	public boolean initializePlayer(){
		return connectionprobelem;
		
	}
	
	private void CheckAppKey() {
    	db = new Database(this);
		db.open();
		c = db.getKey();
		if(c!=null){
			if (c.moveToFirst()) {
				keyIsPressent  = true;
				localIdent = c.getString(c.getColumnIndex("key"));
				localKey = c.getString(c.getColumnIndex("hesh"));
			}
		}
		c.close();
		db.close();
		if(!keyIsPressent){
			something = setupAppKey();
	        if(something==null){
	    	}else{
	    		jsonKeyString = something.toString();
	    		try {
					jsonKey = new JSONObject(jsonKeyString);
					status  = jsonKey.getString("status");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		if(status.equals("ok")){
	    			db.open();
	    			try {
						db.insertKey(jsonKey.getString("ident"), jsonKey.getString("key"));
						localIdent = jsonKey.getString("ident");
						localKey = jsonKey.getString("key");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    			db.close();
	    		}else{
	    			something = getAppKey();
	    			if(something==null){
	    			}else{
	    	    		jsonKeyString = something.toString();
	    	    		try {
	    					jsonKey = new JSONObject(jsonKeyString);
	    					status  = jsonKey.getString("status");
	    				} catch (JSONException e) {
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}
	    	    		if(status.equals("ok")){
	    	    			db.open();
	    	    			try {
	    						db.insertKey(jsonKey.getString("ident"), jsonKey.getString("key"));
	    						localIdent = jsonKey.getString("ident");
	    						localKey = jsonKey.getString("key");
	    					} catch (JSONException e) {
	    						// TODO Auto-generated catch block
	    						e.printStackTrace();
	    					}
	    	    			db.close();
	    	    		}else{
	    	    			localIdent = appidentification;
	    	    			localKey = sicretKey;
	    	    		}
	    			}
	    		}
	    	}
		}
	}

	private StringBuffer getAppKey(){
		String data;
		String sig;
		
	    try {
	        String key = generateKey();
	        // Send the request
	        URL url = new URL("http://api.bf3stats.com/global/getkey/");
	        URLConnection conn = url.openConnection();
	        conn.setDoOutput(true);
	        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
	        
	        //JSON and Base64 encoding for "data" string
	        JSONObject json = new JSONObject();
	        try {
	        	json.put("time", ""+now());
	        	json.put("ident", appidentification);
	        	json.put("clientident", key);
	        } catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        data = json.toString();
	        String encoded64data = str_replace(Base64.encodeToString(data.getBytes(),10));
	        
	        
	        //SHA256-HMAC and Base64 encoding for "sig" string
	        String encoded64sig = null;
	        try {
	            SecretKey signingKey = new SecretKeySpec(sicretKey.getBytes(), "HMACSHA256");  
	            Mac mac = Mac.getInstance("HMACSHA256");
	            mac.init(signingKey);  
	            byte[] digest = mac.doFinal(encoded64data.getBytes());     //output of sha256  
	            encoded64sig = str_replace(Base64.encodeToString(digest,10));   // using it as an input  
	        	} 
	        catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
	        catch (InvalidKeyException e) {
				e.printStackTrace();
			}  
	        
	        //write parameters
	        writer.write("data="+encoded64data+"&sig="+encoded64sig);
	        writer.flush();
	        
	        // Get the response
	        StringBuffer answer = new StringBuffer();
	        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String line;
	        while ((line = reader.readLine()) != null) {
	            answer.append(line);
	        }
	        writer.close();
	        reader.close();
	        
	        //Return the response
	        return answer;
	        
	    } catch (MalformedURLException ex) {
	        ex.printStackTrace();
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }
	    
	    return null;
	}
	
	private StringBuffer setupAppKey() {
		String data;
		String sig;
		
	    try {
	        String key = generateKey();
	        // Send the request
	        URL url = new URL("http://api.bf3stats.com/global/setupkey/");
	        URLConnection conn = url.openConnection();
	        conn.setDoOutput(true);
	        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
	        
	        //JSON and Base64 encoding for "data" string
	        JSONObject json = new JSONObject();
	        try {
	        	json.put("time", ""+now());
	        	json.put("ident", appidentification);
	        	json.put("clientident", key);
	        	json.put("name", "New Client");
	        } catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        data = json.toString();
	        String encoded64data = str_replace(Base64.encodeToString(data.getBytes(),10));
	        
	        
	        //SHA256-HMAC and Base64 encoding for "sig" string
	        String encoded64sig = null;
	        try {
	            SecretKey signingKey = new SecretKeySpec(sicretKey.getBytes(), "HMACSHA256");  
	            Mac mac = Mac.getInstance("HMACSHA256");
	            mac.init(signingKey);  
	            byte[] digest = mac.doFinal(encoded64data.getBytes());     //output of sha256  
	            encoded64sig = str_replace(Base64.encodeToString(digest,10));   // using it as an input  
	        	} 
	        catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}  
	        catch (InvalidKeyException e) {
				e.printStackTrace();
			}  
	        
	        //write parameters
	        writer.write("data="+encoded64data+"&sig="+encoded64sig);
	        writer.flush();
	        
	        // Get the response
	        StringBuffer answer = new StringBuffer();
	        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String line;
	        while ((line = reader.readLine()) != null) {
	            answer.append(line);
	        }
	        writer.close();
	        reader.close();
	        
	        //Return the response
	        return answer;
	        
	    } catch (MalformedURLException ex) {
	        ex.printStackTrace();
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }
	    
	    return null;
		
	}
	
	//Generate unique key (10 chars) for device
	private String generateKey() {
		final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

	    final String tmDevice, tmSerial, androidId;
	    tmDevice = "" + tm.getDeviceId();
	    tmSerial = "" + tm.getSimSerialNumber();
	    androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

	    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
	    String deviceId = deviceUuid.toString();
	    return deviceId.substring(deviceId.length()-10);
		
	}
	
	//Getting all data which we can for our player
	public boolean getData(){
		Log.v(tag, "Getting data");
		try {
			jsonObject = new JSONObject(jsonString);
			dogtags = jsonObject.getJSONObject(TAG_DOGTAGS);
			rankAndScore = jsonObject.getJSONObject(TAG_STATS);
			setPlayerCountryName(jsonObject.getString(TAG_COUNTRY));
			getDogtags(dogtags);
			getRankAndScore(rankAndScore);
			getScore(rankAndScore);
			getGlobals(rankAndScore);
			getWeaponsJSON(rankAndScore);
			getVehiclesJSON(rankAndScore);
			getVehicleUnlocks(rankAndScore);
			getMedals(rankAndScore);
			getRibons(rankAndScore);
			getKits(rankAndScore);
			getEquipement(rankAndScore);
			getVehiclekategories(rankAndScore);
			getAssighmentsForPlayer(rankAndScore);
			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (NullPointerException e) {
			return false;
		}
	}
	public boolean getData2(){
		try {
			jsonObject2 = new JSONObject(jsonString2);
			rankAndScore2 = jsonObject2.getJSONObject(TAG_STATS);
			setRankAndScore2(rankAndScore2);
			setRank2(rankAndScore2);
			setGlobals2(rankAndScore2);
			setWeapons2(rankAndScore2);
			setVehicles2(rankAndScore2);
			setVehicleUnlocks2(rankAndScore2);
			setMedals2(rankAndScore2);
			setRibons2(rankAndScore2);
			setKit2(rankAndScore2);
			setEquipement2(rankAndScore2);
			setVehiclekategories2(rankAndScore2);
			setAssighments2(rankAndScore2);
			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (NullPointerException e) {
			return false;
		}
	}
	
	private void getAssighmentsForPlayer(JSONObject rankAndScore2) {
		try {
			setAssighments(rankAndScore2.getJSONObject("assignments"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void getVehiclekategories(JSONObject rankAndScore2) {
		try {
			setVehiclekategories(rankAndScore2.getJSONObject("vehcats"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void getEquipement(JSONObject rankAndScore2) {
		try {
			setEquipement(rankAndScore2.getJSONObject("equipment"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void setEquipement(JSONObject jsonObject2) {
		this.equipement = jsonObject2;
		
	}

	private void getKits(JSONObject rankAndScore2) {
		try {
			setKits(rankAndScore2.getJSONObject("kits"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void setKits(JSONObject jsonObject2) {
		this.setKit(jsonObject2);
		
	}

	private void getRibons(JSONObject rankAndScore2) {
		try {
			setRibons(rankAndScore2.getJSONObject("ribbons"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void getMedals(JSONObject rankAndScore2) {
		try {
			setMedals(rankAndScore2.getJSONObject("medals"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void getVehicleUnlocks(JSONObject rankAndScore2) {
		try {
			setVehicleUnlocks(rankAndScore2.getJSONObject("vehcats"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public JSONObject getingVehicleUnlocks(){
		return vehicleUnlocks;
	}
	private void setVehicleUnlocks(JSONObject jsonObject2) {
		this.vehicleUnlocks = jsonObject2;
		
	}

	private void getVehiclesJSON(JSONObject rankAndScore2) {
		try {
			setVehicles(rankAndScore2.getJSONObject(TAG_VEHICLES));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void getWeaponsJSON(JSONObject rankAndScore2) {
		try {
			setWeapons(rankAndScore2.getJSONObject(TAG_WEAPONS));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void getGlobals(JSONObject globals2) {
		try {
			globals = globals2.getJSONObject(TAG_GLOBALS);
			setPlayerGlobalKills(globals.getLong(TAG_KILLS));
			setPlayerGlobalDeaths(globals.getLong(TAG_DEATHS));
			setPlayerGameLose(globals.getLong(TAG_LOST));
			setPlayerGameWin(globals.getLong(TAG_WINS));
			setPlayerShootsFired(globals.getLong(TAG_SHOOTS_FIRED));
			setPlayerHits(globals.getLong(TAG_HITS));
			setPlayerHeadShoots(globals.getLong(TAG_HEADSHOOTS));
			setPlayerLongestHeadshoot(globals.getLong(TAG_LONGEST_HEADSOOT));
			setPlayerGameTime(globals.getLong(TAG_TIME));
			setPlayerVehicleTime(globals.getLong(TAG_TIME_VEHICLE));
			setPlayerVehicleKills(globals.getLong(TAG_VEHICLE_KILLS));
			setPlayerRevives(globals.getLong(TAG_REVIVE));
			setPlayerKillAssist(globals.getLong(TAG_KILL_ASSIST));
			setPlayerResuply(globals.getLong(TAG_RESUPLY));
			setPlayerHeals(globals.getLong(TAG_HEALS));
			setPlayerRepaires(globals.getLong(TAG_REPAIRES));
			setPlayerRoundsPlayed(globals.getLong(TAG_ROUNDS_PLAYES));
			setPlayerRoundsFinished(globals.getLong(TAG_FINISHED_ROUNDS));
			setPlayerKillStreek(globals.getLong(TAG_KILLSTREEK));
			setPlayerVehicleDestroyedAssistance(globals.getLong(TAG_VEHICLE_DESTROY_ASSIST));
			setPlayerVehicleDestroyed(globals.getLong(TAG_VEHICLE_DESTROY));
			setPlayerDogtagsTaken(globals.getLong(TAG_DOGTAGS_TAKEN));
			setPlayerAvangerKills(globals.getLong(TAG_AVANGER_KILLS));
			setPlayerSavioureKills(globals.getLong(TAG_SAVIOR_KILLS));
			setPlayerDamageAssist(globals.getLong(TAG_DAMAGE_ASSIST));
			setPlayerSupression(globals.getLong(TAG_SUPPRESION));
			setPlayerNemessiskills(globals.getLong(TAG_NEMESSIS_KILLS));
			setPlayerNemmesisStreak(globals.getLong(TAG_NEMMESIS_STREAK));
			setPlayerMCOMDestroyed(globals.getLong(TAG_MCOM_DESTROYED));
			setPlayerMCOMDefendKills(globals.getLong(TAG_MCOM_DEFENDING_KILLS));
			setPlayerFlagCaptured(globals.getLong(TAG_FLAG_CAP));
			setPlayerFlagDefendedKills(globals.getLong(TAG_FLAG_DEFENDED_KILS));
			setPlayerSkill(globals.getLong(TAG_SKILL));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	private void getScore(JSONObject statPoints2) {
		try {
			statPoints = statPoints2.getJSONObject(TAG_SCORES);
			setPlayerScore(statPoints.getLong(TAG_SCORE));
			setPlayerAwardPoints(statPoints.getLong(TAG_AWARD_SCORE));
			setPlayerAssaultScore(statPoints.getLong(TAG_ASSAULT));
			setPlayerBonusPoints(statPoints.getLong(TAG_BONUS));
			setPlayerEnginerScore(statPoints.getLong(TAG_ENGINER));
			setPlayerGeneralScore(statPoints.getLong(TAG_GENERAL));
			setPlayerObjectiveScore(statPoints.getLong(TAG_OBJECTIVE));
			setPlayerReaconScore(statPoints.getLong(TAG_REACON));
			setPlayerSquadScore(statPoints.getLong(TAG_SQUAD));
			setPlayerSuportScore(statPoints.getLong(TAG_SUPORT));
			setPlayerTeamScore(statPoints.getLong(TAG_TEAM_SCORE));
			setPlayerUnlocksScore(statPoints.getLong(TAG_UNLOCKS));
			setPlayerAAVehicleScore(statPoints.getLong(TAG_AAVEHICLE));
			setPlayerVehicleScore(statPoints.getLong(TAG_VEHICLEALL));
			setPlayerIFVScore(statPoints.getLong(TAG_IFV));
			setPlayerJetScore(statPoints.getLong(TAG_JET));
			setPlayerMBTScore(statPoints.getLong(TAG_MBT));
			setPlayerScoutHellyScore(statPoints.getLong(TAG_SH));
			setPlayerPreviouseRankScore(statPoints2.getJSONObject("rank").getLong("score"));
			nextRank = statPoints2.getJSONArray("nextranks");
			row = nextRank.getJSONObject(1);
			setPlayerNextRankScore(row.getLong("score"));
			setPlayerScoreLeftToNextRank(row.getLong("left"));
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void getRankAndScore(JSONObject stats2) {
		try {
			rank = stats2.getJSONObject(TAG_RANK);
			setPlayerRank(rank.getString(TAG_LEVEL));
			setPlayerRankImmage(rank.getString(TAG_MEDIUM_IMAGE));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		
	}

	/**
	 * Getting all dogtags and placing them in variables
	 * @param dogtags2
	 */
	private void getDogtags(JSONObject dogtags2) {
			getAdvanceDogtags(dogtags2);
			getBasicDogtags(dogtags2);
	}

	/**
	 * Getting advanced dogtags and placing in variables
	 * @param advancedogtag2
	 */
	private void getAdvanceDogtags(JSONObject dogtags2) {		
		try {
			advancedogtag = dogtags2.getJSONObject(TAG_ADVANCE);
			setPlayerAddvenceDogtagName(advancedogtag.getString(TAG_NAME));
			setPlayerAddvenceDogtagDesc(advancedogtag.getString(TAG_DESC));
			setPlayerAddvanceDogtagImage(advancedogtag.getString(TAG_SMALL_IMAGE));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	/**
	 * Getting basic dogtags
	 * @param basicdogtag2
	 */
	private void getBasicDogtags(JSONObject dogtags2) {
		try {
			basicdogtag = dogtags2.getJSONObject(TAG_BASIC);
			setPlayerBasicDoggtagName(basicdogtag.getString(TAG_NAME));
			setPlayerBasicDogtagDesc(basicdogtag.getString(TAG_DESC));
			setPlayerBasicDogtagImage(basicdogtag.getString(TAG_SMALL_IMAGE));
		} catch (JSONException e) {
			// TODO: handle exception
		}
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public String getPlayerLanguage() {
		return playerLanguage;
	}

	public void setPlayerLanguage(String playerLanguage) {
		this.playerLanguage = playerLanguage;
	}

	public String getPlayerCountryName() {
		return playerCountryName;
	}

	public void setPlayerCountryName(String playerCountryName) {
		this.playerCountryName = playerCountryName;
	}

	public String getPlayerBasicDoggtagName() {
		return playerBasicDoggtagName;
	}

	public void setPlayerBasicDoggtagName(String playerBasicDoggtagName) {
		this.playerBasicDoggtagName = playerBasicDoggtagName;
	}

	public String getPlayerCountryImage() {
		return playerCountryImage;
	}

	public void setPlayerCountryImage(String playerCountryImage) {
		this.playerCountryImage = playerCountryImage;
	}

	public String getPlayerBasicDogtagImage() {
		return playerBasicDogtagImage;
	}

	public void setPlayerBasicDogtagImage(String playerBasicDogtagImage) {
		this.playerBasicDogtagImage = playerBasicDogtagImage;
	}

	public long getPlayerScore() {
		return playerScore;
	}

	public void setPlayerScore(long l) {
		this.playerScore = l;
	}

	public String getPlayerAddvenceDogtagName() {
		return playerAddvenceDogtagName;
	}

	public void setPlayerAddvenceDogtagName(String playerAddvenceDogtagName) {
		this.playerAddvenceDogtagName = playerAddvenceDogtagName;
	}

	public String getPlayerAddvanceDogtagImage() {
		return playerAddvanceDogtagImage;
	}

	public void setPlayerAddvanceDogtagImage(String playerAddvanceDogtagImage) {
		this.playerAddvanceDogtagImage = playerAddvanceDogtagImage;
	}

	public String getPlayerBasicDogtagDesc() {
		return playerBasicDogtagDesc;
	}

	public void setPlayerBasicDogtagDesc(String playerBasicDogtagDesc) {
		this.playerBasicDogtagDesc = playerBasicDogtagDesc;
	}

	public String getPlayerAddvenceDogtagDesc() {
		return playerAddvenceDogtagDesc;
	}

	public void setPlayerAddvenceDogtagDesc(String playerAddvenceDogtagDesc) {
		this.playerAddvenceDogtagDesc = playerAddvenceDogtagDesc;
	}

	public String getPlayerRank() {
		return playerRank;
	}

	public void setPlayerRank(String playerRank) {
		this.playerRank = playerRank;
	}

	public String getPlayerRankImmage() {
		return playerRankImmage;
	}

	public void setPlayerRankImmage(String playerRankImmage) {
		this.playerRankImmage = playerRankImmage;
	}

	public long getPlayerAwardPoints() {
		return playerAwardPoints;
	}

	public void setPlayerAwardPoints(long l) {
		this.playerAwardPoints = l;
	}

	public long getPlayerObjectiveScore() {
		return playerObjectiveScore;
	}

	public void setPlayerObjectiveScore(long l) {
		this.playerObjectiveScore = l;
	}

	public long getPlayerTeamScore() {
		return playerTeamScore;
	}

	public void setPlayerTeamScore(long l) {
		this.playerTeamScore = l;
	}

	public long getPlayerGeneralScore() {
		return playerGeneralScore;
	}

	public void setPlayerGeneralScore(long l) {
		this.playerGeneralScore = l;
	}

	public long getPlayerIFVScore() {
		return playerIFVScore;
	}

	public void setPlayerIFVScore(long l) {
		this.playerIFVScore = l;
	}

	public long getPlayerBonusPoints() {
		return playerBonusPoints;
	}

	public void setPlayerBonusPoints(long l) {
		this.playerBonusPoints = l;
	}

	public long getPlayerUnlocksScore() {
		return playerUnlocksScore;
	}

	public void setPlayerUnlocksScore(long l) {
		this.playerUnlocksScore = l;
	}

	public long getPlayerSquadScore() {
		return playerSquadScore;
	}

	public void setPlayerSquadScore(long l) {
		this.playerSquadScore = l;
	}

	public long getPlayerJetScore() {
		return playerJetScore;
	}

	public void setPlayerJetScore(long l) {
		this.playerJetScore = l;
	}

	public long getPlayerAttackHeliScore() {
		return playerAttackHeliScore;
	}

	public void setPlayerAttackHeliScore(long playerAttackHeliScore) {
		this.playerAttackHeliScore = playerAttackHeliScore;
	}

	public long getPlayerAAVehicleScore() {
		return playerAAVehicleScore;
	}

	public void setPlayerAAVehicleScore(long l) {
		this.playerAAVehicleScore = l;
	}

	public long getPlayerMBTScore() {
		return playerMBTScore;
	}

	public void setPlayerMBTScore(long l) {
		this.playerMBTScore = l;
	}

	public long getPlayerVehicleScore() {
		return playerVehicleScore;
	}

	public void setPlayerVehicleScore(long l) {
		this.playerVehicleScore = l;
	}

	public long getPlayerReaconScore() {
		return playerReaconScore;
	}

	public void setPlayerReaconScore(long l) {
		this.playerReaconScore = l;
	}

	public long getPlayerScoutHellyScore() {
		return playerScoutHellyScore;
	}

	public void setPlayerScoutHellyScore(long l) {
		this.playerScoutHellyScore = l;
	}

	public long getPlayerEnginerScore() {
		return playerEnginerScore;
	}

	public void setPlayerEnginerScore(long l) {
		this.playerEnginerScore = l;
	}

	public long getPlayerSuportScore() {
		return playerSuportScore;
	}

	public void setPlayerSuportScore(long l) {
		this.playerSuportScore = l;
	}

	public long getPlayerAssaultScore() {
		return playerAssaultScore;
	}

	public void setPlayerAssaultScore(long l) {
		this.playerAssaultScore = l;
	}

	public long getPlayerGameLose() {
		return playerGameLose;
	}

	public void setPlayerGameLose(long playerGameLose) {
		this.playerGameLose = playerGameLose;
	}

	public long getPlayerGameWin() {
		return playerGameWin;
	}

	public void setPlayerGameWin(long playerGameWin) {
		this.playerGameWin = playerGameWin;
	}

	public long getPlayerGlobalKills() {
		return playerGlobalKills;
	}

	public void setPlayerGlobalKills(long playerGlobalKills) {
		this.playerGlobalKills = playerGlobalKills;
	}

	public long getPlayerGlobalDeaths() {
		return playerGlobalDeaths;
	}

	public void setPlayerGlobalDeaths(long playerGlobalDeaths) {
		this.playerGlobalDeaths = playerGlobalDeaths;
	}

	public long getPlayerShootsFired() {
		return playerShootsFired;
	}

	public void setPlayerShootsFired(long playerShootsFired) {
		this.playerShootsFired = playerShootsFired;
	}

	public long getPlayerVehicleKills() {
		return playerVehicleKills;
	}

	public void setPlayerVehicleKills(long playerVehicleKills) {
		this.playerVehicleKills = playerVehicleKills;
	}

	public long getPlayerKillAssist() {
		return playerKillAssist;
	}

	public void setPlayerKillAssist(long playerKillAssist) {
		this.playerKillAssist = playerKillAssist;
	}

	public long getPlayerVehicleTime() {
		return playerVehicleTime;
	}

	public void setPlayerVehicleTime(long playerVehicleTime) {
		this.playerVehicleTime = playerVehicleTime;
	}

	public long getPlayerHeadShoots() {
		return playerHeadShoots;
	}

	public void setPlayerHeadShoots(long playerHeadShoots) {
		this.playerHeadShoots = playerHeadShoots;
	}

	public long getPlayerHits() {
		return playerHits;
	}

	public void setPlayerHits(long playerHits) {
		this.playerHits = playerHits;
	}

	public long getPlayerLongestHeadshoot() {
		return playerLongestHeadshoot;
	}

	public void setPlayerLongestHeadshoot(long playerLongestHeadshoot) {
		this.playerLongestHeadshoot = playerLongestHeadshoot;
	}

	public long getPlayerGameTime() {
		return playerGameTime;
	}

	public void setPlayerGameTime(long playerGameTime) {
		this.playerGameTime = playerGameTime;
	}

	public long getPlayerRevives() {
		return playerRevives;
	}

	public void setPlayerRevives(long playerRevives) {
		this.playerRevives = playerRevives;
	}

	public long getPlayerResuply() {
		return playerResuply;
	}

	public void setPlayerResuply(long playerResuply) {
		this.playerResuply = playerResuply;
	}

	public long getPlayerVehicleDestroyedAssistance() {
		return playerVehicleDestroyedAssistance;
	}

	public void setPlayerVehicleDestroyedAssistance(
			long playerVehicleDestroyedAssistance) {
		this.playerVehicleDestroyedAssistance = playerVehicleDestroyedAssistance;
	}

	public long getPlayerRoundsPlayed() {
		return playerRoundsPlayed;
	}

	public void setPlayerRoundsPlayed(long playerRoundsPlayed) {
		this.playerRoundsPlayed = playerRoundsPlayed;
	}

	public long getPlayerDogtagsTaken() {
		return playerDogtagsTaken;
	}

	public void setPlayerDogtagsTaken(long playerDogtagsTaken) {
		this.playerDogtagsTaken = playerDogtagsTaken;
	}

	public long getPlayerVehicleDestroyed() {
		return playerVehicleDestroyed;
	}

	public void setPlayerVehicleDestroyed(long playerVehicleDestroyed) {
		this.playerVehicleDestroyed = playerVehicleDestroyed;
	}

	public long getPlayerRoundsFinished() {
		return playerRoundsFinished;
	}

	public void setPlayerRoundsFinished(long playerRoundsFinished) {
		this.playerRoundsFinished = playerRoundsFinished;
	}

	public long getPlayerHeals() {
		return playerHeals;
	}

	public void setPlayerHeals(long playerHeals) {
		this.playerHeals = playerHeals;
	}

	public long getPlayerRepaires() {
		return playerRepaires;
	}

	public void setPlayerRepaires(long playerRepaires) {
		this.playerRepaires = playerRepaires;
	}

	public long getPlayerKillStreek() {
		return playerKillStreek;
	}

	public void setPlayerKillStreek(long playerKillStreek) {
		this.playerKillStreek = playerKillStreek;
	}

	public long getPlayerMCOMDefendKills() {
		return playerMCOMDefendKills;
	}

	public void setPlayerMCOMDefendKills(long playerMCOMDefendKills) {
		this.playerMCOMDefendKills = playerMCOMDefendKills;
	}

	public long getPlayerAvangerKills() {
		return playerAvangerKills;
	}

	public void setPlayerAvangerKills(long playerAvangerKills) {
		this.playerAvangerKills = playerAvangerKills;
	}

	public long getPlayerSavioureKills() {
		return playerSavioureKills;
	}

	public void setPlayerSavioureKills(long playerSavioureKills) {
		this.playerSavioureKills = playerSavioureKills;
	}

	public long getPlayerMCOMDestroyed() {
		return playerMCOMDestroyed;
	}

	public void setPlayerMCOMDestroyed(long playerMCOMDestroyed) {
		this.playerMCOMDestroyed = playerMCOMDestroyed;
	}

	public long getPlayerNemessiskills() {
		return playerNemessiskills;
	}

	public void setPlayerNemessiskills(long playerNemessiskills) {
		this.playerNemessiskills = playerNemessiskills;
	}

	public long getPlayerDamageAssist() {
		return playerDamageAssist;
	}

	public void setPlayerDamageAssist(long playerDamageAssist) {
		this.playerDamageAssist = playerDamageAssist;
	}

	public long getPlayerNemmesisStreak() {
		return playerNemmesisStreak;
	}

	public void setPlayerNemmesisStreak(long playerNemmesisStreak) {
		this.playerNemmesisStreak = playerNemmesisStreak;
	}

	public long getPlayerSupression() {
		return playerSupression;
	}

	public void setPlayerSupression(long playerSupression) {
		this.playerSupression = playerSupression;
	}

	public long getPlayerFlagCaptured() {
		return playerFlagCaptured;
	}

	public void setPlayerFlagCaptured(long playerFlagCaptured) {
		this.playerFlagCaptured = playerFlagCaptured;
	}

	public long getPlayerFlagDefendedKills() {
		return playerFlagDefendedKills;
	}

	public void setPlayerFlagDefendedKills(long playerFlagDefendedKills) {
		this.playerFlagDefendedKills = playerFlagDefendedKills;
	}
	
	public void setConnectionProblems(boolean problem){
		this.connectionprobelem = problem;
	}
	
	public boolean getConnectionProblems() {
		return connectionprobelem;
	}
	
	public void setPlayerName(String playerName){
		this.playerName = playerName;
	}
	
	public String getPlayerName(){
		return playerName;
	}
	
	public void setPlayerPlatform(String playerPlatform){
		this.playerPlatform = playerPlatform;
	}
	
	public String getPlayerPlatform(){
		return playerPlatform;
	}
	
	public HashMap getPlayerhashMap(){
		return meMap;
	}

	public void setPlayerSkill(long playerSkill) {
		this.playerSkill = playerSkill;
	}

	public long getPlayerSkill() {
		return playerSkill;
	}

	public void setWeapons(JSONObject weapons) {
		this.weapons = weapons;
	}

	public JSONObject getWeapons() {
		return weapons;
	}

	public void setVehicles(JSONObject vehicles) {
		this.vehicles = vehicles;
	}

	public JSONObject getVehicles() {
		return vehicles;
	}
	public void setRankAndScore(JSONObject rankandScore){
		this.rankAndScore = rankandScore;
	}
	
	public JSONObject getRankAndScore(){
		return rankAndScore;
	}

	public void setMedals(JSONObject medals) {
		this.medals = medals;
	}

	public JSONObject getMedals() {
		return medals;
	}

	public void setRibons(JSONObject ribons) {
		this.ribons = ribons;
	}

	public JSONObject getRibons() {
		return ribons;
	}

	public JSONObject getKit() {
		return kit;
	}

	public void setKit(JSONObject kit) {
		this.kit = kit;
	}
	
	public JSONObject getEquipements() {
		return equipement;
	}

	public JSONObject getVehiclekategories() {
		return vehiclekategories;
	}

	public void setVehiclekategories(JSONObject vehiclekategories) {
		this.vehiclekategories = vehiclekategories;
	}

	public long getPlayerScoreLeftToNextRank() {
		return playerScoreLeftToNextRank;
	}

	public void setPlayerScoreLeftToNextRank(long playerScoreLeftToNextRank) {
		this.playerScoreLeftToNextRank = playerScoreLeftToNextRank;
	}

	public long getPlayerNextRankScore() {
		return playerNextRankScore;
	}

	public void setPlayerNextRankScore(long playerNextRankScore) {
		this.playerNextRankScore = playerNextRankScore;
	}

	public long getPlayerPreviouseRankScore() {
		return playerPreviouseRankScore;
	}

	public void setPlayerPreviouseRankScore(long playerPreviouseRankScore) {
		this.playerPreviouseRankScore = playerPreviouseRankScore;
	}

	public JSONObject getAssighments() {
		return assighments;
	}

	public void setAssighments(JSONObject assighments) {
		this.assighments = assighments;
	}
	public String getJsonString2() {
		return jsonString2;
	}
	public void setJsonString2(String jsonString2) {
		this.jsonString2 = jsonString2;
	}
	public JSONObject getJsonObject2() {
		return jsonObject2;
	}
	public void setJsonObject2(JSONObject jsonObject2) {
		this.jsonObject2 = jsonObject2;
	}
	public JSONObject getRankAndScore2() {
		return score2;
	}
	public void setRankAndScore2(JSONObject rankAndScore2) {
		try {
			rank = rankAndScore2.getJSONObject(TAG_SCORES);
			this.score2 = rank;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.rankAndScore2 = rankAndScore2;
	}
	public JSONObject getGlobals2() {
		return globals2;
	}
	public void setGlobals2(JSONObject globals2) {
		try {
			rank = globals2.getJSONObject(TAG_GLOBALS);
			this.globals2 = rank;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getRank2() {
		return rank2;
	}
	public void setRank2(JSONObject rank2) {
		try {
			rank = rank2.getJSONObject(TAG_RANK);
			this.rank2 = rank.getString(TAG_LEVEL);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public JSONObject getStatPoints2() {
		return statPoints2;
	}
	public void setStatPoints2(JSONObject statPoints2) {
		this.statPoints2 = statPoints2;
	}
	public JSONObject getWeapons2() {
		return weapons2;
	}
	public void setWeapons2(JSONObject weapons2) {
		this.weapons2 = weapons2;
	}
	public JSONObject getVehicles2() {
		return vehicles2;
	}
	public void setVehicles2(JSONObject vehicles2) {
		this.vehicles2 = vehicles2;
	}
	public JSONObject getMedals2() {
		return medals2;
	}
	public void setMedals2(JSONObject medals2) {
		this.medals2 = medals2;
	}
	public JSONObject getRibons2() {
		return ribons2;
	}
	public void setRibons2(JSONObject ribons2) {
		this.ribons2 = ribons2;
	}
	public JSONObject getVehicleUnlocks2() {
		return vehicleUnlocks2;
	}
	public void setVehicleUnlocks2(JSONObject vehicleUnlocks2) {
		this.vehicleUnlocks2 = vehicleUnlocks2;
	}
	public JSONObject getKit2() {
		return kit2;
	}
	public void setKit2(JSONObject kit2) {
		this.kit2 = kit2;
	}
	public JSONObject getVehiclekategories2() {
		return vehiclekategories2;
	}
	public void setVehiclekategories2(JSONObject vehiclekategories2) {
		this.vehiclekategories2 = vehiclekategories2;
	}
	public JSONObject getEquipement2() {
		return equipement2;
	}
	public void setEquipement2(JSONObject equipement2) {
		this.equipement2 = equipement2;
	}
	public JSONObject getAssighments2() {
		return assighments2;
	}
	public void setAssighments2(JSONObject assighments2) {
		this.assighments2 = assighments2;
	}
	public String getPlayer2name() {
		return player2name;
	}
	public void setPlayer2name(String player2name) {
		this.player2name = player2name;
	}
	public String getPlayer2platform() {
		return player2platform;
	}
	public void setPlayer2platform(String player2platform) {
		this.player2platform = player2platform;
	}
}
