package com.bf3stats;

import java.text.DecimalFormat;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PageFragment extends Fragment{

	public static PageFragment newInstance(int page) {
		  
        PageFragment pageFragment = new PageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("key", page);
        pageFragment.setArguments(bundle);
        return pageFragment;
    }

	private ImageManager imageManager;
	private Context context;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;
	private String tag = "djevtic";
	public Player player = Player.getInstance();
	private ImageView bfstatimage;
	private ImageView oldguys;
	@Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
    }  
      
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        int page = getArguments().getInt("key");
        View view = null;
        LoadImageLibs();
        if(page ==0){
        	view = inflater.inflate(R.layout.playerstatlayout, container, false);
        	fillPage(page,view);
        }else if(page==10){
        	view = inflater.inflate(R.layout.about, container, false);
        	fillPage(page,view);
            addListenerOnButton(view);
            addImageOnClickListener(view);
        }/*else if(page==2){
        	view = inflater.inflate(R.layout.playerkills, container, false);
        	fillPage(page,view);
        }*/
        //loadAdds(view);
        return view;  
    }
    
    private void fillPage(int page, View view) {
    	if(page==0){
    		TextView playerName = (TextView) view.findViewById(R.id.PlayerName);
    		ImageView playerRankImg = (ImageView) view.findViewById(R.id.PlayerRankImg);
    		ImageView playerDogtagBasic = (ImageView) view.findViewById(R.id.playerdogtagbasic);
    		ImageView playerDogtagAdvance = (ImageView) view.findViewById(R.id.playerdogtagadvance);
    		playerName.setText(player.getPlayerName());
    		setImage(playerRankImg, player.getPlayerRankImmage());
    		setImage(playerDogtagBasic, player.getPlayerBasicDogtagImage());
    		setImage(playerDogtagAdvance, player.getPlayerAddvanceDogtagImage());
    		TextView playerRounds = (TextView) view.findViewById(R.id.textRounds);
    		TextView playerFinishedRounds = (TextView) view.findViewById(R.id.textFinishedRounds);
    		TextView playerSkils = (TextView) view.findViewById(R.id.textSkils);
    		TextView playerWins = (TextView) view.findViewById(R.id.textWins);
    		TextView playerLoses = (TextView) view.findViewById(R.id.textLoses);
    		TextView playerKils = (TextView) view.findViewById(R.id.textKills);
    		TextView playerDeaths = (TextView) view.findViewById(R.id.textDeths);
    		TextView playerHeadshots = (TextView) view.findViewById(R.id.textHeadshots);
    		TextView playerLongestHeadshot = (TextView) view.findViewById(R.id.textLongestHeadshots);
    		TextView playerKillStreak = (TextView) view.findViewById(R.id.textKillStreak);
    		TextView playerShots = (TextView) view.findViewById(R.id.textShots);
    		TextView playerHits = (TextView) view.findViewById(R.id.textHits);
    		TextView playerAcuracy = (TextView) view.findViewById(R.id.textAccuracy);
    		TextView playerAssault = (TextView) view.findViewById(R.id.textAssault);
    		TextView playerEnginer = (TextView) view.findViewById(R.id.textEnginer);
    		TextView playerReacon = (TextView) view.findViewById(R.id.textReacon);
    		TextView playerSuport = (TextView) view.findViewById(R.id.textSuport);
    		TextView playerVehicle = (TextView) view.findViewById(R.id.textVehicle);
    		TextView playerAwards = (TextView) view.findViewById(R.id.textAwards);
    		TextView playerUnlocks = (TextView) view.findViewById(R.id.textUnlocks);
    		TextView playerTeam = (TextView) view.findViewById(R.id.textTeam);
    		TextView playerSquad = (TextView) view.findViewById(R.id.textSquad);
    		TextView playerBonus = (TextView) view.findViewById(R.id.textBonus);
    		TextView playerObjective = (TextView) view.findViewById(R.id.textObjective);
    		TextView playerGeneral = (TextView) view.findViewById(R.id.textGeneral);
    		TextView playerScore = (TextView) view.findViewById(R.id.textScore);
    		TextView playerTime = (TextView) view.findViewById(R.id.textTime);
    		TextView playerSMP = (TextView) view.findViewById(R.id.textSPM);
    		TextView playerKD = (TextView) view.findViewById(R.id.textKD);
    		TextView playerWL = (TextView) view.findViewById(R.id.textWL);
    		TextView playerRank = (TextView) view.findViewById(R.id.textRank);
    		TextView playerKillAssist = (TextView) view.findViewById(R.id.textKillAssist);
    		TextView playerDestroyAssist = (TextView) view.findViewById(R.id.textDestroyAssist);
    		TextView playerDamageAssist = (TextView) view.findViewById(R.id.textDamageAssist);
    		TextView playerSupresionAssist = (TextView) view.findViewById(R.id.textSupressionAssist);
    		TextView playerRevive = (TextView) view.findViewById(R.id.textRevive);
    		TextView playerResuply = (TextView) view.findViewById(R.id.textResuply);
    		TextView playerHeals = (TextView) view.findViewById(R.id.textHeals);
    		TextView playerRepaire = (TextView) view.findViewById(R.id.textRepaire);
    		playerRounds.setText(""+player.getPlayerRoundsPlayed());
    		playerFinishedRounds.setText(""+player.getPlayerRoundsFinished());
    		playerSkils.setText(""+player.getPlayerSkill());
    		playerWins.setText(""+player.getPlayerGameWin());
    		playerLoses.setText(""+player.getPlayerGameLose());
    		playerKils.setText(""+player.getPlayerGlobalKills());
    		playerDeaths.setText(""+player.getPlayerGlobalDeaths());
    		playerHeadshots.setText(""+player.getPlayerHeadShoots());
    		playerLongestHeadshot.setText(""+player.getPlayerLongestHeadshoot());
    		playerKillStreak.setText(""+player.getPlayerKillStreek());
    		playerShots.setText(""+player.getPlayerShootsFired());
    		playerHits.setText(""+player.getPlayerHits());
    		playerAcuracy.setText(""+GetAccuracy(player.getPlayerHits(),player.getPlayerShootsFired())+"%");
    		playerAssault.setText(""+player.getPlayerAssaultScore());
    		playerEnginer.setText(""+player.getPlayerEnginerScore());
    		playerReacon.setText(""+player.getPlayerReaconScore());
    		playerSuport.setText(""+player.getPlayerSuportScore());
    		playerVehicle.setText(""+player.getPlayerVehicleScore());
    		playerAwards.setText(""+player.getPlayerAwardPoints());
    		playerUnlocks.setText(""+player.getPlayerUnlocksScore());
    		playerTeam.setText(""+player.getPlayerTeamScore());
    		playerSquad.setText(""+player.getPlayerSquadScore());
    		playerBonus.setText(""+player.getPlayerBonusPoints());
    		playerObjective.setText(""+player.getPlayerObjectiveScore());
    		playerGeneral.setText(""+player.getPlayerGeneralScore());
    		playerScore.setText(""+player.getPlayerScore());
    		playerTime.setText(""+getTime(player.getPlayerGameTime()));
    		playerSMP.setText(""+getScorePerMinute(player.getPlayerScore(), player.getPlayerGameTime()));
    		playerKD.setText(""+GetTwoDecimal(player.getPlayerGlobalKills(), player.getPlayerGlobalDeaths()));
    		playerWL.setText(""+GetTwoDecimal(player.getPlayerGameWin(),player.getPlayerGameLose()));
    		playerRank.setText(""+player.getPlayerRank());
    		playerKillAssist.setText(""+player.getPlayerKillAssist());
    		playerDestroyAssist.setText(""+player.getPlayerVehicleDestroyedAssistance());
    		playerDamageAssist.setText(""+player.getPlayerDamageAssist());
    		playerSupresionAssist.setText(""+player.getPlayerSupression());
    		playerRevive.setText(""+player.getPlayerRevives());
    		playerResuply.setText(""+player.getPlayerResuply());
    		playerHeals.setText(""+player.getPlayerHeals());
    		playerRepaire.setText(""+player.getPlayerRepaires());
    	}
    }
    
    private void LoadImageLibs() {
		LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(getActivity());
		imageManager = new ImageManager(getActivity(), settings);
		imageTagFactory = new ImageTagFactory(getActivity(),R.drawable.oldguysgamingscaled);
		imageTagFactory.setErrorImageId(R.drawable.oldguysgamingscaled);
		imageLoader = imageManager.getLoader();
		
	}
    
    private void setImage(ImageView imageView, String url) {
		Log.v(tag  ,"ImageView == "+imageView);
		Log.v(tag ,"url == "+url);
		String link = "http://dl.dropbox.com/u/1480498/bf3/" + url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}
    
    public String getTime(long timeLong){
		String timeString = "";
		int seconds = (int) (timeLong) % 60 ;
		int minutes = (int) ((timeLong/60) % 60);
		int hours   = (int) ((timeLong / (60*60)) % 24);
		int days   = (int) ((timeLong / (60*60*24)) % 365);
		if(days>0){
			timeString = timeString+days+"d ";
		}
		if(hours>0){
			timeString = timeString+hours+"h ";
		}
		timeString = timeString+minutes+"m "+seconds+"s ";
		return timeString;
	}
	
	public String GetAccuracy(long first, long secound){
		 
        double d;
        d = ((double)first/(double)secound)*100;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	private long getScorePerMinute(long playerScore, long playerGameTime) {
		long gameTimeInMinutes = playerGameTime/60;
		long scorePerMinute = playerScore/gameTimeInMinutes;
		return scorePerMinute;
	}
	
	public String GetTwoDecimal(long first, long secound){
		 
        double d;
        d = (double)first/(double)secound;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	private void addImageOnClickListener(View view) {
		bfstatimage = (ImageView) view.findViewById(R.id.bfstatimage);
		bfstatimage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bf3stats.com/"));
				startActivity(urlIntent);
			}
 
		});
		oldguys = (ImageView) view.findViewById(R.id.bfstatimage);
		oldguys.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.oldguys.eu/"));
				startActivity(urlIntent);
			}
 
		});
	}
	
	public void addListenerOnButton(View view) {
		 
		Button emailButton = (Button) view.findViewById(R.id.email_button);
		Button marketButton = (Button) view.findViewById(R.id.market_button);
 
		emailButton.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
 
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"gisseares@gmail.com"});
				i.putExtra(Intent.EXTRA_SUBJECT, "BF3Stats");
				i.putExtra(Intent.EXTRA_TEXT   , "Enter your text here");
				try {
				    startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
 
			}
 
		});
		
		marketButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.bf3stats"));
				startActivity(intent);
 
			}
 
		});
 
	}
}
