package com.bf3stats;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.inmobi.androidsdk.IMAdInterstitial;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.IMAdView;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PageListFragment extends ListFragment{
	
	private Player player;
	private String tag = "djevtic";
	private GlobalStatsArrayAdaptor adapter;
	private HashMap<String, String> meMap;
	private HashMap<String, String> weaponHash;
	private String weponCode;
	private JSONObject weaponJSON;
	private JSONObject separateWeaponString;
	private HashMap<String, String> vehicleHash;
	private String vehicleCode;
	private JSONObject vehicleJSON;
	private JSONObject separateVehicleString;
	private String[] weaponString;
	private JSONArray weaponUnlocksArray;
	private JSONObject weaponUnlocksSeparated;
	private JSONObject separateVehicle;
	private JSONArray unlocksArray;
	private JSONObject row;
	private JSONObject vehiclekategoriesJSON;
	private JSONObject vehicleunlocksObject;
	private JSONArray vehicleKategoryUnlocksArray;
	private JSONObject vehicleUnlockSingle;
	private JSONObject medalsJSON;
	private JSONObject medalObject;
	private IMAdView mIMAdView;
	private IMAdRequest mAdRequest;
	private IMAdInterstitial mIMAdInterstitial;
	
	public static PageListFragment newInstance(int page) {
		  
		PageListFragment pageListFragment = new PageListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("key", page);
        pageListFragment.setArguments(bundle);
        return pageListFragment;
    }

	@Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
    }  
      
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        int page = getArguments().getInt("key");
        player = Player.getInstance();
        View view = null;
        	view = inflater.inflate(R.layout.statistics, container, false);
        	fillPage(page,view);
        	loadAdds(view);
        
        return view;  
    }
    
    private void loadAdds(View view) {
    	try{
        	// Get the IMAdView instance
        				mIMAdView = (IMAdView) view.findViewById(R.id.imAdview);			
        				
        				// set the test mode to true (Make sure you set the test mode to false
        				// when distributing to the users)
        				mAdRequest = new IMAdRequest();
        				mAdRequest.setTestMode(false);
        				mIMAdView.setIMAdRequest(mAdRequest);
        				
        				//load new ad has to be called explicitly..Just adding xml to layout wont call load new ad
        				mIMAdView.loadNewAd(mAdRequest);
        				
        				// initialize an interstitial ad
        				mIMAdInterstitial = new IMAdInterstitial(getActivity(),"4028cbff3b77ce76013b84ef8e710077");
        				    			
        			}catch(InflateException e){
        				/*
        				 * Check the following in your configuration
        				 * 1. Is Ad Network SDK and IMCommons SDK from the same release bundle ?
        				 * 2. Is Activity value null ?
        				 * 3. Is APP ID value null ?
        				 * 4. Is APP ID value empty ?
        				 * 5. Is AD SIZE value negative ?			  
        				 */
        				e.printStackTrace();
        			}catch(IllegalArgumentException e){
        				/* 1. Is APP ID value empty ?
        				 * 2. Is AD SIZE value negative ?
        				 */
        				e.printStackTrace();
        			}catch(NullPointerException e){
        				/*
        				 * 1.Is APP ID value null ?
        				 * 2. Is Activity value null?
        				 */
        				e.printStackTrace();
        			}
		
	}

	private void fillPage(int page, View view) {
		if(page==1){
			//Weapons
			adapter = new GlobalStatsArrayAdaptor(getActivity(), sortByKill(), "Weapon Stats");
			setListAdapter(adapter);
		}else if(page==2){
			//Vehicles
			adapter = new GlobalStatsArrayAdaptor(getActivity(), getVehicleByKills(), "Vehicle Stats");
			setListAdapter(adapter);
		}else if(page==3){
			//Medals
			adapter = new GlobalStatsArrayAdaptor(getActivity(), MEDALS, "Medals");
			setListAdapter(adapter);
		}else if(page==4){
			//Ribons
			adapter = new GlobalStatsArrayAdaptor(getActivity(), RIBONS, "Ribbons");
			setListAdapter(adapter);
		}else if(page==5){
			//Assighnments
			adapter = new GlobalStatsArrayAdaptor(getActivity(), getAsighmentsString(), "Assignments");
			setListAdapter(adapter);
		}else if(page==6){
			//Equipement
			adapter = new GlobalStatsArrayAdaptor(getActivity(), getEquipementString(), "Equipement");
			setListAdapter(adapter);
		}else if(page==7){
			//weaponUnlocks
			adapter = new GlobalStatsArrayAdaptor(getActivity(), getWeaponUnlockString(), "Weapon Unlocks");
			setListAdapter(adapter);
		}else if(page==8){
			adapter = new GlobalStatsArrayAdaptor(getActivity(), getVehicleUnlockString(), "Vehicle Unlocks");
			setListAdapter(adapter);
		}else if(page==9){
			adapter = new GlobalStatsArrayAdaptor(getActivity(), getMedalUnlockString(), "Medals To Unlock");
			setListAdapter(adapter);
		}
		
	}
    
    private String[] sortByKill() {
		String[] weaponString = getPlayerWeaponString();
		String[][] weaponKillsArray = new String [weaponString.length][2];
		for(int i = 0; weaponString.length>i;i++){
			weaponHash = weaponHashMap();
			weponCode = weaponHash.get(weaponString[i]);
			try {
				weaponJSON = player.getWeapons();
				separateWeaponString = weaponJSON.getJSONObject(weponCode);
				weaponKillsArray[i][1] = separateWeaponString.getString("kills");
				weaponKillsArray[i][0] = weaponString[i];
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String[][] temp = new String[1][2];
		for(int counter=0; weaponString.length>counter; counter++) { //Loop once for each element in the array.
	            for (int i = 0; i < weaponKillsArray.length - 1; i++){
	        		int min = i;

	        		for (int  j = i + 1; j < weaponKillsArray.length; j++){
	        			if (Long.parseLong(weaponKillsArray[j][1]) > Long.parseLong(weaponKillsArray[min][1])){

	        				min = j;

	        			}

	        		}
	        		if (i != min){ 
	        			temp[0][0] = weaponKillsArray[i][0];
	        			temp[0][1] = weaponKillsArray[i][1];
	        			//int tmp = data[i];
	        			weaponKillsArray[i][0]=weaponKillsArray[min][0];
	        			weaponKillsArray[i][1]=weaponKillsArray[min][1];
	        			//data[i] = data[min];
	        			weaponKillsArray[min][0]=temp[0][0];
	        			weaponKillsArray[min][1]=temp[0][1];
	        			//data[min] = tmp;

	    	      }

	        	}
	        }
		String[] arrayToReturn= new String[weaponKillsArray.length];
		for(int i = 0; weaponKillsArray.length>i;i++){
			arrayToReturn[i]=weaponKillsArray[i][0];
		}
		return arrayToReturn;
	}
    
    public static String[] getPlayerWeaponString() {
		String[] menuList = new String[] {"MP7", "G17C", "M98B", "SG553","870MCS","MP443 SUPP.","RPG-7V2", "MTAR-21", "KH2002", "AKS-74u", "M1911 SUPP.", 
				"M27 IAR", "G36C", ".44 MAGNUM", "AS VAL", "DAO-12", "M320", "UMP-45", "SA-18 IGLA", "SCAR-H", "MP412 REX", "M416", "AK-74M", "PKP PECHENEG", 
				"M60E4", "M240B", "M16A4", "M1911 TACT.", "PP-2000", "G3A3", "M4A1", "AEK-971", "AUG A3", "MK11 MOD 0", "AN-94", "SPAS-12", ".44 SCOPED", 
				"USAS-12", "MP443", "RPK-74M", "ACB-90", "L85A2", "SV98", "M40A5", "SCAR-L", "ACW-R", "MG36", "SAIGA 12K", "FAMAS", "M1911 S-TAC", "93R", 
				"M9 SUPP.", "M1911", "SVD", "MK3A1", "G18 SUPP.", "M249", "G17C SUPP.", "QBU-88", "M9", "M26 MASS", "JNG-90", "L86A2", "LSAT", "SMAW", "P90", 
				"M417", "A-91", "SKS", "L96", "PP-19", "TYPE 88 LMG", "QBZ-95B", "M9 TACT.", "G18", "F2000", "FGM-148 JAVELIN", "M5K", "MP443 TACT.", "G53", 
				"FIM-92 STINGER", "M39 EMR", "M1014", "PDW-R", "QBB-95", "XBOW SCOPED", "XBOW"};
		Arrays.sort(menuList);
		return menuList;
	}
    
    public HashMap<String, String> weaponHashMap(){
		meMap = new HashMap<String, String>();
		meMap.put("MP7","smMP7");
		meMap.put("G17C","pG17");
		meMap.put("M98B","srM98");
		meMap.put("SG553","caSG553");
		meMap.put("870MCS","sg870");
		meMap.put("MP443 SUPP.","pMP443S");
		meMap.put("RPG-7V2","wLATRPG");
		meMap.put("MTAR-21","caMTAR21");
		meMap.put("KH2002","arKH");
		meMap.put("AKS-74u","caAKS");
		meMap.put("M1911 SUPP.","pM1911S");
		meMap.put("M27 IAR","mgM27");
		meMap.put("G36C", "caG36");
		meMap.put(".44 MAGNUM", "pTaur");
		meMap.put("AS VAL", "smVAL");
		meMap.put("DAO-12", "sgDAO");
		meMap.put("M320", "wahUGL");
		meMap.put("UMP-45", "smUMP");
		meMap.put("SA-18 IGLA", "wLAAIGL");
		meMap.put("SCAR-H", "caSCAR");
		meMap.put("MP412 REX", "pM412");
		meMap.put("M416", "arM416");
		meMap.put("AK-74M", "arAK74");
		meMap.put("PKP PECHENEG", "mgPech");
		meMap.put("M60E4", "mgM60");
		meMap.put("M240B","mgM240");
		meMap.put("M16A4","arM16");
		meMap.put("M1911 TACT.", "pM1911L");
		meMap.put("PP-2000", "smPP2000");
		meMap.put("G3A3","arG3");
		meMap.put("M4A1", "caM4");
		meMap.put("AEK-971","arAEK");
		meMap.put("AUG A3", "arAUG");
		meMap.put("MK11 MOD 0", "srMK11");
		meMap.put("AN-94","arAN94");
		meMap.put("SPAS-12", "sgSPAS12");
		meMap.put(".44 SCOPED","pTaurS");
		meMap.put("USAS-12","sgUSAS");
		meMap.put("MP443", "pMP443");
		meMap.put("RPK-74M", "mgRPK");
		meMap.put("ACB-90", "wasK");
		meMap.put("L85A2", "arL85A2");
		meMap.put("SV98", "srSV98");
		meMap.put("M40A5", "srM40");
		meMap.put("SCAR-L", "arSCARL");
		meMap.put("ACW-R", "caACR");
		meMap.put("MG36", "mgMG36");
		meMap.put("SAIGA 12K", "sgSaiga");
		meMap.put("FAMAS","arFAMAS");
		meMap.put("M1911 S-TAC", "pM1911T");
		meMap.put("93R", "pM93R");
		meMap.put("M9 SUPP.","pM9S");
		meMap.put("M1911", "pM1911");
		meMap.put("SVD", "srSVD");
		meMap.put("MK3A1", "sgJackH");
		meMap.put("G18 SUPP.", "pg18S");
		meMap.put("M249", "mgM249");
		meMap.put("G17C SUPP.","pG17S");
		meMap.put("QBU-88","srQBU88");
		meMap.put("M9", "pM9");
		meMap.put("M26 MASS", "wahUSG");
		meMap.put("JNG-90", "srJNG90");
		meMap.put("L86A2","mgL86A1");
		meMap.put("LSAT", "mgLSAT");
		meMap.put("SMAW", "wLATSMAW");
		meMap.put("P90", "smP90");
		meMap.put("M417","srHK417");
		meMap.put("A-91","caA91");
		meMap.put("SKS","srSKS");
		meMap.put("L96", "srL96");
		meMap.put("PP-19", "smPP19");
		meMap.put("TYPE 88 LMG", "mgT88");
		meMap.put("QBZ-95B", "caQBZ95B");
		meMap.put("M9 TACT.","pM9F");
		meMap.put("G18", "pg18");
		meMap.put("F2000", "arF2");
		meMap.put("FGM-148 JAVELIN", "wLATJAV");
		meMap.put("M5K","smM5K");
		meMap.put("MP443 TACT.", "pMP443L");
		meMap.put("G53", "caHK53");
		meMap.put("FIM-92 STINGER","wLAAFIM");
		meMap.put("M39 EMR", "srM39");
		meMap.put("M1014","sgM1014");
		meMap.put("PDW-R", "smPDR");
		meMap.put("QBB-95","mgQBB95");
		meMap.put("XBOW","wasXBK");
		meMap.put("XBOW SCOPED","wasXBS");
		return meMap;
	}
    
    public String[] getVehicleByKills(){
		String[] vehicleString = getPlayerVehiclesString();
		String[][] vehicleKillsArray = new String [vehicleString.length][2];
		for(int i = 0; vehicleString.length>i;i++){
			vehicleHash = vehicleHashMap();
			vehicleCode = vehicleHash.get(vehicleString[i]);
			try {
				vehicleJSON = player.getVehicles();
				separateVehicleString = vehicleJSON.getJSONObject(vehicleCode);
				vehicleKillsArray[i][1] = separateVehicleString.getString("kills");
				vehicleKillsArray[i][0] = vehicleString[i];
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String[][] temp = new String[1][2];
		for(int counter=0; vehicleString.length>counter; counter++) { //Loop once for each element in the array.
	            for (int i = 0; i < vehicleKillsArray.length - 1; i++){
	        		int min = i;

	        		for (int  j = i + 1; j < vehicleKillsArray.length; j++){
	        			if (Long.parseLong(vehicleKillsArray[j][1]) > Long.parseLong(vehicleKillsArray[min][1])){

	        				min = j;

	        			}

	        		}
	        		if (i != min){ 
	        			temp[0][0] = vehicleKillsArray[i][0];
	        			temp[0][1] = vehicleKillsArray[i][1];
	        			//int tmp = data[i];
	        			vehicleKillsArray[i][0]=vehicleKillsArray[min][0];
	        			vehicleKillsArray[i][1]=vehicleKillsArray[min][1];
	        			//data[i] = data[min];
	        			vehicleKillsArray[min][0]=temp[0][0];
	        			vehicleKillsArray[min][1]=temp[0][1];
	        			//data[min] = tmp;

	    	      }

	        	}
	        }
		String[] arrayToReturn= new String[vehicleKillsArray.length];
		for(int i = 0; vehicleKillsArray.length>i;i++){
			arrayToReturn[i]=vehicleKillsArray[i][0];
		}
		return arrayToReturn;
	}
    
    public static String[] getPlayerVehiclesString(){
		String[] menuList = new String[] {                   "UH-1Y VENOM",
                   "AAV-7A1 AMTRAC",
                   "M220 TOW LAUNCHER",
                   "F-35",
                   "T-90A",
                   "SKID LOADER",
                   "LAV-AD",
                   "MI-28 HAVOC",
                   "GAZ-3937 VODNIK",
                   "DPV",
                   "9K22 TUNGUSKA-M",
                   "AH-1Z VIPER",
                   "BMP-2M",
                   "LAV-25",
                   "M1 ABRAMS",
                   "RHIB BOAT",
                   "A-10 THUNDERBOLT",
                   "KA-60 KASATKA",
                   "PANTSIR-S1",
                   "F/A-18E SUPER HORNET",
                   "SU-25TM FROGFOOT",
                   "M1114 HMMWV",
                   "CENTURION C-RAM",
                   "VDV BUGGY",
                   "Z-11W",
                   "9M133 KORNET LAUNCHER",
                   "GROWLER ITV",
                   "SU-35BM FLANKER-E",
                   "BTR-90",
                   "AH-6J LITTLE BIRD",
                   "M1128",
                   "BM23",
                   "QUAD BIKE",
                   "SPRUT-SD",
                   "GUNSHIP",
                   "M142",
                   "RHINO",
                   "PHOENIX",
                   "BARSUK", 
                   "DROPSHIP",
                   "HMMWV ASRAD",
                   "DIRTBIKE",
                   "VODNIK AA"};
		Arrays.sort(menuList);
		return menuList;
	}
    
    private HashMap<String, String> vehicleHashMap() {
		meMap = new HashMap<String, String>();
         meMap.put("UH-1Y VENOM","trUH1");
         meMap.put("AAV-7A1 AMTRAC","trAAV");
         meMap.put("M220 TOW LAUNCHER","atTOW");
         meMap.put("F-35","jfx1F35JSF");
         meMap.put("T-90A","mbtT90");
         meMap.put("SKID LOADER","trSkid");
         meMap.put("LAV-AD","aaLAV");
         meMap.put("MI-28 HAVOC","ahMi28");
         meMap.put("GAZ-3937 VODNIK","trVod");
         meMap.put("DPV","trDpv");
         meMap.put("9K22 TUNGUSKA-M","aa9k22");
         meMap.put("AH-1Z VIPER","ahAH1Z");
         meMap.put("BMP-2M","ifvBMP");
         meMap.put("LAV-25","ifvLAV");
         meMap.put("M1 ABRAMS","mbtM1A");
         meMap.put("RHIB BOAT","trRIB");
         meMap.put("A-10 THUNDERBOLT","jaA10");
         meMap.put("KA-60 KASATKA","trKA60");
         meMap.put("PANTSIR-S1","saaPant");
         meMap.put("F/A-18E SUPER HORNET","jfF18");
         meMap.put("SU-25TM FROGFOOT","jaSU25");
         meMap.put("M1114 HMMWV","trHum");
         meMap.put("CENTURION C-RAM","saaCRAM");
         meMap.put("VDV BUGGY","trVDV");
         meMap.put("Z-11W","shz11");
         meMap.put("9M133 KORNET LAUNCHER","sKorn");
         meMap.put("GROWLER ITV","trITV");
         meMap.put("SU-35BM FLANKER-E","jfSu35");
         meMap.put("BTR-90","ifvBTR90");
         meMap.put("AH-6J LITTLE BIRD","shAH6");
         meMap.put("M1128","mtdM1128");
         meMap.put("BM23","martBM23");
         meMap.put("QUAD BIKE","trQB");
         meMap.put("SPRUT-SD","mtdSPRUTSD");
         meMap.put("GUNSHIP","vmaG");
         meMap.put("M142","martHIM");
         meMap.put("BARSUK","trVodnM");
         meMap.put("PHOENIX","trHmvM");
         meMap.put("RHINO","trVanM");
         meMap.put("DROPSHIP","vmaD");
         meMap.put("HMMWV ASRAD","trhmvnxp5");
         meMap.put("DIRTBIKE","trKLR");
         meMap.put("VODNIK AA","trVodnxp5");
	return meMap;
    }
    
    private String[] getAsighmentsString() {
		String[] menuList = new String[] {
				"xpma01","xpma02","xpma03","xpma04","xpma05","xpma06","xpma07","xpma08","xpma09","xpma10", 
				"xp2prema01","xp2prema02","xp2prema03","xp2prema04","xp2prema05","xp2prema06","xp2prema07","xp2prema08","xp2prema09","xp2prema10",
				"xp2ma01","xp2ma02","xp2ma03","xp2ma04","xp2ma05","xp2ma06","xp2ma07","xp2ma08","xp2ma09","xp2ma10","xp3prema01","xp3prema02","xp3prema03"
				,"xp3prema04","xp3prema05","xp3prema06","xp3prema07","xp3prema08","xp3prema09","xp3prema10","xp3ma01","xp3ma02","xp3ma03","xp3ma04","xp3ma05"
				,"xp4ma01","xp4ma02","xp4ma03","xp4ma04","xp4ma05","xp4ma06","xp4ma07","xp4ma08","xp4ma09", "xp4ma10", "xp4prema01","xp4prema02","xp4prema03"
				,"xp4prema04","xp4prema05","xp4prema06","xp4prema07","xp4prema08","xp4prema09","xp4prema10","xp5prema01","xp5prema02","xp5prema03","xp5prema04"
				,"xp5prema05","xp5ma01", "xp5ma02","xp5ma03","xp5ma04","xp5ma05"
				};
		return menuList;
	}
    
    public static String[] getEquipementString(){
		String[] menuList = new String[]{"RADIO BEACON","M67 GRENADE","REPAIR TOOL","C4 EXPLOSIVES","DEFIBRILLATOR","T-UGS","M15 AT MINE","M18 CLAYMORE","EOD BOT","SOFLAM","M224 MORTAR","MAV"};
		Arrays.sort(menuList);
		return menuList;
	}
    
    private String[] getWeaponUnlockString() {
		int numberOfElements = 0;
		weaponString = getPlayerWeaponString();
		//Check weapons and their unlocks
		String[][] weaponKillsArray = new String [weaponString.length][2];
		for(int i = 0; weaponString.length>i;i++){
			weaponHash = weaponHashMap();
			weponCode = weaponHash.get(weaponString[i]);
			try {
				weaponJSON = player.getWeapons();
				separateWeaponString = weaponJSON.getJSONObject(weponCode);
				weaponUnlocksArray = separateWeaponString.getJSONArray("unlocks");
				for(int c = 0;weaponUnlocksArray.length()>c;c++){
					weaponUnlocksSeparated = weaponUnlocksArray.getJSONObject(c);
					if(!isOnBlackList(weaponUnlocksSeparated.getString("id"))){
						if(weaponUnlocksSeparated.getLong("needed")>weaponUnlocksSeparated.getLong("curr")){
							numberOfElements++;
						}
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		String[][] temp = new String[numberOfElements][3];
		int tempint = 0;
		for(int i = 0; weaponString.length>i;i++){
			weaponHash = weaponHashMap();
			weponCode = weaponHash.get(weaponString[i]);
			try {
				weaponJSON = player.getWeapons();
				separateWeaponString = weaponJSON.getJSONObject(weponCode);
				weaponUnlocksArray = separateWeaponString.getJSONArray("unlocks");
				for(int c = 0;weaponUnlocksArray.length()>c;c++){
					weaponUnlocksSeparated = weaponUnlocksArray.getJSONObject(c);
					if(!isOnBlackList(weaponUnlocksSeparated.getString("id"))){
						if(weaponUnlocksSeparated.getLong("needed")>weaponUnlocksSeparated.getLong("curr")){
							temp[tempint][0]=weponCode;
							temp[tempint][1]=weaponUnlocksSeparated.getString("name");
							temp[tempint][2]=""+getKillsleft(weaponUnlocksSeparated.getLong("needed"),weaponUnlocksSeparated.getLong("curr"));
							tempint++;
						}
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		Arrays.sort(temp, new Comparator<String[]>() {
            public int compare(final String[] entry1, final String[] entry2) {
                final Long time1 = Long.valueOf(entry1[2]);
                final Long time2 = Long.valueOf(entry2[2]);
                return time1.compareTo(time2);
            }
        });
		String[] returnStringArray = new String[temp.length];
		for(int element = 0;temp.length>element;element++){
			returnStringArray[element]=temp[element][0]+"/"+temp[element][1]+"/"+temp[element][2];
		}
		return returnStringArray;
	}
    
    private boolean isOnBlackList(String string) {
		if(string.equals("premiumcamo_partizan")||string.equals("premiumcamo_berkut")
				||string.equals("premiumcamo_kamysh")||string.equals("premiumcamo_atacs")
				||string.equals("premiumcamo_deserttiger")||string.equals("premiumcamo_digiflora")
				||string.equals("premiumcamo_abu")||string.equals("premiumcamo_nwu")){
			return true;
		}
		return false;
	}

	private String getKillsleft(long long1, long long2) {
		long d;
        d = (long1-long2);  // this will helps you to always keeps in two decimal places
        if(d<1){
        	return ""+50;
        }else{
        	return ""+d;
        }
	}
    
    private String[] getVehicleUnlocksString(String statname2) {
		String[] menuList = null;
		String vehicleType = null;
		vehicleHash = vehicleHashMap();
		String vehicleCode = vehicleHash.get(statname2);
		vehicleJSON = player.getVehicles();
		try {
			separateVehicle = vehicleJSON.getJSONObject(vehicleCode);
			vehicleType = separateVehicle.getString("category");
				int numberOgUnlocks = 0;
				unlocksArray = player.getingVehicleUnlocks().getJSONObject(vehicleType).getJSONArray("unlocks");
				if(unlocksArray.length()>0){
					for(int i = 0 ; i < unlocksArray.length(); i++){
						row = unlocksArray.getJSONObject(i);
						numberOgUnlocks++;
					}
				}
				int y = 0;
				menuList = new String[numberOgUnlocks];
				for(int i = 0 ; i < unlocksArray.length(); i++){
					row = unlocksArray.getJSONObject(i);
						menuList[y]=row.getString("name");
						y++;
				}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(menuList==null){
			menuList=new String[0];
		}
		return menuList;
	}
    
    private String[] getVehicleUnlockString() {
		int numberOfElements = 0;
		String[] vehicleString = getPlayerVehiclekategory();
		String[][] vehicleKillsArray = new String [vehicleString.length][2];
		vehiclekategoriesJSON = player.getVehiclekategories();
		for(int i = 0; vehicleString.length>i;i++){
			try {
				vehicleunlocksObject = vehiclekategoriesJSON.getJSONObject(vehicleString[i]);
				vehicleKategoryUnlocksArray = vehicleunlocksObject.getJSONArray("unlocks");
				for(int y = 0; vehicleKategoryUnlocksArray.length()>y;y++){
					vehicleUnlockSingle = vehicleKategoryUnlocksArray.getJSONObject(y);
					if(vehicleUnlockSingle.getLong("needed")>vehicleUnlockSingle.getLong("curr")){
						numberOfElements++;
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String[][] temp = new String[numberOfElements][3];
		int tempint = 0;
		for(int i = 0; vehicleString.length>i;i++){
			try {
				vehicleunlocksObject = vehiclekategoriesJSON.getJSONObject(vehicleString[i]);
				vehicleKategoryUnlocksArray = vehicleunlocksObject.getJSONArray("unlocks");
				for(int y = 0; vehicleKategoryUnlocksArray.length()>y;y++){
					vehicleUnlockSingle = vehicleKategoryUnlocksArray.getJSONObject(y);
					if(vehicleUnlockSingle.getLong("needed")>vehicleUnlockSingle.getLong("curr")){
						temp[tempint][0]=vehicleString[i];
						temp[tempint][1]=vehicleUnlockSingle.getString("name");
						temp[tempint][2]=""+getPointsLeft(vehicleUnlockSingle.getLong("needed"),vehicleUnlockSingle.getLong("curr"));
						tempint++;
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Arrays.sort(temp, new Comparator<String[]>() {
            public int compare(final String[] entry1, final String[] entry2) {
                final Long time1 = Long.valueOf(entry1[2]);
                final Long time2 = Long.valueOf(entry2[2]);
                return time1.compareTo(time2);
            }
        });
		String[] returnStringArray = new String[temp.length];
		for(int element = 0;temp.length>element;element++){
			returnStringArray[element]=temp[element][0]+"/"+temp[element][1]+"/"+temp[element][2];
		}
		return returnStringArray;
		//return null;
	}
    
    public static String[] getPlayerVehiclekategory(){
		String[] menuList = new String[] {
				"vehicleaa"
				, "vehicleah"
				,"vehicleifv"
				,"vehiclejf"
				,"vehiclembt"
				,"vehiclesh"
				//,"vehicleja"
				};
		return menuList;
	}
    
	private String getPointsLeft(long long1,long long2){
		long d;
        d = (long1-long2);  // this will helps you to always keeps in two decimal places
        if(d<1){
        	return ""+50;
        }else{
        	return ""+d;
        }
	}
	
	private String[] getMedalUnlockString() {
		int numberOfElements = 0;
		String[] medalString = MEDALS;
		medalsJSON = player.getMedals();
		for(int i = 0;medalString.length>i;i++){
			try {
				medalObject = medalsJSON.getJSONObject(medalString[i]);
				if(medalObject.getString("type").equals("multi")){
					numberOfElements++;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		int tempint = 0;
		String[][] temp = new String[numberOfElements][2];
		for(int i = 0;medalString.length>i;i++){
			try {
				medalObject = medalsJSON.getJSONObject(medalString[i]);
				if(medalObject.getString("type").equals("multi")){
					temp[tempint][0]=medalString[i];
					temp[tempint][1]=""+getPointsLeft(medalObject.getLong("needed"),medalObject.getLong("curr"));
					tempint++;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		Arrays.sort(temp, new Comparator<String[]>() {
            public int compare(final String[] entry1, final String[] entry2) {
                final Long time1 = Long.valueOf(entry1[1]);
                final Long time2 = Long.valueOf(entry2[1]);
                return time1.compareTo(time2);
            }
        });
		String[] returnStringArray = new String[temp.length];
		for(int element = 0;temp.length>element;element++){
			returnStringArray[element]=temp[element][0]+"/"+temp[element][1];
		}
		return returnStringArray;
		//return null;
	}
	
	static final String[] MEDALS = new String[] {"m01",
		"m02",
		"m03",
		"m04",
		"m05",
		"m06",
		"m07",
		"m08",
		"m09",
		"m10",
		"m11",
		"m12",
		"m13",
		"m14",
		"m15",
		"m16",
		"m17",
		"m18",
		"m19",
		"m20",
		"m21",
		"m22",
		"m23",
		"m24",
		"m25",
		"m26",
		"m27",
		"m28",
		"m29",
		"m30",
		"m31",
		"m32",
		"m33",
		"m34",
		"m35",
		"m36",
		"m37",
		"m38",
		"m39",
		"m40",
		"m41",
		"m42",
		"m43",
		"m44",
		"m45",
		"m46",
		"m47",
		"m48",
		"m49",
		"m50",
		"xp2mdom",
		"xp2mgm",
		"xp3mts",
		"xp4mscv",
		"xp5m501",
		"xp5mas"
		};
	
	static final String[] RIBONS = new String[] {"r01",
		"r02",
		"r03",
		"r04",
		"r05",
		"r06",
		"r07",
		"r08",
		"r09",
		"r10",
		"r11",
		"r12",
		"r13",
		"r14",
		"r15",
		"r16",
		"r17",
		"r18",
		"r19",
		"r20",
		"r21",
		"r22",
		"r23",
		"r24",
		"r25",
		"r26",
		"r27",
		"r28",
		"r29",
		"r30",
		"r31",
		"r32",
		"r33",
		"r34",
		"r35",
		"r36",
		"r37",
		"r38",
		"r39",
		"r40",
		"r41",
		"r42",
		"r43",
		"r44",
		"r45",
		"xp2rgm",
		"xp3rdom",
		"xp3rts",
		"xp4rndom",
		"xp4rnscv",
		"xp4rscav",
		"xp5asw",
		"xp5r501",
		"xp5r502",
		"xp5ras"};
}
