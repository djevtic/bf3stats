package com.bf3stats;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class PlayerSelector extends ListActivity{
	private static String tag = "djevtic";
	private Context context;
	private Database db;
	private Cursor c;
	private String[] playerString;
	private ListView list;
	private String statname = "playerselector";
	private GlobalStatsArrayAdaptor adapter;
	private Cursor p;
	private Player player;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.playerselectorlayout);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
        	boolean value1 = extras.getBoolean("error");
            if (value1 != true) {
            	showErrorMessage("We have problems with connecting to BF3Stats web page. Please try again.");
            }
        }
        InitializeComponents();
    }

    protected void showErrorMessage(CharSequence connectionErrorMessage) {
		Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, connectionErrorMessage, duration);
    	toast.show();
	}
    
	private void InitializeComponents() {
		player = Player.getInstance();
		db = new Database(this);
		db.open();
		c = db.getAllPlayers();
		if (c.moveToFirst()) {
			int numberOfPlayers = 0;
			if  (c.moveToFirst()) {
		        do {
		            numberOfPlayers++;
		        }while (c.moveToNext());
		    }
			String[] playerIds = new String[numberOfPlayers];
			numberOfPlayers = 0;
			if  (c.moveToFirst()) {
		        do {
		        	playerIds[numberOfPlayers]=c.getString(c.getColumnIndex("id"));
		            numberOfPlayers++;
		        }while (c.moveToNext());
		    }
			
			c.close();
			db.close();
			list = 	getListView();
			adapter = new GlobalStatsArrayAdaptor(this, playerIds, statname);
			setListAdapter(adapter);
		}else{
			c.close();
			db.close();
			Intent mainIntent = new Intent(PlayerSelector.this, PlayerSelect.class);
            PlayerSelector.this.startActivity(mainIntent);
		}
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		// Get the item that was clicked
		Object o = this.getListAdapter().getItem(position);
		String name = "";
		String platform = "";
		String keyword = o.toString();
		db.open();
   		c = db.getAllPlayers();
		if(c!=null){
			if  (c.moveToFirst()) {
		        do {
		        	if(keyword.equals(c.getString(c.getColumnIndex("id")))){
		        		name = c.getString(c.getColumnIndex("name"));
		        		platform = c.getString(c.getColumnIndex("platform"));
		        	}
		        }while (c.moveToNext());
		    }
		}
		db.close();
		Intent i = new Intent(context, LoadingData.class);
		i.putExtra("name", name);
		i.putExtra("platform", platform);
		startActivity(i);
	}
	
	// Initiating Menu XML file (menu.xml)
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.playeraddmenu, menu);
        return true;
    }
 
    /**
     * Event Handling for Individual menu item selected
     * Identify single menu item by it's id
     * @return 
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	int itemId = item.getItemId();
		if (itemId == R.id.menuAddPlayer) {
			Intent mainIntent = new Intent(context, PlayerSelect.class);
			PlayerSelector.this.startActivity(mainIntent);
			return true;
		} else if (itemId == R.id.menuDeletePlayer) {
			Intent deleteIntent = new Intent(context, DeletePlayer.class);
			PlayerSelector.this.startActivity(deleteIntent);
			return true;
		} else if (itemId == R.id.menuComparePlayer) {
			Intent compareIntent = new Intent(context, ComparePlayer.class);
			PlayerSelector.this.startActivity(compareIntent);
			return true;
		}
		return false;
    }
    
    @Override
    public void onBackPressed(){
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
    
}
